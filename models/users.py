from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Table, Column, String, DateTime, ForeignKey, Enum, Text, Integer, Boolean
from sqlalchemy.orm import sessionmaker, scoped_session, relationship, backref
from sqlalchemy.orm import aliased
from sqlalchemy.engine import create_engine
import sqlalchemy.exc

from collections import OrderedDict
import hashlib
import re

import common.types
from models.base import Base
from models.accounts import Account
from common import utils

class RoleList(Base):
    '''
    Role list to define permission per type of action
    '''
    __tablename__ = 'role_list'
    __table_args__ = (
                      {'mysql_engine': 'InnoDB',
                       'mysql_charset': 'utf8'}
                     )
    id = Column('id', Integer, primary_key=True, index=True)
    account = Column('account', Enum(*common.types.get_role_states()),
                     index=True, default='NONE')
    configuration = Column('configuration', Enum(*common.types.get_role_states()),
                                    index=True, default='NONE')
    report = Column('report', Enum(*common.types.get_role_states()),
                    index=True, default='NONE')
    content = Column('content', Enum(*common.types.get_role_states()),
                     index=True, default='NONE')

    def to_dict(self):
        '''
        Return a dict with the Role matrix information to display
        '''
        display_attributes = ['account', 'configuration', 'report', 'content']
        return utils.dict_attributes(self, display_attributes)

    def check_dict(self, data):
        '''
        Check if the data is valid for a RoleList
        '''
        for attribute in ['account', 'configuration', 'report', 'content']:
            if data.get(attribute) in common.types.get_role_states():
                continue
            else:
                return False, 'Wrong RoleList'
        return True, ''

    def load_from_dict(self, data, session):
        '''
        Load a RoleList from the DB according to data
        Return true if data match an entry in the DB
        '''
        try:
            role_list = session.query(RoleList).filter(RoleList.account == data['account']).\
                filter(RoleList.configuration == data['configuration']).\
                filter(RoleList.report == data['report']).\
                filter(RoleList.content == data['content']).\
                one()
            for attribute in ['id', 'account', 'configuration', 'report', 'content']:
                setattr(self, attribute, getattr(role_list, attribute))
            return True
        except:
            return False

class RoleMatrix(Base):
    '''
    RoleMatrix for account and subaccounts
    '''
    __tablename__ = 'role_matrix'
    __table_args__ = (
                      {'mysql_engine': 'InnoDB',
                       'mysql_charset': 'utf8'}
                     )
    id = Column('id', Integer, primary_key=True, index=True)
    userAccount = Column('userAccount', Integer, ForeignKey('role_list.id',
                                                            onupdate='CASCADE',
                                                            ondelete='CASCADE'))
    subAccounts = Column('subAccounts', Integer, ForeignKey('role_list.id',
                                                            onupdate='CASCADE',
                                                                ondelete='CASCADE'))

    def check_dict(self, data):
        '''
        Check if the data is valid for a RoleMatrix
        '''
        role_list = RoleList()
        for attribute in ['userAccount', 'subAccounts']:
            if data.get(attribute):
                is_valid, error_message = role_list.check_dict(data[attribute])
                if is_valid:
                    continue
                else:
                    return False, attribute + ' ' + error_message
            return False, 'RoleMatrix : ' + attribute + 'is missing.'
        return True, ''

    def load_from_dict(self, data, session):
        '''
        Load a RoleMatrix from the DB according to data
        Return true if data match an entry in the DB
        '''
        role_list = RoleList()
        for attribute in ['userAccount', 'subAccounts']:
            try:
                role_list.load_from_dict(data.get(attribute), session)
                setattr(self, attribute, role_list.id)
            except:
                return False
        try:
            role_matrix = session.query(RoleMatrix).filter(RoleMatrix.userAccount == self.userAccount).\
                filter(RoleMatrix.subAccounts == self.subAccounts).one()
            for attribute in ['id', 'userAccount', 'subAccounts']:
                setattr(self, attribute, getattr(role_matrix, attribute))
            return True
        except:
            return False

class User(Base):
    '''
    User
    '''
    __tablename__ = 'users'
    __table_args__ = (
                      {'mysql_engine': 'InnoDB',
                       'mysql_charset': 'utf8'}
                     )
    id = Column('id', Integer, primary_key=True, index=True)
    accountHash = Column('accountHash', String(10), ForeignKey('accounts.hashCode',
                                    onupdate='CASCADE',
                                    ondelete='CASCADE'))
    username = Column('username', String(20))
    password = Column('password', String(64))
    firstName = Column('firstName', String(64))
    lastName = Column('lastName', String(64))
    lastLogin = Column('lastLogin', String(20))
    email = Column('email', String(64))
    phone = Column('phone', String(30))
    fax = Column('fax', String(30))
    authorizedSupportContact = Column('authorizedSupportContact', Boolean, default=False)
    roles = Column('roles', Integer, ForeignKey('role_matrix.id',
                                    onupdate='CASCADE',
                                    ondelete='CASCADE'))

    def _check_dict(self, data):
        '''
        Check if the data is valid for a user. Return a list with the result_boolean and an error message
        '''
        required_fields = {
                            'username' : {'regexp': '\w+', 'filled': False},
                            'password' : {'regexp': '(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$', 'filled': False},
                            'firstName' : {'regexp': '\w+', 'filled': False},
                            'lastName' : {'regexp': '\w+', 'filled': False},
                            'email' : {'regexp': '^[a-zA-Z0-9._%-+]+@[a-zA-Z0-9._%-]+.[a-zA-Z]{2,6}$', 'filled': False},
                            'authorizedSupportContact' : {'regexp': '^True|true|false|False|0|1$', 'filled': False},
                            'roles': {'regexp': '.*', 'filled': False},
                        }
        optional_fields = {
                            'phone' : {'regexp': '.*', 'filled': False},
                            'fax' : {'regexp': '.*', 'filled': False},
                        }
        ignored_fields = ['id', 'accountHash', 'lastLogin']
        result, message = utils.check_fields(data, ignored_fields, required_fields, optional_fields)
        if not result:
            return result, message
        else:
            return True, ''

    def create_from_dict(self, data, session, parent_hash_code):
        '''
        Save a user generated from data
        Return a boolean and the dict to describe the user
        '''
        is_valid_data , error_message = self._check_dict(data)
        if not is_valid_data:
            return is_valid_data, error_message
        if data.get('accountHash') not in [None, parent_hash_code]:
            return False, 'Wrong accountHash'
        try:
            parent_account = session.query(Account).\
                filter(Account.hashCode == parent_hash_code).one()
            if parent_account.accountStatus != 'ACTIVATED':
                return False, 'Parent account not activated.'
        except:
            return False, 'Wrong accountHash'
        users = session.query(User).filter(User.username == data['username']).all()
        if len(users) > 0:
            return False, 'Username already exists'
        self.accountHash = parent_hash_code
        role_matrix = RoleMatrix()
        is_valid_data , error_message = role_matrix.check_dict(data['roles'])
        if not is_valid_data:
            return is_valid_data, error_message
        role_matrix.load_from_dict(data['roles'], session)
        self.roles = role_matrix.id
        self.password = hashlib.sha256(data['password']).hexdigest()
        for attribut in ['username', 'firstName', 'lastName', 'email', \
            'authorizedSupportContact', 'phone', 'fax']:
            if data.get(attribut):
                setattr(self, attribut, data[attribut])
        session.add(self)
        session.commit()
        return True, self.to_dict(session)

    def  update_from_dict(self, data, session, parent_hash_code):
        '''
        Update an user from the generated json data
        '''
        is_valid_data , error_message = self._check_dict(data)
        if not is_valid_data:
            return is_valid_data, error_message
        if data.get('accountHash') not in [None, parent_hash_code]:
            return False, 'Wrong accountHash'
        try:
            user_to_update = session.query(User).filter(User.id == data.get('id')).one()
            parent_account = session.query(Account).\
                filter(Account.hashCode == parent_hash_code).one()
            if parent_account.accountStatus != 'ACTIVATED':
                return False, 'Parent account not activated.'
                if user_to_update.accountHash != parent_account.hashCode:
                    return False, 'Wrong user id'
        except:
            return False, 'Wrong user id'
        users = session.query(User).filter(User.username == data['username']).\
                filter(User.id != data["id"]).all()
        if len(users) > 0:
            return False, 'Username already exists'
        role_matrix = RoleMatrix()
        is_valid_data , error_message = role_matrix.check_dict(data['roles'])
        if not is_valid_data:
            return is_valid_data, error_message
        role_matrix.load_from_dict(data['roles'], session)
        user_to_update.roles = role_matrix.id
        if data.get('password'):
            user_to_update.password = hashlib.sha256(data['password']).hexdigest()
        for attribut in ['username', 'firstName', 'lastName', 'email', \
            'authorizedSupportContact', 'phone', 'fax']:
            if data.get(attribut):
                setattr(user_to_update, attribut, data[attribut])
        session.add(user_to_update)
        session.commit()
        return True, user_to_update.to_dict(session)

    def update_me(self, data, session):
        '''
        Update the current user from dict data. No roles changes allowed here
        '''
        required_fields = {
                            'username' : {'regexp': '\w+', 'filled': False},
                            'password' : {'regexp': '(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$', 'filled': False},
                            'firstName' : {'regexp': '\w+', 'filled': False},
                            'lastName' : {'regexp': '\w+', 'filled': False},
                            'email' : {'regexp': '^[a-zA-Z0-9._%-+]+@[a-zA-Z0-9._%-]+.[a-zA-Z]{2,6}$', 'filled': False},
                            'authorizedSupportContact' : {'regexp': '^True|true|false|False|0|1$', 'filled': False},
                        }
        optional_fields = {
                            'phone' : {'regexp': '.*', 'filled': False},
                            'fax' : {'regexp': '.*', 'filled': False},
                        }
        ignored_fields = ['id', 'accountHash', 'lastLogin']
        result, message = utils.check_fields(data, ignored_fields, required_fields, optional_fields)
        if not result:
            return False, message
        if data.get("id") and data.get("id") != self.id:
            return False, 'Wrong user id'
        if data.get("accountHash") and data.get("accountHash") != self.accountHash:
            return False, 'Wrong accountHash'
        users = session.query(User).filter(User.username == data['username']).\
                filter(User.id != self.id).all()
        if len(users) > 0:
            return False, 'Username already exists'
        if data.get('password'):
            self.password = hashlib.sha256(data['password']).hexdigest()
        for attribute in ['username', 'firstName', 'lastName', 'email', \
            'authorizedSupportContact', 'phone', 'fax']:
            if data.get(attribute):
                setattr(self, attribute, data[attribute])
        session.add(self)
        session.commit()
        return True, self.to_dict(session)

    def to_dict(self, session):
        '''
        Return a dict with the user's information to display
        '''
        user, role_matrix = session.query(User, RoleMatrix).\
            join(RoleMatrix, RoleMatrix.id ==  User.roles).\
            filter(User.id == self.id).one()
        user_account = session.query(RoleList).filter(RoleList.id == role_matrix.userAccount).one()
        user_subaccounts = session.query(RoleList).filter(RoleList.id == role_matrix.subAccounts).one()
        try:
            user, role_matrix = session.query(User, RoleMatrix).\
                join(RoleMatrix, RoleMatrix.id ==  user.roles)\
                .filter(User.id == self.id).one()
        except:
            return None
        display_attributes = ['id', 'accountHash', 'username', 'firstName', 'lastName', 'lastLogin',\
            'email', 'phone', 'fax', 'authorizedSupportContact']
        result = utils.dict_attributes(self, display_attributes)
        result['roles'] = { 'userAccount' : user_account.to_dict(),
                            'subAccounts' : user_subaccounts.to_dict()
                        }
        return result
