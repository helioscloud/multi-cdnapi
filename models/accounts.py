'''
Models used for accounts
'''

from sqlalchemy import Table, Column, String, DateTime, ForeignKey, Enum, Text\
    , Integer, Boolean
from sqlalchemy.orm import aliased, relationship, backref
import sqlalchemy.exc

import common.types
from models.base import Base
from common import utils
from models.services import Service


class Contact(Base):
    '''
    Contact used by Account
    '''
    __tablename__ = 'contacts'
    __table_args__ = (
                      {'mysql_engine': 'InnoDB',
                       'mysql_charset': 'utf8'}
                     )
    id = Column('id', Integer, primary_key=True, index=True)
    firstName = Column('firstName', String(64))
    lastName = Column('lastName', String(64))
    email = Column('email', String(64))
    phone = Column('phone', String(30))
    fax = Column('fax', String(30))

    def to_dict(self):
        '''
        Return a dict with the contact's information to display
        '''
        display_attributes = ['id', 'firstName', 'lastName', 'email', 'phone',
                                    'fax']
        return utils.dict_attributes(self, display_attributes)

    def check_dict(self, data):
        '''
        Check if the data is valid for an account creation or update
        '''
        required_fields = {'firstName': {'regexp': '^\w+$', 'filled': False},
                           'lastName': {'regexp': '^\w+$', 'filled': False},
                           'email': {'regexp': '^.*$', 'filled': False},
                           'phone': {'regexp': '^\w+$', 'filled': False},
                           'fax': {'regexp': '^\w+$', 'filled': False}
                           }
        ignored_fields = ['id']
        result, message = utils.check_fields(data, ignored_fields,
                            required_fields)
        if not result:
            return result, message
        for field in required_fields:
            setattr(self, field, data[field])
        return True, ''

    def load_from_dict(self, data):
        '''
        Load a contact from a dict. Dict has already been check with check_dict
        '''
        for attribute in ['firstName', 'lastName', 'email', 'phone', 'fax']:
            if data.get(attribute):
                setattr(self, attribute, data[attribute])

    def cmp(self, other_contact):
        '''
        Compare the currrent contact with another one
        '''
        for attribute in ['firstName', 'lastName', 'email', 'phone', 'fax']:
            if getattr(self, attribute) != getattr(other_contact, attribute):
                return False
        return True

    def copy(self, other_contact):
        '''
        Copy the other_contact attributes
        '''
        for attribute in ['firstName', 'lastName', 'email', 'phone', 'fax']:
            setattr(self, attribute, getattr(other_contact, attribute))

'''
Association table for the many to many relationship
'''
services_accounts = Table('services_accounts', Base.metadata,
                          Column('service_id', Integer, ForeignKey(
                                'services.id')),
                          Column('account_id', Integer, ForeignKey(
                                'accounts.id')))


class Account(Base):
    '''
    Account
    '''
    __tablename__ = 'accounts'
    __table_args__ = (
                      {'mysql_engine': 'InnoDB',
                       'mysql_charset': 'utf8'}
                     )
    id = Column('id', Integer, primary_key=True, index=True)
    hashCode = Column('hashCode', String(10), primary_key=True, index=True)
    accountName = Column('accountName', String(20))
    accountStatus = Column('accountStatus', Enum(*common.types.get_account_states()),
                                    index=True, default='ACTIVATED')
    primaryContact = Column('primaryContact', Integer, ForeignKey('contacts.id',
                                                                  onupdate='CASCADE',
                                                                  ondelete='CASCADE'))
    billingContact = Column('billingContact', Integer, ForeignKey('contacts.id',
                                                                  onupdate='CASCADE',
                                                                  ondelete='CASCADE'))
    technicalContact = Column('technicalContact', Integer, ForeignKey('contacts.id',
                                                                      onupdate='CASCADE',
                                                                      ondelete='CASCADE'))
    maximumDirectSubAccounts = Column('maximumDirectSubAccounts', Integer)
    subAccountCreationEnabled = Column('subAccountCreationEnabled', Boolean, default=True)
    services = relationship('Service', secondary=services_accounts, backref='accounts')
    parentId = Column(Integer, ForeignKey('accounts.id'))
    subAccounts = relationship('Account', backref=backref('accounts', remote_side=[id]))

    def create_from_dict(self, data, session, parent_hash_code):
        '''
        Save an account generated from data
        Contacts are also added in the DB if needed
        Return a boolean and a message
        '''
        is_valid_data , error_message = self._check_dict(data)
        if not is_valid_data:
            return is_valid_data, error_message
        parent_account =  session.query(Account).filter(Account.hashCode == parent_hash_code).one()
        is_valid_request, error_message = self._check_parent_account(data, parent_account)
        if not is_valid_request:
            return is_valid_request, error_message
        self.hashCode = utils.generate_uniq_hashCode('account', 10, session)
        self.parentId = parent_account.id
        for field in ['accountName', 'accountStatus', 'maximumDirectSubAccounts', 'subAccountCreationEnabled'] :
            setattr(self, field, data[field])
        '''
        If a contact is provided and if it's different from the parent's account contact,
        a new contact is created. Otherwise, the id of the parent's contact is used
        '''
        for contact_type in ['primaryContact', 'billingContact','technicalContact']:
            if data.get(contact_type):
                contact = Contact()
                contact.load_from_dict(data[contact_type])
                parent_contact = session.query(Contact).filter(Contact.id == getattr(parent_account,contact_type)).one()
                if not contact.cmp(parent_contact):
                    session.add(contact)
                    session.commit()
                    setattr(self, contact_type, contact.id)
                else:
                    setattr(self, contact_type, parent_contact.id)
            else:
                setattr(self, contact_type, getattr(parent_account, contact_type))
        '''
        if no service is provided, all the parent's services are associated with the new account
        if a service is not is not in the parent's services list, it's simply ignored and not added
        '''
        if data.get('services'):
            for service in data['services']:
                for parent_service in parent_account.services:
                    if service['id'] == parent_service.id:
                        self.services.append(parent_service)
        else:
            self.services = parent_account.services
        session.add(self)
        session.commit()
        return True, self.to_dict(session)

    def update_from_dict(self, data, session, hash_code):
        '''
        Update an account from the generated json data
        '''
        try:
            current_account = session.query(Account).\
                filter(Account.hashCode == hash_code).one()
            parent_account = session.query(Account).\
                filter(Account.id == current_account.parentId).one()
        except:
            return False, 'Wrong account.'
        is_valid_data , error_message = self._check_dict(data)
        if not is_valid_data:
            return False, error_message
        for field in ['accountName', 'accountStatus', 'maximumDirectSubAccounts', 'subAccountCreationEnabled'] :
            setattr(current_account, field, data[field])
        '''
        if no service is provided, all the parent's services are associated with the new account
        if a service is not is not in the parent's services list, it's simply ignored and not added
        '''
        current_account.services = []
        if data.get('services'):
            for service in data['services']:
                for parent_service in parent_account.services:
                    if service['id'] == parent_service.id:
                        current_account.services.append(parent_service)
        else:
            current_account.services = parent_account.services
        '''
        If a contact is provided and if it's different from the parent's contact,
        the current contact is updated (same id), if it's the same id than the parent account
        a new contact is created.
        If no contact is provided, the parent's contact is used.
        '''
        for contact_type in ['primaryContact', 'billingContact','technicalContact']:
            if data.get(contact_type):
                contact = Contact()
                contact.load_from_dict(data[contact_type])
                current_contact = session.query(Contact).filter(Contact.id == getattr(current_account,contact_type)).one()
                if not contact.cmp(current_contact):
                    if not current_contact.id == getattr(parent_account, contact_type):
                        current_contact.copy(contact)
                        session.add(current_contact)
                        session.commit()
                    else:
                        session.add(contact)
                        session.commit()
                        setattr(current_account, contact_type, contact.id)
            else:
                setattr(self, contact_type, getattr(parent_account, contact_type))
        session.add(current_account)
        session.commit()
        return True, current_account.to_dict(session)

    def _check_parent_account(self, data, parent_account):
        '''
        Check if the request is valid with the parent account
        '''
        if parent_account.subAccountCreationEnabled == False:
            return False, "Sub accounts for the parent account are not allowed."
        if parent_account.maximumDirectSubAccounts <= len(parent_account.subAccounts) + 1 :
            return False, 'Too many sub accounts for the parent account.'
        if parent_account.accountStatus in ['DELETED', 'SUSPENDED']:
            return False, 'Parent account %s  is %s' % (parent_account.hashCode, parent_account.accountStatus)
        return True, ''

    def _check_dict(self, data):
        '''
        Check if the data is valid for an account. Return a list with the result_boolean and an error message
        '''
        required_fields = {
                            'accountName' : {'regexp': '\w+', 'filled': False},
                            'accountStatus' : {'regexp': '\w+', 'filled': False},
                            'maximumDirectSubAccounts' : {'regexp': '\d+', 'filled': False},
                            'subAccountCreationEnabled' : {'regexp': '^True|true|false|False$', 'filled': False},
                        }
        optional_fields = {
                            'primaryContact' : {'regexp': '.*', 'filled': False},
                            'billingContact' : {'regexp': '.*', 'filled': False},
                            'technicalContact' : {'regexp': '.*', 'filled': False},
                            'services' : {'regexp': '.*', 'filled': False},
                        }
        ignored_fields = ['id', 'hashCode', 'parentId',  'subAccounts']
        result, message = utils.check_fields(data, ignored_fields, required_fields, optional_fields)
        if not result:
            return result, message
        if data.get('services'):
            service = Service()
            for service_candidate in data['services']:
                result, message = service.check_dict(service_candidate)
                if not result:
                    return False, 'Wrong service format : %s' % message
            for contact_type in ['primaryContact', 'billingContact', 'technicalContact']:
                if data.get(contact_type):
                    contact = Contact()
                    result, message = contact.check_dict(data[contact_type])
                    if not result:
                        return False, 'Wrong contact format : %s' % message
        return True, ''

    def to_dict(self, session, display_subaccounts=False):
        '''
        Return a dict with the account's information to display
        '''
        display_attributes = ['id', 'hashCode', 'accountName', 'accountStatus', 'maximumDirectSubAccounts',
        'subAccountCreationEnabled', 'parentId', 'services']
        primaryContact = aliased(Contact)
        billingContact = aliased(Contact)
        technicalContact = aliased(Contact)
        try:
            account, primary, billing, technical = session.query(Account, primaryContact, billingContact, technicalContact).\
                join(primaryContact, Account.primaryContact == primaryContact.id).\
                join(billingContact, Account.billingContact == billingContact.id).\
                join(technicalContact, Account.technicalContact == technicalContact.id).\
                filter(Account.hashCode == self.hashCode).one()
        except:
            return None
        result = utils.dict_attributes(account, display_attributes)
        result['services'] = []
        result['primaryContact'] = primary.to_dict()
        result['billingContact'] = billing.to_dict()
        result['technicalContact'] = technical.to_dict()
        for service in account.services:
            result['services'].append(service.to_dict())
        if display_subaccounts == 'True':
            result['sub_accounts'] = []
            for sub_accounts in account.subAccounts:
                result['sub_accounts'].append(sub_accounts.hashCode)
        return result

    def delete(self, session):
        try:
            account = session.query(Account).\
                filter(Account.hashCode == self.hashCode).one()
            account.accountStatus = 'DELETED'
            session.add(account)
            session.commit()
            #Additional actions are required , remove conf for example
        except:
            return None
        return True
