'''
Models used for accounts
'''

from sqlalchemy import Table, Column, String, DateTime, ForeignKey, Enum, Text, Integer, Boolean
from sqlalchemy.orm import aliased, relationship, backref
import sqlalchemy.exc

import common.types
from models.base import Base
from common import utils


class Service(Base):
    '''
    Service used by Account
    '''
    __tablename__ = 'services'
    __table_args__ = (
                      {'mysql_engine': 'InnoDB',
                       'mysql_charset': 'utf8'}
                     )
    id = Column('id', Integer, primary_key=True, index=True)
    name = Column('name', String(15))
    description = Column('description', String(64))
    multiCdnService = Column('multiCdnService', Boolean)

    def to_dict(self):
        '''
        Return a dict with the account's information to display
        '''
        display_attributes = ['id', 'name', 'description', 'multiCdnService']
        return utils.dict_attributes(self, display_attributes)

    def check_dict(self, data):
        '''
        Check if the data is valid for an account creation or update
        '''
        required_fields = {
                        'id' : {'regexp': '^\d+$', 'filled': False}
                    }
        ignored_fields = ['name', 'description', 'multiCdnService']
        result, message = utils.check_fields(data, ignored_fields, required_fields)
        if not result:
            return result, message
        for field in required_fields:
            setattr(self, field, data[field])
        return True, ''
