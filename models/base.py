from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.engine import create_engine
import sqlalchemy.exc

Base = declarative_base()


def db_reconnect(db_url, session=None):
    '''
    Start sql session or reconnect to database if necessary.
    '''
    def __macro_session_create(db_url):
        '''
        Macro to create session
        '''
        if 'sqlite' in db_url:
            engine = create_engine(db_url, echo=True)
        else:
            engine = create_engine(db_url, pool_recycle=300, pool_size=10,
                                   echo=False)
        Base.metadata.create_all(bind=engine, checkfirst=True)
        smaker = sessionmaker(bind=engine, autoflush=True,
                              autocommit=False, expire_on_commit=True)
        session = scoped_session(smaker)
        return session
    return __macro_session_create(db_url)
    '''
    Cache issue if we keep the same session
    if not session:
        return __macro_session_create(db_url)
    try:
        session.execute('SELECT 1 FROM DUAL')
    except (sqlalchemy.exc.OperationalError, sqlalchemy.exc.TimeoutError):
        return __macro_session_create(db_url)
    return session
    '''
