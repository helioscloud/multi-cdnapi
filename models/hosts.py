'''
Models used for hosts
'''

from sqlalchemy import Table, Column, String, DateTime, ForeignKey, Enum, \
    Text, Integer, Boolean, func
from sqlalchemy.orm import aliased, relationship, backref
import sqlalchemy.exc

import json
import re

import common.types
from models.base import Base
from models.accounts import Account
from common import utils
from models.services import Service

'''
Association table for the many to many relationship
'''
services_hosts = Table('services_hosts', Base.metadata,
    Column('service_id', Integer, ForeignKey('services.id')),
    Column('host_id', Integer, ForeignKey('hosts.id'))
    )

class CdxApp(Base):
    '''
    Cedexis application
    '''
    __tablename__ = 'cdx_apps'
    __table_args__ = (
                      {'mysql_engine': 'InnoDB',
                       'mysql_charset': 'utf8'}
                     )
    id = Column('id', Integer, primary_key=True, index=True)
    cname = Column('cname', String(255))
    status = Column('status', Enum(*['UNUSED','ACTIVATED', 'SUSPENDED', 'DELETED']))

    def set_free(self, session):
        self.status = 'UNUSED'
        session.add(self)
        session.commit()

class Host(Base):
    '''
    Host
    '''
    __tablename__ = 'hosts'
    __table_args__ = (
                      {'mysql_engine': 'InnoDB',
                       'mysql_charset': 'utf8'}
                     )
    id = Column('id', Integer, primary_key=True, index=True)
    cname = Column('cname', String(10), primary_key=True, index=True)
    accountHash = Column('accountHash', String(10),
                        ForeignKey('accounts.hashCode',
                        onupdate='CASCADE',
                        ondelete='CASCADE'))
    name = Column('name', String(100))
    services = relationship('Service', secondary=services_hosts, backref='hosts')
    active = Column('active', Boolean, default=True)
    status_updated = Column('status_updated', DateTime, default=func.now())

    def _check_dict(self, data):
        if not isinstance(data, dict):
            return False, 'Wrong format'
        if 'name' in data.keys():
            if not re.match('^\w+$', str(data['name'])):
                message = 'Wrong format : name'
                return False, message
        else:
            return False, 'name is missing.'
        if 'services' in data.keys():
            if not isinstance(data['services'], list):
                return False, "Services should be an array"
            sh = Service()
            for service_data in data['services']:
                res, message = sh.check_dict(service_data)
                if not res:
                    return res, message
        if 'hostnames' in data.keys():
            if not isinstance(data['hostnames'], list):
                return False, "hostnames should be an array"
            hh = Hostname()
            for hostname_data in data['hostnames']:
                res, message = hh._check_dict(hostname_data)
                if not res:
                    return res, message
        if 'scopes' in data.keys():
            if not isinstance(data['scopes'], list):
                return False, "scopes should be an array"
            sh = Scope()
            for scope_data in data['scopes']:
                res, message = sh._check_dict(scope_data)
                if not res:
                    return res, message
        if 'origin' not in data.keys():
            return False, 'Origin is missing'
        else:
            oh = Origin()
            res, message = oh._check_dict(data['origin'])
            if not res:
                return res, message
        return True, ''

    def to_dict(self, session):
        '''
        Return a dict with the host's information to display
        '''
        try:
            self = session.query(Host).filter(Host.id == self.id).one()
            display_attributes = ['id', 'cname', 'name', 'accountHash']
            result = utils.dict_attributes(self, display_attributes)
            result['services'] = []
            for service in self.services:
                result['services'].append(service.to_dict())
            scopes = session.query(Scope).\
                filter(Scope.hostcname == self.cname).all()
            result['scopes'] = []
            for scope in scopes:
                result['scopes'].append(scope.to_dict(session))
            hostnames = session.query(Hostname).filter(Hostname.hostcname == self.cname).all()
            origin = session.query(Origin).filter(Origin.hostcname ==  self.cname).one()
            result['origin'] = origin.to_dict(session)
            result['hostnames'] = []
            for hostname in hostnames:
                result['hostnames'].append(hostname.hostname)
        except:
            return None
        return result

    def get_hostnames(self, session):
        '''
        Return an array with the hostnames list associated with the Host
        '''
        result = []
        try:
            self = session.query(Host).filter(Host.id == self.id).one()
            hostnames = session.query(Hostname).\
                filter(Hostname.hostcname == self.cname).\
                all()
            for hostname in hostnames:
                result.append(hostname.hostname)
            return result
        except:
            result = []

    def get_scopes(self, session):
        '''
        Return an array with the list of scopes associated with the Host
        '''
        result = []
        scopes = session.query(Scope).\
            filter(Scope.hostcname == self.cname).all()
        for scope in scopes:
            result.append(scope.to_dict(session))
        return result

    def create_from_dict(self, accountHash, data, session):
        '''
        Create a Host from a dict
        '''
        def _rollback(cdx_app, new_host, cname, session):
            cdx_app.set_free(session)
            hh = Hostname()
            hh.delete_all(new_host.cname, session)
            sh = Scope()
            sh.delete_all(new_host.cname, session)
            oh = Origin()
            oh.delete_all(new_host.cname, session)
            session.delete(new_host)
            session.commit()

        def _add_root_scope(new_host, session):
            data = {"path": "/", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}
            sh = Scope()
            resp, data = sh.create_from_dict(new_host.cname, data, session)
            return resp, message

        new_host = Host()
        resp, message = new_host._check_dict(data)
        if not resp:
            return False, message
        try:
            account = session.query(Account).\
                filter(Account.hashCode == accountHash).one()
        except:
            return False, 'Unknown acccount %s' % accountHash
        new_host.accountHash = accountHash
        free_cdx_apps = session.query(CdxApp).filter(CdxApp.status == 'UNUSED').all()
        if len(free_cdx_apps) == 0:
            return False, 'Ressources depleted'
        cdx_app = free_cdx_apps[0]
        cdx_app.status = 'ACTIVATED'
        session.add(cdx_app)
        session.commit()
        new_host.cname = cdx_app.cname
        new_host.active = True
        new_host.name = data['name'][0:99]
        session.add(new_host)
        session.commit()
        if 'hostnames' in data.keys():
            hh = Hostname()
            for hostname in data['hostnames']:
                resp, message = hh.create_from_dict(new_host.cname, hostname, session)
                if not resp:
                    _rollback(cdx_app, new_host, new_host.cname, session)
                    return resp, message
        if 'services' not in data.keys():
            for service in account.services:
                new_host.services.append(service)
        else:
            for service in data['services']:
                for account_service in account.services:
                    if service['id'] == account_service.id:
                        new_host.services.append(account_service)
            if len(new_host.services) == 0:
                for service in account.services:
                    new_host.services.append(service)
        oh = Origin()
        resp, message = oh._create_from_dict(new_host.cname, data['origin'], session)
        if not resp:
            _rollback(cdx_app, new_host, new_host.cname, session)
            return resp, message
        if 'scopes' in data.keys():
            root_created = False
            sh = Scope()
            for scope in data['scopes']:
                resp, data = sh.create_from_dict(new_host.cname, scope, session)
                if not resp:
                    _rollback(cdx_app, new_host, new_host.cname, session)
                    return resp, message
                if scope['path'] == '/':
                    root_created = True
            if not root_created:
                resp, message = _add_root_scope(new_host, session)
                if not resp:
                    _rollback(cdx_app, new_host, cname, session)
                    return resp, message
        else:
            resp, message = _add_root_scope(new_host, session)
            if not resp:
                _rollback(cdx_app, new_host,cname, session)
                return resp, message
        self = new_host
        return True, self.to_dict(session)

    def update_from_dict(self, accountHash, cname, data, session):
        '''
        Update an Host from a dict
        '''
        try:
            host = session.query(Host).filter(Host.cname == cname).one()
        except:
            return False, 'Unknown host %s for the account %s.' % (cname, accountHash)
        if host.accountHash != accountHash:
            return False, 'Unknown host %s for the account %s.' % (cname, accountHash)
        try:
            account = session.query(Account).\
                filter(Account.hashCode == accountHash).one()
        except:
            return False, 'Unknown host %s for the account %s.' % (cname, accountHash)
        if not isinstance(data, dict):
            return False, 'Wrong format'
        for key in data.keys():
            if key != 'name' and key != 'services':
                return False, 'Only name and services can be updated. Origin, Scopes and Hostnames should be updated with the dedicated call.'
        if 'name' in data.keys():
            if not re.match('^\w+$', str(data['name'])):
                message = 'Wrong format : name'
                return False, message
            else:
                host.name = data['name']
        else:
            return False, 'name is missing.'
        host.services = []
        if 'services' in data.keys():
            if not isinstance(data['services'], list):
                return False, "Services should be an array"
            sh = Service()
            for service_data in data['services']:
                res, message = sh.check_dict(service_data)
                if not res:
                    return res, message
                for account_service in account.services:
                    if service_data['id'] == account_service.id:
                        host.services.append(account_service)
        if len(host.services) == 0:
            for service in account.services:
                host.services.append(service)
        session.add(host)
        session.commit()
        self = host
        return True, self.to_dict(session)

    def delete(self, session):
        try:
            host = session.query(Host).filter(Host.id == self.id).one()
            host.active = False
            session.add(host)
            session.commit()
            self = host
            return True, 'Host %s deleted.' % (self.cname)
        except:
            return False, 'Unknown host'

class Hostname(Base):
    '''
    Hostnames
    '''
    __tablename__ = 'hostnames'
    __table_args__ = (
                      {'mysql_engine': 'InnoDB',
                       'mysql_charset': 'utf8'}
                     )
    id = Column('id', Integer, primary_key=True, index=True)
    hostname = Column('hostname', String(255))
    hostcname = Column(String(10), ForeignKey('hosts.cname'))

    def _check_dict(self, data):
        regexp = '^((([a-zA-Z]|[a-zA-Z][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z]|[A-Za-z][A-Za-z0-9\-]*[A-Za-z0-9])|([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3})$'
        if not re.match(regexp, data):
            return False, 'Wrong hostname format'
        else:
            return True, ''

    def create_from_dict(self, hostcname, data, session):
        '''
        Add an hostname to a hostCode
        '''
        new_hostname = Hostname()
        resp, message = new_hostname._check_dict(data)
        if not resp:
            return resp, message
        hn = session.query(Hostname).filter(Hostname.hostname == data).all()
        if len(hn) > 0:
            for hn_to_check in hn:
                hosts = session.query(Host).filter(Host.active == True).\
                    filter(Host.cname == hn_to_check.hostcname).all()
                if len(hosts) > 0:
                    return False, 'Hostname %s already used.' % (data)
        new_hostname.hostname = data
        new_hostname.hostcname = hostcname
        session.add(new_hostname)
        session.commit()
        self = new_hostname
        return True, data

    def delete_all(self, hostcname, session):
        '''
        Delete all hostname for an hosts
        '''
        hostnames = session.query(Hostname).filter(Hostname.hostcname == hostcname).all()
        for hostname in hostnames:
            session.delete(hostname)
        session.commit()
        return True, ''

    def delete(self, hostcname, hostname, session):
        '''
        Remove the hostname from the hascode
        '''
        hostnames = session.query(Hostname).\
            filter(Hostname.hostcname == hostcname).\
            filter(Hostname.hostname == hostname).all()
        if not len(hostnames) == 1:
            return False, 'Unknown hostname for the host %s' % (hostcname)
        session.delete(hostnames[0])
        session.commit()
        return True, 'Hostname %s deleted' % (hostname)

class Scope(Base):
    '''
    Configuration Scope
    '''
    __tablename__ = 'scopes'
    __table_args__ = (
                      {'mysql_engine': 'InnoDB',
                       'mysql_charset': 'utf8'}
                     )
    id = Column('id', Integer, primary_key=True, index=True)
    path = Column('path', String(80))
    queryStringModeOn = Column('queryStringModeOn', Boolean)
    cacheControlModeInt = Column('cacheControlModeInt', Integer)
    cacheControlModeExt = Column('cacheControlModeExt', Integer)
    cacheControlModeForce = Column('cacheControlModeForce', Boolean)
    hostcname = Column(String(10), ForeignKey('hosts.cname'))

    def _check_dict(self, data):
        if not isinstance(data, dict):
            return False, 'Wrong format'
        required_fields = {'path': {'regexp': '^(/([a-zA-Z0-9-_])*)+$', 'filled': False},
                           'queryStringModeOn': {'regexp': '^True|False$', 'filled': False},
                           'cacheControlMode' : {'regexp': '.*', 'filled' : False}
                           }
        ignored_fields = ['id', 'hostcname']
        result, message = utils.check_fields(data, ignored_fields,
                            required_fields)
        if not result:
            return result, message
        required_fields = {'int': {'regexp': '^\d+$', 'filled': False},
                            'ext': {'regexp': '^\d+$', 'filled': False},
                            'force': {'regexp': '^True|False$', 'filled': False}}
        ignored_fields = []
        result, message = utils.check_fields(data['cacheControlMode'], ignored_fields,
                            required_fields)
        return result, message

    def delete_all(self, hostcname, session):
        '''
        Delete all scopes for an host
        '''
        scopes = session.query(Scope).filter(Scope.hostcname == hostcname).all()
        for scope in scopes:
            session.delete(scope)
        session.commit()
        return True, ''

    def delete(self, id, hostcname, session):
        try:
            scope = session.query(Scope).filter(Scope.id == id)\
                .filter(Scope.hostcname == hostcname).one()
        except:
            return False, 'Unknown scope %d for the host %s' % (id, hostcname)
        if scope.path == '/':
            return False, 'The scope with the path / can\'t be deleted'
        session.delete(scope)
        session.commit()
        return True, 'Scope %d for the host %s deleted' % (id, hostcname)

    def to_dict(self, session):
        '''
        Return a dict with the host's information to display
        '''
        try:
            self = session.query(Scope).filter(Scope.id == self.id).one()
            display_attributes = ['id','path', 'queryStringModeOn']
            result = utils.dict_attributes(self, display_attributes)
            result['cacheControlMode'] = utils.dict_attributes(self,
                ['cacheControlModeInt', 'cacheControlModeExt', 'cacheControlModeForce'])
        except:
            return None
        return result

    def create_from_dict(self, hostcname, data, session):
        '''
        Create a scope from a dict
        '''
        new_scope = Scope()
        resp, message = new_scope._check_dict(data)
        if not resp:
            return resp, message
        try:
            host = session.query(Host).filter(Host.cname == hostcname).one()
        except:
            return False, 'Unable to find the host %s' % (hostcname)
        scope_with_same_path = session.query(Scope).\
            filter(Scope.hostcname == hostcname).\
            filter(Scope.path == data['path']).all()
        if len(scope_with_same_path) > 0:
            return False, 'A scope with a same path already exist.'
        for attribute in ['path', 'queryStringModeOn']:
            setattr(new_scope, attribute, data[attribute])
        new_scope.hostcname = hostcname
        new_scope.cacheControlModeInt = data['cacheControlMode']['int']
        new_scope.cacheControlModeExt = data['cacheControlMode']['ext']
        new_scope.cacheControlModeForce = data['cacheControlMode']['force']
        session.add(new_scope)
        session.commit()
        self = new_scope
        return True, self.to_dict(session)

    def update_from_dict(self, id, hostcname, data, session):
        '''
        Update a scope from a dict
        '''
        try:
            scope = session.query(Scope).filter(Scope.id == id)\
                .filter(Scope.hostcname == hostcname).one()
        except:
            return False, 'Unknown scope %d for the host %s' % (id, hostcname)
        resp, message = scope._check_dict(data)
        if not resp:
            return resp, message
        if scope.path == '/' and data['path'] != '/':
            return False, 'Path / can\'t be changed.'
        scope_with_same_path = session.query(Scope).\
            filter(Scope.hostcname == hostcname).\
            filter(Scope.path == data['path']).\
            filter(Scope.id != id).all()
        if len(scope_with_same_path) > 0:
            return False, 'A scope with the same path already exist for this host.'
        for attribute in ['path', 'queryStringModeOn']:
            setattr(scope, attribute, data[attribute])
        scope.cacheControlModeInt = data['cacheControlMode']['int']
        scope.cacheControlModeExt = data['cacheControlMode']['ext']
        scope.cacheControlModeForce = data['cacheControlMode']['force']
        session.add(scope)
        session.commit()
        self = scope
        return True, self.to_dict(session)

class Origin(Base):
    '''
    Origin
    '''
    __tablename__ = 'origins'
    __table_args__ = (
                      {'mysql_engine': 'InnoDB',
                       'mysql_charset': 'utf8'}
                     )
    id = Column('id', Integer, primary_key=True, index=True)
    hostname = Column('hostname', String(255))
    path = Column('path', String(255))
    port = Column('port', Integer)
    protocol = Column('protocol', Enum(*['http', 'https']))
    hostcname = Column(String(10), ForeignKey('hosts.cname'))

    def _check_dict(self, data):
        '''
        Check if the data is valid for an origin. Return a list with the result_boolean and an error message
        '''
        if not isinstance(data, dict):
            return False, 'Wrong format'
        if 'protocol' in data.keys():
            data['protocol'] = data['protocol'].lower()
        required_fields = {
                            'hostname' : {'regexp': '^((([a-zA-Z]|[a-zA-Z][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z]|[A-Za-z][A-Za-z0-9\-]*[A-Za-z0-9])|([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3})$', 'filled': False},
                            'port' : {'regexp': '^0*(?:6553[0-5]|655[0-2][0-9]|65[0-4][0-9]{2}|6[0-4][0-9]{3}|[1-5][0-9]{4}|[1-9][0-9]{1,3}|[0-9])$', 'filled': False},
                            'protocol' : {'regexp': '^(http|https)$', 'filled': False},
                            'path' : {'regexp': '^\/\w*$', 'filled': False},
                        }
        ignored_fields = ['id', 'cname']
        optional_fields = []
        result, message = utils.check_fields(data, ignored_fields, required_fields, optional_fields)
        if not result:
            return result, message
        else:
            return True, ''

    def _create_from_dict(self, cname, data, session):
        '''
        Create an origin from a dict. Should not be called directly.
        '''
        new_orig = Origin()
        resp, message = new_orig._check_dict(data)
        if not resp:
            return resp, message
        try:
            host = session.query(Host).filter(Host.cname == cname).one()
        except:
            return False, 'Unable to find the host %s' % (cname)
        origins = session.query(Origin).\
            filter(Origin.hostname == data['hostname']).\
            filter(Origin.port == data['port']).\
            filter(Origin.protocol == data['protocol']).\
            filter(Origin.path == data['path']).all()
        if len(origins) > 0:
            for origin in origins:
                host_or = session.query(Host).\
                    filter(Host.cname == origin.hostcname).one()
                if host_or.active:
                    return False, 'Origin already exists.'
        for attribute in ['hostname', 'port', 'protocol', 'path']:
            setattr(new_orig, attribute, data[attribute])
        new_orig.hostcname = cname
        session.add(new_orig)
        session.commit()
        self = new_orig
        return True, self.id

    def to_dict(self, session):
        '''
        Return a dict with the host's information to display
        '''
        try:
            self = session.query(Origin).filter(Origin.id == self.id).one()
            display_attributes = ['protocol', 'hostname', 'port','path']
            result = utils.dict_attributes(self, display_attributes)
        except:
            return None
        return result

    def update_from_dict(self, cname, data, session):
        '''
        Update an origin from a dict
        '''
        try:
            updated_origin = session.query(Origin).\
                filter(Origin.hostcname == cname).one()
            resp, message = updated_origin._check_dict(data)
            if not resp:
                return resp, message
            origins = session.query(Origin).\
                filter(Origin.hostname == data['hostname']).\
                filter(Origin.port == data['port']).\
                filter(Origin.protocol == data['protocol']).\
                filter(Origin.path == data['path']).all()
            if len(origins) > 0:
                for origin in origins:
                    host_or = session.query(Host).\
                        filter(Host.cname == origin.hostcname).one()
                    if host_or.active:
                        return False, 'Origin already used.'
            for attribute in ['hostname', 'port', 'protocol', 'path']:
                setattr(updated_origin, attribute, data[attribute])
            session.add(updated_origin)
            session.commit()
            self = updated_origin
            return True, self.id
        except:
            return False, 'Unknown origin'
        return

    def delete(self):
        '''
        Delete the current host
        '''
        return

    def delete_all(self, hostcname, session):
        '''
        Delete an origin for an host
        '''
        origins = session.query(Origin).filter(Origin.hostcname == hostcname).all()
        for origin in origins:
            session.delete(origin)
        session.commit()
        return True, ''

class HwOrigin(Base):
    '''
    HW Origin
    '''
    __tablename__ = 'hw_origins'
    __table_args__ = (
                      {'mysql_engine': 'InnoDB',
                       'mysql_charset': 'utf8'}
                     )
    id = Column('id', Integer, primary_key=True, index=True)
    hw_id = Column('hw_id', Integer, index=True)
    ocdn_id = Column('ocdn_id', Integer, ForeignKey('origins.id',
                                                    onupdate='CASCADE',
                                                    ondelete='CASCADE'))

class HwHost(Base):
    '''
    HW Host'''
    __tablename__ = 'hw_host'
    __table_args__ = (
                      {'mysql_engine': 'InnoDB',
                       'mysql_charset': 'utf8'}
                     )
    id = Column('id', Integer, primary_key=True, index=True)
    host_hash = Column('host_hash', String(20))
    status = Column('status', Enum(*['UNUSED','ACTIVATED', 'SUSPENDED', 'DELETED']))
    cname = Column('cname', String(10))
    scope_id = Column('scope_id', Integer, index=True)
