'''
Models used for analytics
'''

import datetime
import calendar
from collections import OrderedDict


from sqlalchemy import Table, Column, String, DateTime, ForeignKey, Enum, \
    Text, Integer, BigInteger, Boolean, func
from sqlalchemy.orm import aliased, relationship, backref
import sqlalchemy.exc

from models.base import Base
from models.accounts import Account
from models.hosts import Host

class ConsolidatedUsage(Base):
    '''
    ConsolidatedUsage
    '''
    __tablename__ = 'consolidated_usage'
    __table_args__ = (
                      {'mysql_engine': 'InnoDB',
                       'mysql_charset': 'utf8'}
                     )
    id = Column('id', Integer, primary_key=True, index=True)
    accountHash = Column('accountHash', String(10),
                        ForeignKey('accounts.hashCode',
                        onupdate='CASCADE',
                        ondelete='CASCADE'), index=True)
    hostcname = Column(String(10), ForeignKey('hosts.cname'), index=True)
    timestamp = Column('timestamp', Integer, index=True)
    hits = Column('hits', Integer)
    downloaded_bytes = Column('bytes', BigInteger)

    def _make_serie(self, serie_type, key, session, start_date, end_date):
        start_date = datetime.datetime.strptime(start_date, '%Y-%m-%dT%H:%M:00Z')
        end_date = datetime.datetime.strptime(end_date, '%Y-%m-%dT%H:%M:00Z')
        start_timestamp = str(calendar.timegm(start_date.utctimetuple()))
        end_timestamp = str(calendar.timegm(end_date.utctimetuple()))
        serie =  OrderedDict({})
        serie["serie_type"] = serie_type.upper()
        serie["key"] = key
        serie["metrics"] = ["timestamp", "hits", "Bytes transfered", "Bytes/s"]
        serie["datapoints"] = []
        if serie_type == 'account':
            data = session.query(ConsolidatedUsage.timestamp.label('timestamp'), \
                func.sum(ConsolidatedUsage.hits).label('hits'), \
                func.sum(ConsolidatedUsage.downloaded_bytes).label('downloaded_bytes')).\
                filter(ConsolidatedUsage.timestamp>start_timestamp).\
                filter(ConsolidatedUsage.timestamp<=end_timestamp).\
                filter(ConsolidatedUsage.accountHash==key).\
                group_by(ConsolidatedUsage.timestamp).all()
        if serie_type == 'host':
            data = session.query(ConsolidatedUsage).\
                filter(ConsolidatedUsage.timestamp>start_timestamp).\
                filter(ConsolidatedUsage.timestamp<=end_timestamp).\
                filter(ConsolidatedUsage.hostcname==key).all()
        for entry in data:
            bdw = int(entry.downloaded_bytes/300)
            serie["datapoints"].append([entry.timestamp, int(entry.hits), int(entry.downloaded_bytes), int(bdw)])
        return serie

    def get_analytics(self, start_date, end_date, session, account, group_by='account', host=None):
        resp = {'series': []}
        if host:
            serie = self._make_serie('host', host, session, start_date, end_date)
            resp['series'].append(serie)
            return resp
        if group_by == 'account':
            serie = self._make_serie('account', account, session, start_date, end_date)
            resp['series'].append(serie)
            return resp
        if group_by == "host":
            hosts = session.query(Host).filter(Host.accountHash==account).all()
            for host in hosts:
                serie = self._make_serie('host', host.cname, session, start_date, end_date)
                resp['series'].append(serie)
            return resp
        return resp

class RealTimeUsage(Base):
    __tablename__ = 'realtime_usage'
    __table_args__ = (
                      {'mysql_engine': 'InnoDB',
                       'mysql_charset': 'utf8'}
                     )
    id = Column('id', Integer, primary_key=True, index=True)
    accountHash = Column('accountHash', String(10),
                        ForeignKey('accounts.hashCode',
                        onupdate='CASCADE',
                        ondelete='CASCADE'), index=True)
    hostcname = Column(String(10), ForeignKey('hosts.cname'), index=True)
    timestamp = Column('timestamp', Integer, index=True)
    hits = Column('hits', Integer)
    bandwidth = Column('bandwidth', BigInteger)

    def _make_serie(self, serie_type, key, session, start_date, end_date):
        start_date = datetime.datetime.strptime(start_date, '%Y-%m-%dT%H:%M:00Z')
        end_date = datetime.datetime.strptime(end_date, '%Y-%m-%dT%H:%M:00Z')
        start_timestamp = str(calendar.timegm(start_date.utctimetuple()))
        end_timestamp = str(calendar.timegm(end_date.utctimetuple()))
        serie =  OrderedDict({})
        serie["serie_type"] = serie_type.upper()
        serie["key"] = key
        serie["metrics"] = ["timestamp", "hits", "Bytes transfered", "Bytes/s"]
        serie["datapoints"] = []
        if serie_type == 'account':
            data = session.query(RealTimeUsage.timestamp.label('timestamp'), \
                func.sum(RealTimeUsage.hits).label('hits'), \
                func.sum(RealTimeUsage.bandwidth).label('bandwidth')).\
                filter(RealTimeUsage.timestamp>start_timestamp).\
                filter(RealTimeUsage.timestamp<=end_timestamp).\
                filter(RealTimeUsage.accountHash==key).\
                group_by(RealTimeUsage.timestamp).all()
        if serie_type == 'host':
            data = session.query(RealTimeUsage).\
                filter(RealTimeUsage.timestamp>start_timestamp).\
                filter(RealTimeUsage.timestamp<=end_timestamp).\
                filter(RealTimeUsage.hostcname==key).all()
        for entry in data:
            serie["datapoints"].append([entry.timestamp, int(entry.hits), '', int(entry.bandwidth)])
        return serie

    def get_analytics(self, start_date, end_date, session, account, group_by='account', host=None):
        resp = {'series': []}
        if host:
            serie = self._make_serie('host', host, session, start_date, end_date)
            resp['series'].append(serie)
            return resp
        if group_by == 'account':
            serie = self._make_serie('account', account, session, start_date, end_date)
            resp['series'].append(serie)
            return resp
        if group_by == "host":
            hosts = session.query(Host).filter(Host.accountHash==account).all()
            for host in hosts:
                serie = self._make_serie('host', host.cname, session, start_date, end_date)
                resp['series'].append(serie)
            return resp
        return resp
