#!/usr/bin/env python
from common import amqp
import ConfigParser

import requests
import memcache
import json
import copy
import string
import random

from test.unit.utils import *
from models.base import *
from models.services import Service
from models.hosts import Host, Origin, Scope, Hostname, CdxApp
import backend

token_endpoint = 'http://127.0.0.1:8080/auth/token'
api_base_url = 'http://127.0.0.1:8080/'

def parse_config(config_file, logger=None):
    '''
    Parse config from config file. Return global config.
    '''
    try:
        f = open(config_file, 'r')
    except IOError:
        logger.error('Unable to find %s', config_file)
    conf_dict = {}
    conf = ConfigParser.ConfigParser()
    conf.readfp(f)
    sections = ['DEFAULT']
    for section in sections:
        for k, v in conf.items(section):
            conf_dict[k] = v
    return conf_dict

def push_to_amqp(datafeed, conf):
    '''
    Submit a message to amqp for processing
    '''
    publisher = amqp.Publisher(conf)
    publisher.produce(datafeed)

def generate_uniq_hashCode(size):
    ''' Generate a uniq hash and check the DB '''
    chars = string.ascii_lowercase + string.digits
    return ''.join(random.choice(chars) for _ in range(size))

def test_Host_POST_main():
    '''
    Test POST /api/v1/accounts/{account_hash}/hosts
    '''
    random = generate_uniq_hashCode(10)
    endpoint_base = api_base_url + '/api/v1/accounts/'
    token =  request_token('dummy', 'dummy', token_endpoint)
    headers = {
                    'Accept' : 'application/json',
                    'Content-type' : 'application/json',
                    'Authorization': 'Bearer %s' % str(token['access_token'])
                  }
    data = {"name": random,
            "scopes": [{"path": "/", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}],
            "services": [{"id": 1,}],
            "origin": {"hostname": 'www.'+ random + "orig.com", "port": 80, "protocol": "HTTP", "path": "/"},
            "hostnames": [random + ".ApiPostConf.tld"]}
    r = requests.post(endpoint_base + 'AAAAAAAA/hosts', \
        json.dumps(data), headers=headers)
    print r.status_code
    print r.text

def test_Origin_update(cname):
    '''
    Update an origin
    '''
    random = generate_uniq_hashCode(5)
    endpoint_base = api_base_url + '/api/v1/accounts/'
    token =  request_token('dummy', 'dummy', token_endpoint)
    headers = {
                'Accept' : 'application/json',
                'Content-type' : 'application/json',
                'Authorization': 'Bearer %s' % str(token['access_token'])
    }
    url = '%sAAAAAAAA/hosts/%s/origin' % (endpoint_base, cname)
    data = {"hostname": random + ".ApiPutOrigin.net", "port": 8080, "protocol": "HTTP", "path": "/"}
    r = requests.put(url, json.dumps(data), headers=headers)
    print r.status_code
    print r.text

def test_Hostname_POST(cname):
    '''
    Create a host
    '''
    random = generate_uniq_hashCode(5)
    endpoint_base = api_base_url + '/api/v1/accounts/'
    token =  request_token('dummy', 'dummy', token_endpoint)
    headers = {
                'Accept' : 'application/json',
                'Content-type' : 'application/json',
                'Authorization': 'Bearer %s' % str(token['access_token'])
    }
    data = random + ".piost.net"
    url = '%sAAAAAAAA/hosts/%s/hostnames' % (endpoint_base, cname)
    r = requests.post(url, json.dumps(data), headers=headers)
    print r.status_code
    print r.text

def test_Hostname_DELETE(cname, hostname):
    '''
    Create a host
    '''
    random = generate_uniq_hashCode(5)
    endpoint_base = api_base_url + '/api/v1/accounts/'
    token =  request_token('dummy', 'dummy', token_endpoint)
    headers = {
                'Accept' : 'application/json',
                'Content-type' : 'application/json',
                'Authorization': 'Bearer %s' % str(token['access_token'])
    }
    url = '%sAAAAAAAA/hosts/%s/hostnames/%s' % (endpoint_base, cname, hostname)
    r = requests.delete(url, headers=headers)
    print r.status_code
    print r.text

def test_Scope_POST(cname):
    '''
    Create a new scope
    '''
    random = generate_uniq_hashCode(5)
    endpoint_base = api_base_url + '/api/v1/accounts/'
    token =  request_token('dummy', 'dummy', token_endpoint)
    headers = {
                'Accept' : 'application/json',
                'Content-type' : 'application/json',
                'Authorization': 'Bearer %s' % str(token['access_token'])
    }
    url = '%sAAAAAAAA/hosts/%s/scopes' % (endpoint_base, cname)
    data = {"path": "/" + random, "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}
    r = requests.post(url, json.dumps(data), headers=headers)
    print r.status_code
    print r.text

def test_Scope_PUT(cname, id):
    '''
    Create a new scope
    '''
    random = generate_uniq_hashCode(5)
    endpoint_base = api_base_url + '/api/v1/accounts/'
    token =  request_token('dummy', 'dummy', token_endpoint)
    headers = {
                'Accept' : 'application/json',
                'Content-type' : 'application/json',
                'Authorization': 'Bearer %s' % str(token['access_token'])
    }
    url = '%sAAAAAAAA/hosts/%s/scopes/%s' % (endpoint_base, cname, id)
    data = {"path": "/" + random, "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}
    r = requests.put(url, json.dumps(data), headers=headers)
    print r.status_code
    print r.text

def test_Scope_DELETE(cname, id):
    '''
    Create a new scope
    '''
    random = generate_uniq_hashCode(5)
    endpoint_base = api_base_url + '/api/v1/accounts/'
    token =  request_token('dummy', 'dummy', token_endpoint)
    headers = {
                'Accept' : 'application/json',
                'Content-type' : 'application/json',
                'Authorization': 'Bearer %s' % str(token['access_token'])
    }
    url = '%sAAAAAAAA/hosts/%s/scopes/%s' % (endpoint_base, cname, id)
    r = requests.delete(url, headers=headers)
    print r.status_code
    print r.text


conf = parse_config('etc/cdn_api.ini')
#push_to_amqp({'object_type': 'application', 'action': 'add'}, conf)

#push_to_amqp({'object_type': 'hostname', 'action': 'add', 'id': 61}, conf)
#push_to_amqp({'object_type': 'hostname', 'action': 'add', 'hostname': 'toto.com', 'cname': ''}, conf)
#push_to_amqp({'object_type': 'hostname', 'action': 'delete', 'hostname': 'toto.com', 'cname': ''}, conf)

#push_to_amqp({'object_type': 'scope', 'action': 'add', 'id': 72}, conf)
#push_to_amqp({'object_type': 'scope', 'action': 'delete', 'id': 72, 'cname': 'vnl8yqww5x'}, conf)
#push_to_amqp({'object_type': 'scope', 'action': 'delete', 'id': 72, 'cname': 'vnl8yqww5x'}, conf)

#push_to_amqp({'object_type': 'hostname', 'action': 'add', 'id': 72}, conf)
#push_to_amqp({'object_type': 'hostname', 'action': 'delete', 'cname': 'x54edtmidg', 'hostname': 'www.asstesthostnames.eu'}, conf)

#push_to_amqp({'object_type': 'origin', 'action': 'add', 'id': 24}, conf)
#push_to_amqp({'object_type': 'origin', 'action': 'update', 'id': 61}, conf)

#test_Host_POST_main()
test_Origin_update('x54edtmidg')
#test_Hostname_POST('x54edtmidg')
#test_Hostname_DELETE('x54edtmidg', 'p2svw.piost.net')
#test_Scope_POST('x54edtmidg')
#test_Scope_PUT('x54edtmidg', 77)
#test_Scope_DELETE('x54edtmidg', 77)


#push_to_amqp({'object_type': 'hwhost', 'action': 'add'}, conf)
#push_to_amqp({'object_type': 'hwscope', 'action': 'add', 'id': 29}, conf)
