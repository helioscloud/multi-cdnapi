import MySQLdb
import requests
import copy
from models.base import *
from models.accounts import Contact, Account
from models.services import Service
from models.users import RoleList, RoleMatrix, User
from models.hosts import Host, CdxApp
import yaml


def load_conf():
    f = open('conf.yml', 'r')
    return yaml.load(f.read())


def request_token(username, password, token_endpoint):
    '''
    Request a token for a user, return the full token
    '''
    data = '{"grant_type": "password","username": "%s","password": "%s"}' % (username, password)
    r = requests.post(token_endpoint, data, headers={
      "Accept": "application/json"
    })
    if (r.status_code == 200):
        return r.json()
    else:
        return False


def drop_create_db(db_hostname, db_user, db_password, db_mysql):
    '''
    Drop and create again the db.
    '''
    db = MySQLdb.connect(host=db_hostname, user=db_user, passwd=db_password, \
        db=db_mysql)
    cursor = db.cursor()
    #Mysql we delete the sleeping process
    sql_query = 'SHOW FULL PROCESSLIST'
    cursor.execute(sql_query)
    for row in cursor.fetchall():
        if (row[3] == db_mysql and row[4] == 'Sleep'):
            cursor2 = db.cursor()
            sql_query = 'KILL %d' % (row[0])
            cursor2.execute(sql_query)
    drop_query = "DROP DATABASE IF EXISTS %s; " % (db_mysql)
    create_query = "CREATE DATABASE %s; " % (db_mysql)
    for sql_query in (drop_query, create_query):
        cursor.execute(sql_query)


def roles_populate():
    '''
    Generate a array of dict with all possible RoleList
    '''
    roles = [{'account':'EDIT', },{'account':'READ', },{'account':'NONE' }]
    for value in ['configuration', 'report', 'content']:
        current_roles = copy.deepcopy(roles)
        for permission in ['EDIT', 'READ', 'NONE']:
            for i in range(0, len(current_roles)):
                if roles[i].get(value):
                    new_role = dict(roles[i])
                    new_role[value] = permission
                    roles.append(new_role)
                else:
                    roles[i][value] = permission
    return roles


def role_matrix_populate(db):
    '''
    Insert in the db all possible RoleMatrix. RoleList table shoud have been
    populated before
    '''
    session = db_reconnect(db)
    for account_role in range(1, 82):
        for sub_account_role in range(1, 82):
            role_matrix = RoleMatrix()
            role_matrix.userAccount = account_role
            role_matrix.subAccounts = sub_account_role
            session.add(role_matrix)
    session.commit()


def initialize_db(db):
    '''
    Generate tables according the models and insert default values
    '''
    session = db_reconnect(db)
    service = session.query(Service).filter(Service.id == 1)
    roles_data = roles_populate()
    _add_object(roles_data, RoleList, session)
    role_matrix_populate(db)
    services = [
        {   'name' : 'MonoCDN',
            'description' : 'MonoCDN Service',
            'multiCdnService' : False
        },
        {   'name' : 'CDN Global',
            'description' : 'Global CDN MAP',
            'multiCdnService' : False
        },
        {   'name' : 'CDN Premium',
            'description' : 'Premium CDN MAP',
            'multiCdnService' : False
        },
    ]
    _add_object(services, Service, session)


def feed_token_data(db):
    '''
    Add in the DB, the data needed for token tests
    '''
    session = db_reconnect(db)
    contacts = [{'id' : 1}]
    accounts =[{'hashCode' : 'FFFFFFFFFF', 'accountName' : 'TestAccount',
        'accountStatus' :  'ACTIVATED', 'primaryContact' : 1,
        'billingContact' : 1, 'technicalContact' : 1,
        'maximumDirectSubAccounts' : 200, 'subAccountCreationEnabled' : 1}]
    users = [{'accountHash' : 'FFFFFFFFFF', 'username' : 'dummy',
        'password' : 'b5a2c96250612366ea272ffac6d9744aaf4b45aacd96aa7cfcb931ee3b558259',
        'firstName' : 'firstname', 'lastName': 'lastname',
        'email' : 'dummy@nowhere.com', 'phone' : '+33111111111',
        'fax' : '+33111111111', 'authorizedSupportContact' : 1, 'roles' : 1
    }]
    _add_object(contacts, Contact, session)
    _add_object(accounts, Account, session)
    _add_object(users, User, session)


def feed_get_account(db):
    '''
    Add in the DB, the data needed for account tests
    '''
    session = db_reconnect(db)
    services = []
    contacts = [{'id' : 1},]
    accounts =[
        {'hashCode' : 'AAAAAAAA', 'accountName' : 'ParentAccount',
        'accountStatus' :  'ACTIVATED', 'primaryContact' : 1,
        'billingContact' : 1, 'technicalContact' : 1,
        'maximumDirectSubAccounts' : 200, 'subAccountCreationEnabled' : 1},
        {'hashCode' : 'BBBBBBBB', 'accountName' : 'ChildAccount',
        'accountStatus' :  'ACTIVATED', 'primaryContact' : 1,
        'billingContact' : 1, 'technicalContact' : 1, 'parentId' :1,
        'maximumDirectSubAccounts' : 200, 'subAccountCreationEnabled' : 1},
        {'hashCode' : 'CCCCCCCC', 'accountName' : 'StandaloneAccount',
        'accountStatus' :  'ACTIVATED', 'primaryContact' : 1,
        'billingContact' : 1, 'technicalContact' : 1,
        'maximumDirectSubAccounts' : 200, 'subAccountCreationEnabled' : 1},
        {'hashCode' : 'DDDDDDDD', 'accountName' : 'ChildAccount',
        'accountStatus' :  'DELETED', 'primaryContact' : 1,
        'billingContact' : 1, 'technicalContact' : 1, 'parentId' :1,
        'maximumDirectSubAccounts' : 200, 'subAccountCreationEnabled' : 1},
        {'hashCode' : 'EEEEEEEE', 'accountName' : 'ChildAccount',
        'accountStatus' :  'SUSPENDED', 'primaryContact' : 1,
        'billingContact' : 1, 'technicalContact' : 1, 'parentId' :1,
        'maximumDirectSubAccounts' : 200, 'subAccountCreationEnabled' : 1},
        ]
    users = [{'accountHash' : 'AAAAAAAA', 'username' : 'dummy',
        'password' : 'b5a2c96250612366ea272ffac6d9744aaf4b45aacd96aa7cfcb931ee3b558259',
        'firstName' : 'firstname', 'lastName': 'lastname',
        'email' : 'dummy@nowhere.com', 'phone' : '+33111111111',
        'fax' : '+33111111111', 'authorizedSupportContact' : 1, 'roles' : 1
    }]

    _add_object(contacts, Contact, session)
    _add_object(accounts, Account, session)
    _add_object(users, User, session)
    account = session.query(Account).filter(Account.id == 1).one()
    for service_id in [1,3]:
        _add_service_to_account('AAAAAAAA', service_id, session)


def feed_put_account(db):
    '''
    Add some contacts and an account for the put account tests
    '''
    session = db_reconnect(db)
    contacts = [{'id' : 15, 'firstName': 'HPfirstName', 'lastName': 'HPlastName',\
        'email': 'HPemail', 'phone': 'HPphone', 'fax': 'HPfax'},
        {'id' : 16, 'firstName': 'HBfirstName', 'lastName': 'HBlastName',\
        'email': 'HBemail', 'phone': 'HBphone', 'fax': 'HBfax'},
        {'id' : 17, 'firstName': 'HTfirstName', 'lastName': 'HTlastName',\
        'email': 'HTemail', 'phone': 'HTphone', 'fax': 'HTfax'},
        ]
    accounts = [{'hashCode' : 'HHHHHHHH', 'accountName' : 'differentContacts',
    'accountStatus' :  'ACTIVATED', 'primaryContact' : 15,
    'billingContact' : 16, 'technicalContact' : 17, 'parentId' :1,
    'maximumDirectSubAccounts' : 200, 'subAccountCreationEnabled' : 1},
    {'hashCode' : 'IIIIIIII', 'accountName' : 'differentContacts',
    'accountStatus' :  'ACTIVATED', 'primaryContact' : 15,
    'billingContact' : 16, 'technicalContact' : 17, 'parentId' :1,
    'maximumDirectSubAccounts' : 200, 'subAccountCreationEnabled' : 1},
    {'hashCode' : 'JJJJJJJJ', 'accountName' : 'differentContacts',
    'accountStatus' :  'ACTIVATED', 'primaryContact' : 15,
    'billingContact' : 16, 'technicalContact' : 17, 'parentId' :1,
    'maximumDirectSubAccounts' : 200, 'subAccountCreationEnabled' : 1},
    ]
    _add_object(contacts, Contact, session)
    _add_object(accounts, Account, session)
    _add_service_to_account('HHHHHHHH', 1, session)
    _add_service_to_account('IIIIIIII', 3, session)
    _add_service_to_account('JJJJJJJJ', 3, session)


def feed_get_services(db):
    session = db_reconnect(db)
    accounts = [{'hashCode' : 'KKKKKKKK', 'accountName' : 'differentContacts',
    'accountStatus' :  'ACTIVATED', 'primaryContact' : 1,
    'billingContact' : 1, 'technicalContact' : 1, 'parentId' :1,
    'maximumDirectSubAccounts' : 200, 'subAccountCreationEnabled' : 1}]
    _add_object(accounts, Account, session)
    _add_service_to_account('KKKKKKKK', 1, session)


def feed_get_user(db):
    session = db_reconnect(db)
    users = [{'accountHash' : 'AAAAAAAA', 'username' : 'Nopermission',
        'password' : 'b5a2c96250612366ea272ffac6d9744aaf4b45aacd96aa7cfcb931ee3b558259',
        'firstName' : 'Nofirstname', 'lastName': 'Nolastname',
        'email' : 'nopermission@nowhere.com', 'phone' : '+33111111111',
        'fax' : '+33111111111', 'authorizedSupportContact' : 1, 'roles' : 6561
    },{'accountHash' : 'CCCCCCCC', 'username' : 'Cuser',
        'password' : 'b5a2c96250612366ea272ffac6d9744aaf4b45aacd96aa7cfcb931ee3b558259',
        'firstName' : 'Cfirstname', 'lastName': 'Colastname',
        'email' : 'nopermission@nowhere.com', 'phone' : '+33111111111',
        'fax' : '+33111111111', 'authorizedSupportContact' : 1, 'roles' : 1
    },{'accountHash' : 'BBBBBBBB', 'username' : 'Buser',
        'password' : 'b5a2c96250612366ea272ffac6d9744aaf4b45aacd96aa7cfcb931ee3b558259',
        'firstName' : 'Bfirstname', 'lastName': 'Bolastname',
        'email' : 'nopermission@nowhere.com', 'phone' : '+33111111111',
        'fax' : '+33111111111', 'authorizedSupportContact' : 1, 'roles' : 1
    }]
    _add_object(users, User, session)


def feed_delete_user(db):
    session = db_reconnect(db)
    users = [{'accountHash' : 'AAAAAAAA', 'username' : 'Nopermission2',
        'password' : 'b5a2c96250612366ea272ffac6d9744aaf4b45aacd96aa7cfcb931ee3b558259',
        'firstName' : 'Nofirstname', 'lastName': 'Nolastname',
        'email' : 'nopermission@nowhere.com', 'phone' : '+33111111111',
        'fax' : '+33111111111', 'authorizedSupportContact' : 1, 'roles' : 6561
    }]
    _add_object(users, User, session)


def feed_hosts(db):
    '''
    Add some hosts for hosts and scope tests
    '''
    session = db_reconnect(db)
    hosts = [{'accountHash': 'AAAAAAAA', 'cname': 'dummyhost', 'name': 'dummyhost'}]
    _add_object(hosts, Host, session)
    cdx_app = [{'cname': 'cdx-app-1', 'status':'UNUSED'},
            {'cname': 'cdx-app-2', 'status':'UNUSED'},
            {'cname': 'cdx-app-3', 'status':'UNUSED'},
            {'cname': 'cdx-app-4', 'status':'UNUSED'},
            {'cname': 'cdx-app-5', 'status':'UNUSED'},
            {'cname': 'cdx-app-6', 'status':'UNUSED'},
            {'cname': 'cdx-app-7', 'status':'UNUSED'},
            {'cname': 'cdx-app-8', 'status':'UNUSED'},
            {'cname': 'cdx-app-9', 'status':'UNUSED'},
            {'cname': 'cdx-app-10', 'status':'UNUSED'},
            {'cname': 'cdx-app-11', 'status':'UNUSED'},
            {'cname': 'cdx-app-12', 'status':'UNUSED'},
            {'cname': 'cdx-app-13', 'status':'UNUSED'},
            {'cname': 'cdx-app-14', 'status':'UNUSED'},
            {'cname': 'cdx-app-15', 'status':'UNUSED'},
            {'cname': 'cdx-app-16', 'status':'UNUSED'},
            {'cname': 'cdx-app-17', 'status':'UNUSED'},
    ]
    _add_object(cdx_app, CdxApp, session)


def clean_hw_hosts(db, hw):
    '''
    Clean all hosts added to the hw account
    '''
    session = db_reconnect(db)
    hosts = session.query(Host).all()
    for host in hosts:
        hw.del_host(host.cname)


def _add_service_to_host(cname, service_id, session):
    host = session.query(Host).filter(Host.cname == cname).one()
    service = session.query(Service).filter(Service.id == service_id).one()
    host.services.append(service)
    session.add(host)
    session.commit()


def _add_service_to_account(account_hashcode, serviice_id, session):
    account = session.query(Account).filter(Account.hashCode == account_hashcode).one()
    service = session.query(Service).filter(Service.id == serviice_id).one()
    account.services.append(service)
    session.add(account)
    session.commit()


def _add_object(data, class_name, session):
    '''
    Add the data in the DB
    '''
    for item in data:
        obj = class_name()
        for key in item.keys():
            setattr(obj, key, item[key])
        session.add(obj)
    session.commit()
