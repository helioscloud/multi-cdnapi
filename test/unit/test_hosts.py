import unittest

import requests
import memcache
import json
import copy

from unit import utils
from models.base import *
from models.services import Service
from models.hosts import Host, Origin, Scope, Hostname, CdxApp

'''
The database will be DROPPED in the process. MYSQL sleeping process
will be KILLED
'''
db_hostname = '127.0.0.1'
db_user = 'api'
db_password = 'password'
db_mysql = 'optimicdnapi'
db = 'mysql+mysqldb://api:password@127.0.0.1/optimicdnapi'
token_endpoint = 'http://127.0.0.1:8080/auth/token'
api_base_url = 'http://127.0.0.1:8080/'
memcache_servers = ['127.0.0.1:11211']


class TestObjects(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        utils.drop_create_db(db_hostname, db_user, db_password, db_mysql)
        utils.initialize_db(db)
        utils.feed_get_account(db)
        utils.feed_hosts(db)
        cls.session = db_reconnect(db)

    def test_Hostname_check_dict(self):
        data = [{'load': 'www.mydomain.tdd', 'expected': True},
                {'load': 'tdd_', 'expected': False},
                {'load': '1.1.1.1', 'expected': True},
        ]
        hostname = Hostname()
        for to_check in data:
            resp, message = hostname._check_dict(to_check['load'])
            self.assertEqual(resp, to_check['expected'])

    def test_Hostname_create_from_dict(self):
        hostcname = 'dummyhost'
        hostname = Hostname()
        data = [{'load': 'www1.mydomain.tld', 'expected': True},
                {'load': 'www1.mydomain.tld', 'expected': False},
                {'load': 'www2.mydomain.tld', 'expected': True}
        ]
        for to_check in data:
            resp, message = hostname.create_from_dict(hostcname, to_check['load'], self.session)
            self.assertEqual(resp, to_check['expected'])

    def test_Hostname_delete_all(self):
        hostname = Hostname()
        hostcname = 'dummyhost'
        for i in range(3,5):
            data = 'www%s.mydomain.tld' % (i)
            resp, message = hostname.create_from_dict(hostcname, data, self.session)
            self.assertTrue(resp)
        resp, message = hostname.delete_all(hostcname, self.session)
        self.assertTrue(resp)
        hostnames = self.session.query(Hostname).filter(Hostname.hostcname == hostcname).all()
        self.assertEqual(len(hostnames), 0)

    def test_Hostname_delete(self):
        hostname = Hostname()
        hostcname = 'dummyhost'
        resp, message = hostname.create_from_dict(hostcname, 'myhostname-to-delete.com', self.session)
        resp, message = hostname.delete(hostcname, 'myhostname-to-delete.com', self.session)
        self.assertTrue(resp)
        resp, message = hostname.delete(hostcname, 'myhostname-to-delete.com', self.session)
        self.assertFalse(resp)

    def test_Host_check_dict(self):
        load = {"name": "MydomainMainConf",
                "scopes": [{"path": "/", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}],
                "services": [{"id": 2,}],
                "origin": {"hostname": "orig.mydomain.tld", "port": 80, "protocol": "HTTP", "path": "/"},
                "hostnames": ["www.mydomain.tld"]}
        host = Host()
        resp, message = host._check_dict(load)
        self.assertEqual(resp, True)
        load2 = copy.deepcopy(load)
        load2["hostnames"] = 'mydomain.tld'
        resp, message = host._check_dict(load2)
        self.assertEqual(resp, False)
        load2 = copy.deepcopy(load)
        load2["hostnames"] = ['ty_!']
        resp, message = host._check_dict(load2)
        self.assertEqual(resp, False)
        load2 = copy.deepcopy(load)
        load2["scopes"] = 'ty_!'
        resp, message = host._check_dict(load2)
        self.assertEqual(resp, False)
        load2 = copy.deepcopy(load)
        del load2["origin"]
        resp, message = host._check_dict(load2)
        self.assertEqual(resp, False)
        load2 = copy.deepcopy(load)
        load2["origin"] = 'ty_!'
        resp, message = host._check_dict(load2)
        self.assertEqual(resp, False)

    def test_Host_to_dict(self):
        accountHash = 'AAAAAAAA'
        load = {"name": "MydomainMainConfToDict",
                "scopes": [{"path": "/", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}],
                "services": [{"id": 1,}],
                "origin": {"hostname": "todict-origin.mydomain.tld", "port": 80, "protocol": "HTTP", "path": "/"},
                "hostnames": ["todict.mydomain.tld"]}
        host = Host()
        resp, message = host.create_from_dict(accountHash, load, self.session)
        self.assertTrue(resp)
        self.assertTrue(isinstance(int(message['id']), int))
        del(message['id'])
        self.assertTrue(isinstance(int(message['scopes'][0]['id']), int))
        del(message['scopes'][0]['id'])
        expected = '{"cname": "cdx-app-5", "name": "MydomainMainConfToDict", "accountHash": "AAAAAAAA", "services": [{"id": 1, "name": "MonoCDN", "description": "MonoCDN Service", "multiCdnService": false}], "scopes": [{"path": "/", "queryStringModeOn": true, "cacheControlMode": {"cacheControlModeInt": 86400, "cacheControlModeExt": 3600, "cacheControlModeForce": false}}], "origin": {"protocol": "http", "hostname": "todict-origin.mydomain.tld", "port": 80, "path": "/"}, "hostnames": ["todict.mydomain.tld"]}'
        self.assertEqual(json.dumps(message), expected)

    def test_Host_get_hostnames(self):
        hostcname = 'dummyhost'
        hostname = Hostname()
        hostname.delete_all(hostcname, self.session)
        for i in range(3,5):
            data = 'wwwTT%s.mydomain.tld' % (i)
            resp, message = hostname.create_from_dict(hostcname, data, self.session)
            self.assertTrue(resp)
        host = self.session.query(Host).filter(Host.cname == hostcname).one()
        hostnames = host.get_hostnames(self.session)
        self.assertTrue(isinstance(hostnames, list))
        self.assertEqual(len(hostnames), 2)
        for i in range(3,5):
            data = 'wwwTT%s.mydomain.tld' % (i)
            self.assertTrue(data in hostnames)
        hostname.delete_all(hostcname, self.session)

    def test_Host_create_from_dict(self):
        accountHash = 'AAAAAAAA'
        load = {"name": "MydomainMainConf",
                "scopes": [{"path": "/", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}],
                "services": [{"id": 1,}],
                "origin": {"hostname": "orig.mydomain.tld", "port": 80, "protocol": "HTTP", "path": "/"},
                "hostnames": ["wwwRR.mydomain.tld"]}
        host = Host()
        resp, message = host.create_from_dict('wrong', load, self.session)
        self.assertFalse(resp)
        resp, message = host.create_from_dict(accountHash, load, self.session)
        message['services']
        self.assertTrue(resp)
        self.assertEqual(len(message['services']), 1)
        self.assertEqual(message['services'][0]['id'], 1)
        resp, message = host.create_from_dict(accountHash, load, self.session)
        self.assertEqual(message, 'Hostname wwwRR.mydomain.tld already used.')
        self.assertFalse(resp)
        del(load['hostnames'])
        resp, message = host.create_from_dict(accountHash, load, self.session)
        self.assertFalse(resp)
        self.assertEqual(message, 'Origin already exists.')
        load['origin'] = {"hostname": "orig2.mydomain.tld", "port": 80, "protocol": "HTTP", "path": "/"}
        load['services'] = [{"id": 2595}]
        resp, message = host.create_from_dict(accountHash, load, self.session)
        self.assertEqual(len(message['services']), 2)
        self.assertEqual(message['services'][0]['id'], 1)
        self.assertEqual(message['services'][1]['id'], 3)
        self.assertTrue(resp)
        del(load['scopes'])
        load['origin'] = {"hostname": "orig3.mydomain.tld", "port": 80, "protocol": "HTTP", "path": "/"}
        resp, message = host.create_from_dict(accountHash, load, self.session)
        self.assertTrue(resp)
        self.assertEqual(len(message['scopes']), 1)
        load = {"name": "without_root",
                "scopes": [
                    {"path": "/images", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}},
                    {"path": "/videos", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}},
                ],
                "services": [{"id": 1,}],
                "origin": {"hostname": "withoutroot.UpdateConf2.tdl", "port": 80, "protocol": "http", "path": "/"},
                "hostnames": ["www.withoutroot.tld"]}
        resp, message = host.create_from_dict(accountHash, load, self.session)
        self.assertTrue(resp)
        self.assertEqual(len(message['scopes']), 3)
        self.assertEqual(message['scopes'][2]['path'], '/')
        load = {
                "scopes": [{"path": "/", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}],
                "services": [{"id": 1,}],
                "origin": {"hostname": "without-name.mydomain.tld", "port": 80, "protocol": "HTTP", "path": "/"},
                "hostnames": ["without-name.mydomain.tld"]}
        resp, message = host.create_from_dict(accountHash, load, self.session)
        self.assertFalse(resp)
        self.assertEqual(message, 'name is missing.')

    def test_Host_update_from_dict(self):
        accountHash = 'AAAAAAAA'
        load = {"name": "UpdateFromDictConf",
                "scopes": [{"path": "/", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}],
                "services": [{"id": 1,}],
                "origin": {"hostname": "orig.UpdateFromDictConf.tld", "port": 80, "protocol": "HTTP", "path": "/"},
                "hostnames": ["www.UpdateFromDictConf.tld"]}
        host = Host()
        resp, message = host.create_from_dict(accountHash, load, self.session)
        cname = message['cname']
        resp, message = host.update_from_dict(accountHash, cname, load, self.session)
        self.assertFalse(resp)
        self.assertEqual(message, 'Only name and services can be updated. Origin, Scopes and Hostnames should be updated with the dedicated call.')
        load = {"name": "UpdateFromDictConf2"}
        resp, message = host.update_from_dict(accountHash, cname, load, self.session)
        self.assertTrue(resp)
        services_json = '[{"id": 1, "name": "MonoCDN", "description": "MonoCDN Service", "multiCdnService": false}, {"id": 3, "name": "CDN Premium", "description": "Premium CDN MAP", "multiCdnService": false}]'
        self.assertEqual(json.dumps(message['services']), services_json)
        self.assertEqual(message['name'], 'UpdateFromDictConf2')
        del(load['name'])
        resp, message = host.update_from_dict(accountHash, cname, load, self.session)
        self.assertFalse(resp)
        self.assertEqual(message, 'name is missing.')
        load['name'] = "UpdateFromDictConf3"
        load["services"] = [{"id": 3}]
        resp, message = host.update_from_dict(accountHash, cname, load, self.session)
        self.assertTrue(resp)
        self.assertEqual(message['name'], "UpdateFromDictConf3")
        self.assertEqual(len(message['services']), 1)
        self.assertEqual(message['services'][0]['id'], 3)

    def test_Scope_check_dict(self):
        data = [{'load': {"path": "/", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}, 'expected': True},
                {'load': {"queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}, 'expected': False},
                {'load': {"path": "22", "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}, 'expected': False},
                {'load': {"path": "/", "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}, 'expected': False},
                {'load': {"path": "/", "queryStringModeOn": 'ff', "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}, 'expected': False},
                {'load': {"path": "/", "queryStringModeOn": True}, 'expected': False},
                {'load': {"path": "/", "queryStringModeOn": True, "cacheControlMode": {"ext": 3600, "force": False}}, 'expected': False},
                {'load': {"path": "/", "queryStringModeOn": True, "cacheControlMode": {"int": 'rr', "ext": 3600, "force": False}}, 'expected': False},
                {'load': {"path": "/", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "force": False}}, 'expected': False},
                {'load': {"path": "/", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 'rr', "force": False}}, 'expected': False},
                {'load': {"path": "/", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600}}, 'expected': False},
                {'load': {"path": "/", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": 'dd'}}, 'expected': False},
        ]
        scope = Scope()
        for to_check in data:
            resp, message = scope._check_dict(to_check['load'])
            self.assertEqual(resp, to_check['expected'])

    def test_Scope_create_from_dict(self):
        data = [{'load': {"path": "/", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}, 'expected': True},
                {'load': {"path": "/", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}, 'expected': False},
                {'load': {"path": "/sd", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}, 'expected': True}
        ]
        hostcname = 'dummyhost'
        scope = Scope()
        for to_check in data:
            resp, message = scope.create_from_dict(hostcname, to_check['load'], self.session)
            self.assertEqual(resp, to_check['expected'])

    def test_Scope_to_dict(self):
        data = {"path": "/25", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}
        hostcname = 'dummyhost'
        scope = Scope()
        resp, message = scope.create_from_dict(hostcname, data, self.session)
        self.assertTrue(resp)
        expected = '{"path": "/25", "queryStringModeOn": true, "cacheControlMode": {"cacheControlModeInt": 86400, "cacheControlModeExt": 3600, "cacheControlModeForce": false}}'
        del(message['id'])
        self.assertEqual(json.dumps(message), expected)

    def test_Scope_delete_all(self):
        scope = Scope()
        hostcname = 'dummyhost'
        for i in range(2,5):
            data = {"path": "/%d" % (i), "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}
            resp, message = scope.create_from_dict(hostcname, data, self.session)
            self.assertTrue(resp)
        resp, message = scope.delete_all(hostcname, self.session)
        self.assertTrue(resp)
        scopes = self.session.query(Scope).filter(Scope.hostcname == hostcname).all()
        self.assertEqual(len(scopes), 0)

    def test_Scope_delete(self):
        load = {"name": "deletescope",
                "scopes": [{"path": "/", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}],
                "services": [{"id": 1,}],
                "origin": {"hostname": "deletescope.UpdateConf2.tdl", "port": 80, "protocol": "http", "path": "/"},
                "hostnames": ["www.deletescope.tld"]}
        host = Host()
        accountHash = 'AAAAAAAA'
        resp, message = host.create_from_dict(accountHash, load, self.session)
        hostcname = message['cname']
        scope_id = message['scopes'][0]['id']
        scope = Scope()
        resp, message = scope.delete(scope_id, hostcname, self.session)
        self.assertFalse(resp)
        self.assertEqual(message, 'The scope with the path / can\'t be deleted')
        data = {"path": "/images", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}
        resp, message = scope.create_from_dict(hostcname, data, self.session)
        resp, message = scope.delete(message['id'], hostcname, self.session)
        self.assertTrue(resp)

    def test_Scope_update(self):
        load = {"name": "updatescopes",
                "scopes": [
                    {"path": "/", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}},
                    {"path": "/images", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}},
                    {"path": "/videos", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}},
                ],
                "services": [{"id": 1,}],
                "origin": {"hostname": "updatescopes.UpdateConf2.tdl", "port": 80, "protocol": "http", "path": "/"},
                "hostnames": ["www.updatescopes.tld"]}
        host = Host()
        accountHash = 'AAAAAAAA'
        resp, message = host.create_from_dict(accountHash, load, self.session)
        root_scope_id = message['scopes'][0]['id']
        images_scope_id = message['scopes'][1]['id']
        videos_scope_id =  message['scopes'][2]['id']
        hostcname = message['cname']
        new_scope = {"path": "/images", "queryStringModeOn": False, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}
        scope = Scope()
        resp, message = scope.update_from_dict(root_scope_id, hostcname, new_scope, self.session)
        self.assertFalse(resp)
        self.assertEqual(message, 'Path / can\'t be changed.')
        resp, message = scope.update_from_dict(videos_scope_id, hostcname, new_scope, self.session)
        self.assertFalse(resp)
        self.assertEqual(message, 'A scope with the same path already exist for this host.')
        resp, message = scope.update_from_dict(images_scope_id, hostcname, new_scope, self.session)
        self.assertTrue(resp)
        new_scope['path'] = '/img'
        resp, message = scope.update_from_dict(images_scope_id, hostcname, new_scope, self.session)
        self.assertTrue(resp)
        new_scope['path'] = 'img'
        resp, message = scope.update_from_dict(images_scope_id, hostcname, new_scope, self.session)
        self.assertFalse(resp)
        self.assertEqual(message, 'Wrong format : path')

    def test_Origin_check_dict(self):
        data = [{'load': {"hostname": "mydomain.tdl", "port": 80, "protocol": "http", "path": "/"}, 'expected': True},
                {'load': {"port": 80, "protocol": "http", "path": "/"}, 'expected': False},
                {'load': {"hostname": 12, "port": 80, "protocol": "http", "path": "/"}, 'expected': False},
                {'load': {"hostname": "mydomain.tdl", "protocol": "http", "path": "/"}, 'expected': False},
                {'load': {"hostname": "mydomain.tdl", "port": 'rr', "protocol": "http", "path": "/"}, 'expected': False},
                {'load': {"hostname": "mydomain.tdl", "port": 80, "path": "/"}, 'expected': False},
                {'load': {"hostname": "mydomain.tdl", "port": 80, "protocol": "sdds", "path": "/"}, 'expected': False},
                {'load': {"hostname": "mydomain.tdl", "port": 80, "protocol": "http"}, 'expected': False},
                {'load': {"hostname": "mydomain.tdl", "port": 80, "protocol": "http", "path": "ss/"}, 'expected': False},
        ]
        origin = Origin()
        for to_check in data:
            resp, message = origin._check_dict(to_check['load'])
            self.assertEqual(resp, to_check['expected'])

    def test_Origin_create_from_dict(self):
        data = {"hostname": "mydomain.tdl", "port": 80, "protocol": "http", "path": "/"}
        origin = Origin()
        resp, message = origin._create_from_dict('tt', data, self.session)
        self.assertFalse(resp)
        resp, message = origin._create_from_dict('dummyhost', data, self.session)
        self.assertTrue(resp)
        resp, message = origin._create_from_dict('dummyhost', data, self.session)
        self.assertFalse(resp)
        load = {"name": "AnotherConf",
                "scopes": [{"path": "/", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}],
                "services": [{"id": 1,}],
                "origin": data,
                "hostnames": ["www.anothermydomain.tld"]}
        host = Host()
        accountHash = 'AAAAAAAA'
        resp, message = host.create_from_dict(accountHash, load, self.session)
        self.assertFalse(resp)
        host = self.session.query(Host).filter(Host.cname == 'dummyhost').one()
        host.active = False
        self.session.add(host)
        self.session.commit()
        resp, message = host.create_from_dict(accountHash, load, self.session)
        self.assertTrue(resp)
        host.active = True
        self.session.add(host)
        self.session.commit()
        host = self.session.query(Host).filter(Host.id == message['id']).one()
        host.active = False
        self.session.add(host)
        self.session.commit()

    def test_Origin_update_from_dict(self):
        data = {"hostname": "orig.UpdateConf1.tdl", "port": 80, "protocol": "http", "path": "/"}
        load1 = {"name": "UpdateConf1",
                "scopes": [{"path": "/", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}],
                "services": [{"id": 1,}],
                "origin": data,
                "hostnames": ["www.UpdateConf1.tld"]}
        load2 = {"name": "UpdateConf2",
                "scopes": [{"path": "/", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}],
                "services": [{"id": 1,}],
                "origin": {"hostname": "orig.UpdateConf2.tdl", "port": 80, "protocol": "http", "path": "/"},
                "hostnames": ["www.UpdateConf2.tld"]}
        host = Host()
        accountHash = 'AAAAAAAA'
        resp, message1 = host.create_from_dict(accountHash, load1, self.session)
        self.assertTrue(resp)
        resp, message2 = host.create_from_dict(accountHash, load2, self.session)
        self.assertTrue(resp)
        orig = Origin()
        resp, message = orig.update_from_dict(message2['cname'], data, self.session)
        self.assertFalse(resp)
        resp, message = orig.update_from_dict(message1['cname'], data, self.session)
        self.assertFalse(resp)
        data["port"] = 8080
        resp, message = orig.update_from_dict(message2['cname'], data, self.session)
        self.assertTrue(resp)
        resp, message = orig.update_from_dict('wrong_cname', data, self.session)
        self.assertFalse(resp)

    def test_Origin_to_dict(self):
        origin = Origin()
        origins = self.session.query(Origin).filter(Origin.hostcname == 'dummyhost').all()
        if len(origins) == 0:
            data = {"hostname": "mydomain.tdl", "port": 80, "protocol": "http", "path": "/"}
            origin._create_from_dict('dummyhost', data, self.session)
        origin = self.session.query(Origin).filter(Origin.hostcname == 'dummyhost').one()
        message = origin.to_dict(self.session)
        expected = '{"protocol": "http", "hostname": "mydomain.tdl", "port": 80, "path": "/"}'
        self.assertEqual(json.dumps(message), expected)

    def test_Origin_delete_all(self):
        origin = Origin()
        origins = self.session.query(Origin).filter(Origin.hostcname == 'dummyhost').all()
        self.assertEqual(len(origins), 1)
        origin.delete_all('dummyhost', self.session)
        origins = self.session.query(Origin).filter(Origin.hostcname == 'dummyhost').all()
        self.assertEqual(len(origins), 0)

class TestHostApiEndpoints(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        utils.drop_create_db(db_hostname, db_user, db_password, db_mysql)
        utils.initialize_db(db)
        utils.feed_get_account(db)
        utils.feed_hosts(db)
        cls.session = db_reconnect(db)
        cls.token =  utils.request_token('dummy', 'dummy', token_endpoint)
        cls.endpoint_base = api_base_url + '/api/v1/accounts/'
        cls.headers = {
                    'Accept' : 'application/json',
                    'Content-type' : 'application/json',
                    'Authorization': 'Bearer %s' % str(cls.token['access_token'])
                  }

    def test_Host_POST_main(self):
        '''
        Test POST /api/v1/accounts/{account_hash}/hosts
        '''
        data = {"name": "ApiPostConf",
                "scopes": [{"path": "/", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}],
                "services": [{"id": 1,}],
                "origin": {"hostname": "orig.ApiPostConf.tld", "port": 80, "protocol": "HTTP", "path": "/"},
                "hostnames": ["www.ApiPostConf.tld"]}
        r = requests.post(self.endpoint_base + 'AAAAAAAA/hosts', \
            json.dumps(data), headers=self.headers)
        self.assertEqual(r.status_code, 200)
        r = requests.post(self.endpoint_base + 'AAAAAAAA/hosts', \
            json.dumps(data), headers=self.headers)
        self.assertEqual(r.status_code, 400)

    def test_Host_GET_main(self):
        '''
        Test GET /api/v1/accounts/{account_hash}/hosts
        '''
        r = requests.get(self.endpoint_base + 'AAAAAAAA/hosts', headers=self.headers)
        self.assertEqual(r.status_code, 200)
        hosts = self.session.query(Host).filter(Host.accountHash == 'AAAAAAAA').\
            filter(Host.active == True).all()
        hosts_list = []
        for host in hosts:
            hosts_list.append(host.to_dict(self.session))
        self.assertEqual(r.text, json.dumps(hosts_list))

    def test_Host_GET_detail(self):
        '''
        Test GET /api/v1/accounts/{account_hash}/hosts/{cname}
        '''
        data = {"name": "ApiGetDetailConf",
                "scopes": [{"path": "/", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}],
                "services": [{"id": 1,}],
                "origin": {"hostname": "orig.ApiGetDetailConf.tld", "port": 80, "protocol": "HTTP", "path": "/"},
                "hostnames": ["www.ApiGetDetailConf.tld"]}
        r = requests.post(self.endpoint_base + 'AAAAAAAA/hosts', \
            json.dumps(data), headers=self.headers)
        host = r.json()
        cname = host['cname']
        r = requests.get(self.endpoint_base + 'AAAAAAAA/hosts/' + cname, headers=self.headers)
        self.assertEqual(r.status_code, 200)
        host = self.session.query(Host).filter(Host.cname == cname).one()
        self.assertEqual(r.json(), host.to_dict(self.session))
        r = requests.get(self.endpoint_base + 'AAAAAAAA/hosts/nop', headers=self.headers)
        self.assertEqual(r.status_code, 403)

    def test_Host_PUT_detail(self):
        '''
        Test PUT /api/v1/accounts/{account_hash}/hosts/{cname}
        '''
        data = {"name": "ApiPutDetailConf",
                "scopes": [{"path": "/", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}],
                "services": [{"id": 1,}],
                "origin": {"hostname": "orig.ApiPutDetailConf.tld", "port": 80, "protocol": "HTTP", "path": "/"},
                "hostnames": ["www.ApiPutDetailConf.tld"]}
        r = requests.post(self.endpoint_base + 'AAAAAAAA/hosts', \
            json.dumps(data), headers=self.headers)
        host = r.json()
        cname = host['cname']
        data = {"name": "AnotherName"}
        r = requests.put(self.endpoint_base + 'AAAAAAAA/hosts/' + cname, \
            json.dumps(data), headers=self.headers)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.json()['name'], data['name'])
        data = {'badname': 'YetAnotherNameForApiPutDetailConf'}
        r = requests.put(self.endpoint_base + 'AAAAAAAA/hosts/' + cname, \
            json.dumps(data), headers=self.headers)
        self.assertEqual(r.status_code, 400)
        r = requests.put(self.endpoint_base + 'AAAAAAAA/hosts/nop', \
            json.dumps(data), headers=self.headers)
        self.assertEqual(r.status_code, 403)

    def test_Host_DELETE_detail(self):
        '''
        Test DELETE /api/v1/accounts/{account_hash}/hosts/{cname}
        '''
        data = {"name": "ApiDeleteDetailConf",
                "scopes": [{"path": "/", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}],
                "services": [{"id": 1,}],
                "origin": {"hostname": "orig.ApiDeleteDetailConf.tld", "port": 80, "protocol": "HTTP", "path": "/"},
                "hostnames": ["www.ApiDeleteDetailConf.tld"]}
        r = requests.post(self.endpoint_base + 'AAAAAAAA/hosts', \
            json.dumps(data), headers=self.headers)
        host = r.json()
        cname = host['cname']
        r = requests.delete(self.endpoint_base + 'AAAAAAAA/hosts/' + cname, \
            headers=self.headers)
        self.assertEqual(r.status_code, 200)
        r = requests.delete(self.endpoint_base + 'AAAAAAAA/hosts/nope', \
            headers=self.headers)
        self.assertEqual(r.status_code, 403)

class TestOrigin(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        utils.drop_create_db(db_hostname, db_user, db_password, db_mysql)
        utils.initialize_db(db)
        utils.feed_get_account(db)
        utils.feed_hosts(db)
        cls.session = db_reconnect(db)
        cls.token =  utils.request_token('dummy', 'dummy', token_endpoint)
        cls.endpoint_base = api_base_url + '/api/v1/accounts/'
        cls.headers = {
                    'Accept' : 'application/json',
                    'Content-type' : 'application/json',
                    'Authorization': 'Bearer %s' % str(cls.token['access_token'])
                  }
    def test_Origin_GET(self):
        '''
        Test GET /api/v1/accounts/{account_hash}/hosts/{cname}/origin
        '''
        data = {"name": "ApiGetOrigin",
                "scopes": [{"path": "/", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}],
                "services": [{"id": 1,}],
                "origin": {"hostname": "orig.ApiGetOrigin.tld", "port": 80, "protocol": "HTTP", "path": "/"},
                "hostnames": ["www.ApiGetOrigin.tld"]}
        r = requests.post(self.endpoint_base + 'AAAAAAAA/hosts', \
            json.dumps(data), headers=self.headers)
        host = r.json()
        cname = host['cname']
        url = '%sAAAAAAAA/hosts/%s/origin' % (self.endpoint_base, cname)
        r = requests.get(url, headers=self.headers)
        self.assertEqual(r.status_code, 200)
        expected = '{"protocol": "http", "hostname": "orig.ApiGetOrigin.tld", "port": 80, "path": "/"}'
        self.assertEqual(expected, r.text)
        url = '%sAAAAAAAB/hosts/%s/origin' % (self.endpoint_base, cname)
        r = requests.get(url, headers=self.headers)
        self.assertEqual(r.status_code, 403)
        url = '%sBBBBBBBB/hosts/%s/origin' % (self.endpoint_base, cname)
        r = requests.get(url, headers=self.headers)
        self.assertEqual(r.status_code, 400)

    def test_Origin_PUT(self):
        '''
        Test PUT /api/v1/accounts/{account_hash}/hosts/{cname}/origin
        '''
        data = {"name": "ApiPutOrigin",
                "scopes": [{"path": "/", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}],
                "services": [{"id": 1,}],
                "origin": {"hostname": "orig.ApiPutOrigin.tld", "port": 80, "protocol": "HTTP", "path": "/"},
                "hostnames": ["www.ApiPutOrigin.tld"]}
        r = requests.post(self.endpoint_base + 'AAAAAAAA/hosts', \
            json.dumps(data), headers=self.headers)
        host = r.json()
        cname = host['cname']
        url = '%sAAAAAAAA/hosts/%s/origin' % (self.endpoint_base, cname)
        data = {"hostname": "orig.ApiPutOrigin.tld", "port": 8080, "protocol": "HTTP", "path": "/"}
        r = requests.put(url, json.dumps(data), headers=self.headers)
        self.assertEqual(r.status_code, 200)
        expected = '{"protocol": "http", "hostname": "orig.ApiPutOrigin.tld", "port": 8080, "path": "/"}'
        self.assertEqual(expected, r.text)
        url = '%sAAAAAAAB/hosts/%s/origin' % (self.endpoint_base, cname)
        r = requests.put(url, json.dumps(data), headers=self.headers)
        self.assertEqual(r.status_code, 403)
        data = []
        url = '%sAAAAAAAA/hosts/%s/origin' % (self.endpoint_base, cname)
        r = requests.put(url, json.dumps(data), headers=self.headers)
        self.assertEqual(r.status_code, 400)

class TestHostname(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        utils.drop_create_db(db_hostname, db_user, db_password, db_mysql)
        utils.initialize_db(db)
        utils.feed_get_account(db)
        utils.feed_hosts(db)
        cls.session = db_reconnect(db)
        cls.token =  utils.request_token('dummy', 'dummy', token_endpoint)
        cls.endpoint_base = api_base_url + '/api/v1/accounts/'
        cls.headers = {
                    'Accept' : 'application/json',
                    'Content-type' : 'application/json',
                    'Authorization': 'Bearer %s' % str(cls.token['access_token'])
                  }

    def test_Hostname_POST(self):
        '''
        Test POST /api/v1/accounts/{account_hash}/hosts/{cname}/hostnames
        '''
        data = {"name": "ApiPostHostname",
                "scopes": [{"path": "/", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}],
                "services": [{"id": 1,}],
                "origin": {"hostname": "orig.ApiPostHostname.tld", "port": 80, "protocol": "HTTP", "path": "/"},
                "hostnames": ["www.ApiPostHostname1.tld", "www.ApiPostHostname2.tld"]}
        r = requests.post(self.endpoint_base + 'AAAAAAAA/hosts', \
            json.dumps(data), headers=self.headers)
        host = r.json()
        cname = host['cname']
        data = "www.ApiPostHostname3.tld"
        url = '%sAAAAAAAA/hosts/%s/hostnames' % (self.endpoint_base, cname)
        r = requests.post(url, json.dumps(data), headers=self.headers)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(json.dumps("www.ApiPostHostname3.tld"), r.text)
        url2 = '%sAAAAAAAA/hosts/%s/hostnames' % (self.endpoint_base, cname)
        r = requests.get(url2, headers=self.headers)
        self.assertEqual(["www.ApiPostHostname1.tld", "www.ApiPostHostname2.tld", "www.ApiPostHostname3.tld"], json.loads(r.text))
        r = requests.post(url, json.dumps(data), headers=self.headers)
        self.assertEqual(r.status_code, 400)
        data = "www.ApiPostHostname4.tld"
        url = '%sBAAAAAAA/hosts/%s/hostnames' % (self.endpoint_base, cname)
        r = requests.post(url, json.dumps(data), headers=self.headers)
        self.assertEqual(r.status_code, 403)

    def test_Hostname_GET(self):
        '''
        Test GET /api/v1/accounts/{account_hash}/hosts/{cname}/hostnames
        '''
        data = {"name": "ApiGetHostname",
                "scopes": [{"path": "/", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}],
                "services": [{"id": 1,}],
                "origin": {"hostname": "orig.ApiGetHostname.tld", "port": 80, "protocol": "HTTP", "path": "/"},
                "hostnames": ["www.ApiGetHostname1.tld", "www.ApiGetHostname2.tld"]}
        r = requests.post(self.endpoint_base + 'AAAAAAAA/hosts', \
            json.dumps(data), headers=self.headers)
        host = r.json()
        cname = host['cname']
        url = '%sAAAAAAAA/hosts/%s/hostnames' % (self.endpoint_base, cname)
        r = requests.get(url, headers=self.headers)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(json.dumps(data["hostnames"]), r.text)
        url = '%sBBBBBBBB/hosts/%s/hostnames' % (self.endpoint_base, cname)
        r = requests.get(url, headers=self.headers)
        self.assertEqual(r.status_code, 400)
        url = '%sABBBBBBB/hosts/%s/hostnames' % (self.endpoint_base, cname)
        r = requests.get(url, headers=self.headers)
        self.assertEqual(r.status_code, 403)

    def test_Hostname_DELETE(self):
        '''
        Test DELETE /api/v1/accounts/{account_hash}/hosts/{cname}/hostnames/{hostname_name}
        '''
        data = {"name": "ApiDeleteHostname",
                "scopes": [{"path": "/", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}],
                "services": [{"id": 1,}],
                "origin": {"hostname": "orig.ApiDeleteHostname.tld", "port": 80, "protocol": "HTTP", "path": "/"},
                "hostnames": ["www.ApiDeleteHostname1.tld", "www.ApiDeleteHostname2.tld"]}
        r = requests.post(self.endpoint_base + 'AAAAAAAA/hosts', \
            json.dumps(data), headers=self.headers)
        host = r.json()
        cname = host['cname']
        url = '%sAAAAAAAA/hosts/%s/hostnames/%s' % (self.endpoint_base, cname, "www.ApiDeleteHostname1.tld")
        r = requests.delete(url, headers=self.headers)
        self.assertEqual(r.status_code, 200)
        url = '%sAAAAAAAA/hosts/%s/hostnames' % (self.endpoint_base, cname)
        r = requests.get(url, headers=self.headers)
        self.assertEqual(["www.ApiDeleteHostname2.tld"], json.loads(r.text))
        url = '%sBAAAAAAA/hosts/%s/hostnames/%s' % (self.endpoint_base, cname, "www.ApiDeleteHostname1.tld")
        r = requests.delete(url, headers=self.headers)
        self.assertEqual(r.status_code, 403)
        url = '%sAAAAAAAA/hosts/%s/hostnames/%s' % (self.endpoint_base, cname, "www.ApiDeleteHostname1.tld")
        r = requests.delete(url, headers=self.headers)
        self.assertEqual(r.status_code, 400)

class TestScope(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        utils.drop_create_db(db_hostname, db_user, db_password, db_mysql)
        utils.initialize_db(db)
        utils.feed_get_account(db)
        utils.feed_hosts(db)
        cls.session = db_reconnect(db)
        cls.token =  utils.request_token('dummy', 'dummy', token_endpoint)
        cls.endpoint_base = api_base_url + '/api/v1/accounts/'
        cls.headers = {
                    'Accept' : 'application/json',
                    'Content-type' : 'application/json',
                    'Authorization': 'Bearer %s' % str(cls.token['access_token'])
                  }

    def test_Scope_GET_main(self):
        '''
        Test GET /api/v1/accounts/{account_hash}/hosts/{cname}/scopes
        '''
        data = {"name": "ApiGetScopeMain",
                "scopes": [ {"path": "/", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}},
                            {"path": "/images", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}],
                "services": [{"id": 1,}],
                "origin": {"hostname": "orig.ApiGetScopeMain.tld", "port": 80, "protocol": "HTTP", "path": "/"},
                "hostnames": ["www.ApiGetScopeMain.tld"]}
        r = requests.post(self.endpoint_base + 'AAAAAAAA/hosts', \
            json.dumps(data), headers=self.headers)
        host = r.json()
        cname = host['cname']
        url = '%sAAAAAAAA/hosts/%s/scopes' % (self.endpoint_base, cname)
        r = requests.get(url, headers= self.headers)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(len(json.loads(r.text)), 2)
        url = '%sBAAAAAAA/hosts/%s/scopes' % (self.endpoint_base, cname)
        r = requests.get(url, headers= self.headers)
        self.assertEqual(r.status_code, 403)

    def test_Scope_POST_main(self):
        '''
        Test POST /api/v1/accounts/{account_hash}/hosts/{cname}/scopes
        '''
        data = {"name": "ApiPostScopeMain",
                "scopes": [ {"path": "/", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}},
                            {"path": "/images", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}],
                "services": [{"id": 1,}],
                "origin": {"hostname": "orig.ApiPostScopeMain.tld", "port": 80, "protocol": "HTTP", "path": "/"},
                "hostnames": ["www.ApiPostScopeMain.tld"]}
        load = {"path": "/videos", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}
        r = requests.post(self.endpoint_base + 'AAAAAAAA/hosts', \
            json.dumps(data), headers=self.headers)
        host = r.json()
        cname = host['cname']
        url = '%sAAAAAAAA/hosts/%s/scopes' % (self.endpoint_base, cname)
        r = requests.post(url, json.dumps(load), headers=self.headers)
        self.assertEqual(r.status_code, 200)
        r = requests.get(url, headers=self.headers)
        self.assertEqual(len(json.loads(r.text)), 3)
        load['path'] = '/'
        r = requests.post(url, json.dumps(load), headers=self.headers)
        self.assertEqual(r.status_code, 400)
        url = '%sBAAAAAAA/hosts/%s/scopes' % (self.endpoint_base, cname)
        r = requests.post(url, json.dumps(load), headers=self.headers)
        self.assertEqual(r.status_code, 403)

    def test_Scope_GET_detail(self):
        '''
        Test POST /api/v1/accounts/{account_hash}/hosts/{cname}/scopes/{scope_id}
        '''
        data = {"name": "ApiGetScopeDetail",
                "scopes": [ {"path": "/", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}},
                            {"path": "/images", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}}],
                "services": [{"id": 1,}],
                "origin": {"hostname": "orig.ApiGetScopeDetail.tld", "port": 80, "protocol": "HTTP", "path": "/"},
                "hostnames": ["www.ApiGetScopeDetail.tld"]}
        r = requests.post(self.endpoint_base + 'AAAAAAAA/hosts', \
            json.dumps(data), headers=self.headers)
        host = r.json()
        cname = host['cname']
        scope_images = scope = host['scopes'][1]
        url = '%sAAAAAAAA/hosts/%s/scopes/%s' % (self.endpoint_base, cname, scope_images['id'])
        r = requests.get(url, headers=self.headers)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(json.loads(r.text), scope_images)
        url = '%sBAAAAAAA/hosts/%s/scopes/%s' % (self.endpoint_base, cname, scope_images['id'])
        r = requests.get(url, headers=self.headers)
        self.assertEqual(r.status_code, 403)
        url = '%sAAAAAAAA/hosts/%s/scopes/%s' % (self.endpoint_base, cname, '125232596')
        r = requests.get(url, headers=self.headers)
        self.assertEqual(r.status_code, 400)

    def test_Scope_PUT_detail(self):
        '''
        Test POST /api/v1/accounts/{account_hash}/hosts/{cname}/scopes/{scope_id}
        '''
        data = {"name": "ApiPutScopeDetail",
                "scopes": [ {"path": "/", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}},
                            {"path": "/images", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}},
                            {"path": "/videos", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}},
                        ],
                "services": [{"id": 1,}],
                "origin": {"hostname": "orig.ApiPutScopeDetail.tld", "port": 80, "protocol": "HTTP", "path": "/"},
                "hostnames": ["www.ApiPutScopeDetail.tld"]}
        load = {"path": "/videos-1", "queryStringModeOn": True, "cacheControlMode": {"int": 3600, "ext": 3600, "force": False}}
        r = requests.post(self.endpoint_base + 'AAAAAAAA/hosts', \
            json.dumps(data), headers=self.headers)
        host = r.json()
        cname = host['cname']
        scope_images = scope = host['scopes'][1]
        url = '%sAAAAAAAA/hosts/%s/scopes/%s' % (self.endpoint_base, cname, scope_images['id'])
        r = requests.put(url, json.dumps(load), headers=self.headers)
        self.assertEqual(r.status_code, 200)
        load['path'] = '/videos'
        r = requests.put(url, json.dumps(load), headers=self.headers)
        self.assertEqual(r.status_code, 400)
        url = '%sBAAAAAAA/hosts/%s/scopes/%s' % (self.endpoint_base, cname, scope_images['id'])
        r = requests.get(url, headers=self.headers)
        self.assertEqual(r.status_code, 403)

    def test_Scope_DELETE_detail(self):
        '''
        Test DELETE /api/v1/accounts/{account_hash}/hosts/{cname}/scopes/{scope_id}
        '''
        data = {"name": "ApiDeleteScopeDetail",
                "scopes": [ {"path": "/", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}},
                            {"path": "/images", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}},
                            {"path": "/videos", "queryStringModeOn": True, "cacheControlMode": {"int": 86400, "ext": 3600, "force": False}},
                        ],
                "services": [{"id": 1,}],
                "origin": {"hostname": "orig.ApiDeleteScopeDetail.tld", "port": 80, "protocol": "HTTP", "path": "/"},
                "hostnames": ["www.ApiDeleteScopeDetail.tld"]}
        r = requests.post(self.endpoint_base + 'AAAAAAAA/hosts', \
            json.dumps(data), headers=self.headers)
        host = r.json()
        cname = host['cname']
        scope_images = scope = host['scopes'][1]
        url = '%sAAAAAAAA/hosts/%s/scopes/%s' % (self.endpoint_base, cname, scope_images['id'])
        r = requests.delete(url, headers=self.headers)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.text, '"Scope %d for the host %s deleted"' % (scope_images['id'], cname))
        r = requests.delete(url, headers=self.headers)
        self.assertEqual(r.status_code, 400)
        url = '%sBAAAAAAA/hosts/%s/scopes/%s' % (self.endpoint_base, cname, scope_images['id'])
        r = requests.delete(url, headers=self.headers)
        self.assertEqual(r.status_code, 403)
