import unittest

import requests
import memcache
import json
from collections import OrderedDict

from unit import utils
from models.base import *
from models.accounts import Contact, Account
from models.services import Service
from models.users import RoleList, RoleMatrix, User
from common import utils as common_utils


'''
The database will be DROPPED in the process. MYSQL sleeping process
will be KILLED
'''
db_hostname = '127.0.0.1'
db_user = 'api'
db_password = 'password'
db_mysql = 'optimicdnapi'
db = 'mysql+mysqldb://api:password@127.0.0.1/optimicdnapi'
token_endpoint = 'http://127.0.0.1:8080/auth/token'
api_base_url = 'http://127.0.0.1:8080/'
memcache_servers = ['127.0.0.1:11211']

class TestObjects(unittest.TestCase):
    '''
    Test Objects methods
    '''

    @classmethod
    def setUpClass(cls):
        utils.drop_create_db(db_hostname, db_user, db_password, db_mysql)
        utils.initialize_db(db)
        utils.feed_get_account(db)
        cls.session = db_reconnect(db)

    def test_RoleList_to_dict(self):
        role_list = self.session.query(RoleList).filter(RoleList.id==1).one()
        expected = {"account": "EDIT", "configuration": "EDIT", "report": "EDIT", "content": "EDIT"}
        self.assertEqual(role_list.to_dict(), expected)

    def test_RoleList_check_dict(self):
        role_list = RoleList()
        data = [
                {
                    'load' : {'account' : 'READ', 'configuration' : 'READ', 'report' : 'READ', 'content': 'READ'},
                    'expected' : True
                },
                {
                    'load' : {'accounts' : 'READ', 'configuration' : 'READ', 'report' : 'READ', 'content': 'READ'},
                    'expected' : False
                },
                {
                    'load' : {'configuration' : 'READ', 'report' : 'READ', 'content': 'READ'},
                    'expected' : False
                },
                {
                    'load' : {'account' : 'WRONG', 'configuration' : 'READ', 'report' : 'READ', 'content': 'READ'},
                    'expected' : False
                }
                ]
        for to_check in data:
            resp, message = role_list.check_dict(to_check['load'])
            self.assertEqual(resp, to_check['expected'])

    def test_RoleList_load_from_dict(self):
        role_list = RoleList()
        data = [
                {
                    'load': {"account": "NONE", "configuration": "NONE", "report": "NONE", "content": "NONE"},
                    'expected' : True,
                    'id' : 81
                },
                {
                    'load': {"account": "EDIT", "configuration": "EDIT", "report": "EDIT", "content": "EDIT"},
                    'expected' : True,
                    'id' : 1
                },
                {
                    'load' : {},
                    'expected' : False,
                    'id' : None
                }
        ]
        for to_check in data:
            resp = role_list.load_from_dict(to_check['load'], self.session)
            self.assertEqual(resp, to_check['expected'])
            self.assertEqual(role_list.id, to_check['id'])
            role_list.id = None

    def test_RoleMatrix_check_dict(self):
        role_matrix = RoleMatrix()
        data = [
                {
                    'load': {
                                "userAccount": {"account": "NONE", "configuration": "NONE", "report": "NONE", "content": "NONE"},
                                "subAccounts": {"account": "NONE", "configuration": "NONE", "report": "NONE", "content": "NONE"}
                            },
                    'expected' : True,
                },
                {
                    'load': {
                                "userAccount": {"account": "EDIT", "configuration": "EDIT", "report": "EDIT", "content": "EDIT"},
                                "subAccounts": {"account": "EDIT", "configuration": "EDIT", "report": "EDIT", "content": "EDIT"}
                            },
                    'expected' : True,
                },
                {
                    'load': {
                                "userAccount": {"account": "EDIT", "configuration": "EDIT", "report": "EDIT", "content": "EDIT"},
                    },
                    'expected' : False,
                },
                ]
        for to_check in data:
            resp, message = role_matrix.check_dict(to_check['load'])
            self.assertEqual(resp, to_check['expected'])

    def test_RoleMatrix_load_from_dict(self):
        role_matrix = RoleMatrix()
        data = [
                {
                    'load': {
                                "userAccount": {"account": "NONE", "configuration": "NONE", "report": "NONE", "content": "NONE"},
                                "subAccounts": {"account": "NONE", "configuration": "NONE", "report": "NONE", "content": "NONE"}
                            },
                    'expected' : True,
                    'id' : 6561
                },
                {
                    'load': {
                                "userAccount": {"account": "EDIT", "configuration": "EDIT", "report": "EDIT", "content": "EDIT"},
                                "subAccounts": {"account": "EDIT", "configuration": "EDIT", "report": "EDIT", "content": "EDIT"}
                            },
                    'expected' : True,
                    'id' : 1
                },
                {
                    'load': {},
                    'expected' : False,
                    'id': None
                },
        ]
        for to_check in data:
            resp = role_matrix.load_from_dict(to_check['load'], self.session)
            self.assertEqual(resp, to_check['expected'])
            self.assertEqual(role_matrix.id, to_check['id'])
            role_matrix.id = None

    def test_User_to_dict(self):
        user = self.session.query(User).filter(User.id==1).one()
        expected = {"id": 1, 'accountHash' : 'AAAAAAAA', "username": "dummy", "firstName": "firstname", "lastName": "lastname", \
            "lastLogin": None, "email": "dummy@nowhere.com", "phone": "+33111111111", \
            "fax": "+33111111111", "authorizedSupportContact": True, \
            "roles": {"userAccount": {"account": "EDIT", "configuration": "EDIT", "report": "EDIT", "content": "EDIT"}, \
            "subAccounts": {"account": "EDIT", "configuration": "EDIT", "report": "EDIT", "content": "EDIT"}}}
        self.assertEqual(user.to_dict(self.session), expected)
        user.roles = 81
        expected = {"id": 1,'accountHash' : 'AAAAAAAA', "username": "dummy", "firstName": "firstname", "lastName": "lastname", \
            "lastLogin": None, "email": "dummy@nowhere.com", "phone": "+33111111111", \
            "fax": "+33111111111", "authorizedSupportContact": True, \
            "roles": {"userAccount": {"account": "EDIT", "configuration": "EDIT", "report": "EDIT", "content": "EDIT"}, \
            "subAccounts": {"account": "NONE", "configuration": "NONE", "report": "NONE", "content": "NONE"}}}
        self.session.add(user)
        self.assertEqual(user.to_dict(self.session), expected)
        user.roles = 1
        self.session.add(user)

    def test_User_check_dict(self):
        user = User()
        data = {"username": "dummy",
                "firstName": "firstname",
                "lastName": "lastname",
                "password" : "fgghhhhhsS1",
                "lastLogin": None,
                "email": "dummy@nowhere.com",
                "phone": "+33111111111",
                "fax": "+33111111111",
                "authorizedSupportContact": True,
                "roles": {
                            "userAccount": {"account": "EDIT", "configuration": "EDIT", "report": "EDIT", "content": "EDIT"},
                            "subAccounts": {"account": "EDIT", "configuration": "EDIT", "report": "EDIT", "content": "EDIT"}
                        }
                }
        resp, message = user._check_dict(data)
        self.assertTrue(resp)
        for password in ['hyjqodlpqs', '1jsjdsdsd!sd', 'SSSSU1HSD']:
            data["password"] = password
            resp, message = user._check_dict(data)
            self.assertFalse(resp)
        data["password"] = "fgghhhhhsS1"
        data["email"] = "kkk"
        resp, message = user._check_dict(data)
        self.assertFalse(resp)
        data["weird"] = ""
        resp, message = user._check_dict(data)
        self.assertFalse(resp)
        del data["weird"]
        del data["lastName"]
        resp, message = user._check_dict(data)
        self.assertFalse(resp)

    def test_User_create_from_dict(self):
        user = User()
        data = {"username": "dummy",
                "firstName": "firstname",
                "lastName": "lastname",
                "password" : "fgghhhhhsS1",
                "accountHash" : 'BBBBBBBB',
                "lastLogin": None,
                "email": "dummy@nowhere.com",
                "phone": "+33111111111",
                "fax": "+33111111111",
                "authorizedSupportContact": True,
                "roles": {
                            "userAccount": {"account": "EDIT", "configuration": "EDIT", "report": "EDIT", "content": "EDIT"},
                            "subAccounts": {"account": "EDIT", "configuration": "EDIT", "report": "EDIT", "content": "EDIT"}
                        }
                }
        # accountHash != parent_accountHash
        resp, message = user.create_from_dict(data, self.session, 'AAAAAAAA')
        self.assertEqual(resp, False)
        del data["accountHash"]
        # username dummy aleardy exists
        resp, message = user.create_from_dict(data, self.session, 'AAAAAAAA')
        self.assertEqual(resp, False)
        data["username"] = "dummy2"
        # parent_accounthash not in the db
        resp, message = user.create_from_dict(data, self.session, 'ZZZZZZZ')
        self.assertEqual(resp, False)
        #everything is ok
        resp, message = user.create_from_dict(data, self.session, 'AAAAAAAA')
        self.assertEqual(resp, True)
        user_in_db = self.session.query(User).filter(User.username == "dummy2").one()
        for attribute in ["username", "firstName", "lastName", "email", "phone", "fax", "authorizedSupportContact"]:
            self.assertEqual(data.get(attribute), getattr(user_in_db, attribute))
        self.assertEqual(user_in_db.password, 'b69749fbe6cb7e17ca9c2195635f47d4acc50d0a1a2abc122530c07777acd033')
        self.assertEqual(user_in_db.roles, 1)

    def test_User_update_from_dict(self):
        user = User()
        data = {    "id": 2,
                    "username": "dummy2",
                    "firstName": "firstname",
                    "lastName": "lastname",
                    "password" : "fgghhhhhsS1",
                    "lastLogin": None,
                    "email": "dummy2@nowhere.com",
                    "phone": "+33111111111",
                    "fax": "+33111111111",
                    "authorizedSupportContact": True,
                    "roles": {
                                "userAccount": {"account": "EDIT", "configuration": "EDIT", "report": "EDIT", "content": "EDIT"},
                                "subAccounts": {"account": "EDIT", "configuration": "EDIT", "report": "EDIT", "content": "EDIT"}
                            }
                    }
        resp, message = user.update_from_dict(data, self.session, 'AAAAAAAA')
        self.assertEqual(resp, True)
        user_updated = self.session.query(User).filter(User.id==2).one()
        for attribute in ["id", "username", "firstName", "lastName", "email", "email", "phone",  "fax", "authorizedSupportContact"]:
            self.assertEqual(getattr(user_updated, attribute), data.get(attribute))
        self.assertEqual(user_updated.password, 'b69749fbe6cb7e17ca9c2195635f47d4acc50d0a1a2abc122530c07777acd033')
        self.assertEqual(user_updated.roles, 1)

class TestGetMe(unittest.TestCase):
    '''
    Test GET /api/v1/users/me
    '''
    @classmethod
    def setUpClass(cls):
        utils.drop_create_db(db_hostname, db_user, db_password, db_mysql)
        utils.initialize_db(db)
        utils.feed_get_account(db)
        cls.token =  utils.request_token('dummy', 'dummy', token_endpoint)
        cls.endpoint_base = api_base_url + 'api/v1/users/me'
        cls.headers = {
                    'Accept' : 'application/json',
                    'Content-type' : 'application/json',
                    'Authorization': 'Bearer %s' % str(cls.token['access_token'])
                  }
        cls.session = db_reconnect(db)

    def test_getme_without_token(self):
        '''
        Test without token
        '''
        r = requests.get(self.endpoint_base)
        self.assertEqual(r.status_code, 403)

    def test_getme(self):
        '''
        Test with a token
        '''
        r = requests.get(self.endpoint_base, headers=self.headers)
        self.assertEqual(r.status_code, 200)
        expected = {"id": 1, "accountHash": "AAAAAAAA", "username": "dummy", "firstName": "firstname", "lastName": "lastname", \
            "lastLogin": None, "email": "dummy@nowhere.com", "phone": "+33111111111", \
            "fax": "+33111111111", "authorizedSupportContact": True, \
            "roles": {"userAccount": {"account": "EDIT", "configuration": "EDIT", "report": "EDIT", "content": "EDIT"}, \
            "subAccounts": {"account": "EDIT", "configuration": "EDIT", "report": "EDIT", "content": "EDIT"}}}
        self.assertEqual(r.json(), expected)

class TestGetUser(unittest.TestCase):
    '''
    Test GET /api/v1/accounts/{account_hash}/users/{user_id}
    '''
    @classmethod
    def setUpClass(cls):
        utils.drop_create_db(db_hostname, db_user, db_password, db_mysql)
        utils.initialize_db(db)
        utils.feed_get_account(db)
        utils.feed_get_user(db)
        cls.token =  utils.request_token('dummy', 'dummy', token_endpoint)
        cls.endpoint_base = api_base_url + '/api/v1/accounts/'
        cls.headers = {
                    'Accept' : 'application/json',
                    'Content-type' : 'application/json',
                    'Authorization': 'Bearer %s' % str(cls.token['access_token'])
                  }
        cls.session = db_reconnect(db)

    def test_good_permission(self):
        '''
        Test with good permission
        '''
        r = requests.get(self.endpoint_base + 'AAAAAAAA/users/2', headers=self.headers)
        self.assertEqual(r.status_code, 200)
        expected = '{"id": 2, "accountHash": "AAAAAAAA", "username": "Nopermission", "firstName": "Nofirstname", "lastName": "Nolastname", "lastLogin": null, "email": "nopermission@nowhere.com", "phone": "+33111111111", "fax": "+33111111111", "authorizedSupportContact": true, "roles": {"userAccount": {"account": "NONE", "configuration": "NONE", "report": "NONE", "content": "NONE"}, "subAccounts": {"account": "NONE", "configuration": "NONE", "report": "NONE", "content": "NONE"}}}'
        self.assertEqual(r.text, expected)

    def test_wrong_account_hash(self):
        '''
        The logged user is not attached (account or any parents) to the account_hash
        '''
        r = requests.get(self.endpoint_base + 'CCCCCCCC/users/3', headers=self.headers)
        self.assertEqual(r.status_code, 403)

    def test_wrong_id(self):
        '''
        The userid don't exists or is not attached to the account_hash
        '''
        r = requests.get(self.endpoint_base + 'AAAAAAAA/users/14', headers=self.headers)
        self.assertEqual(r.status_code, 400)
        r = requests.get(self.endpoint_base + 'AAAAAAAA/users/3', headers=self.headers)
        self.assertEqual(r.status_code, 400)

    def test_without_permission(self):
        '''
        The user doesn't have the READ permission on the Account
        '''
        token = utils.request_token('Nopermission', 'dummy', token_endpoint)
        headers = {
                    'Accept' : 'application/json',
                    'Content-type' : 'application/json',
                    'Authorization': 'Bearer %s' % str(token['access_token'])
                }
        r = requests.get(self.endpoint_base + 'AAAAAAAA/users/2', headers=headers)
        self.assertEqual(r.status_code, 403)

class TestGetUsers(unittest.TestCase):
    '''
    Test GET /api/v1/accounts/{account_hash}/users
    '''
    @classmethod
    def setUpClass(cls):
        utils.drop_create_db(db_hostname, db_user, db_password, db_mysql)
        utils.initialize_db(db)
        utils.feed_get_account(db)
        utils.feed_get_user(db)
        cls.token =  utils.request_token('dummy', 'dummy', token_endpoint)
        cls.endpoint_base = api_base_url + '/api/v1/accounts/'
        cls.headers = {
                    'Accept' : 'application/json',
                    'Content-type' : 'application/json',
                    'Authorization': 'Bearer %s' % str(cls.token['access_token'])
                  }
        cls.session = db_reconnect(db)

    def test_good_permission(self):
        '''
        Test with good permission
        '''
        r = requests.get(self.endpoint_base + 'AAAAAAAA/users', headers=self.headers)
        self.assertEqual(r.status_code, 200)
        expected = [{"id": 1, "accountHash": "AAAAAAAA", "username": "dummy", "firstName": "firstname", \
            "lastName": "lastname", "lastLogin": None, "email": "dummy@nowhere.com", \
            "phone": "+33111111111", "fax": "+33111111111", "authorizedSupportContact": 1, \
            "roles": {"userAccount": {"account": "EDIT", "configuration": "EDIT", \
            "report": "EDIT", "content": "EDIT"}, "subAccounts": {"account": "EDIT", \
            "configuration": "EDIT", "report": "EDIT", "content": "EDIT"}}},\
             {"id": 2, "accountHash": "AAAAAAAA", "username": "Nopermission", "firstName": "Nofirstname", \
             "lastName": "Nolastname", "lastLogin": None, "email": "nopermission@nowhere.com", \
             "phone": "+33111111111", "fax": "+33111111111", "authorizedSupportContact": True, \
             "roles": {"userAccount": {"account": "NONE", "configuration": "NONE", \
             "report": "NONE", "content": "NONE"}, "subAccounts": {"account": "NONE", \
             "configuration": "NONE", "report": "NONE", "content": "NONE"}}}]
        self.assertEqual(r.json(), expected)

    def test_good_permission_sub_account(self):
        '''
        Test with good permission
        '''
        r = requests.get(self.endpoint_base + 'BBBBBBBB/users', headers=self.headers)
        self.assertEqual(r.status_code, 200)
        expected = [{"id": 4, "accountHash": "BBBBBBBB", "username": "Buser", "firstName": "Bfirstname", \
            "lastName": "Bolastname", "lastLogin": None, "email": "nopermission@nowhere.com", \
            "phone": "+33111111111", "fax": "+33111111111", "authorizedSupportContact": True, \
            "roles": {"userAccount": {"account": "EDIT", "configuration": "EDIT", \
            "report": "EDIT", "content": "EDIT"}, "subAccounts": {"account": "EDIT", \
            "configuration": "EDIT", "report": "EDIT", "content": "EDIT"}}}]
        self.assertEqual(r.json(), expected)

    def test_wrong_account_hash(self):
        '''
        The logged user is not attached (account or any parents) to the account_hash
        '''
        r = requests.get(self.endpoint_base + 'CCCCCCCC/users', headers=self.headers)
        self.assertEqual(r.status_code, 403)

    def test_without_permission(self):
        '''
        The user doesn't have the READ permission on the Account
        '''
        token = utils.request_token('Nopermission', 'dummy', token_endpoint)
        headers = {
                    'Accept' : 'application/json',
                    'Content-type' : 'application/json',
                    'Authorization': 'Bearer %s' % str(token['access_token'])
                }
        r = requests.get(self.endpoint_base + 'AAAAAAAA/users', headers=headers)
        self.assertEqual(r.status_code, 403)

class TestPostUsers(unittest.TestCase):
    '''
    Test POST /api/v1/accounts/{account_hash}/users
    '''
    @classmethod
    def setUpClass(cls):
        utils.drop_create_db(db_hostname, db_user, db_password, db_mysql)
        utils.initialize_db(db)
        utils.feed_get_account(db)
        utils.feed_get_user(db)
        cls.token =  utils.request_token('dummy', 'dummy', token_endpoint)
        cls.endpoint_base = api_base_url + '/api/v1/accounts/'
        cls.headers = {
                    'Accept' : 'application/json',
                    'Content-type' : 'application/json',
                    'Authorization': 'Bearer %s' % str(cls.token['access_token'])
                  }
        cls.session = db_reconnect(db)
        cls.data = {"username": "dummy2",
                    "firstName": "firstname",
                    "lastName": "lastname",
                    "password" : "fgghhhhhsS1",
                    "lastLogin": None,
                    "email": "dummy@nowhere.com",
                    "phone": "+33111111111",
                    "fax": "+33111111111",
                    "authorizedSupportContact": True,
                    "roles": {
                                "userAccount": {"account": "EDIT", "configuration": "EDIT", "report": "EDIT", "content": "EDIT"},
                                "subAccounts": {"account": "EDIT", "configuration": "EDIT", "report": "EDIT", "content": "EDIT"}
                            }
                    }

    def test_good_permission(self):
        '''
        Test with good permission and valid data
        '''
        r = requests.post(self.endpoint_base + 'AAAAAAAA/users', json.dumps(self.data), \
            headers=self.headers)
        self.assertEqual(r.status_code, 201)

    def test_good_permission_sub_account(self):
        '''
        Test with good permission and valid data
        '''
        data = dict(self.data)
        data["username"] = "dummy3"
        r = requests.post(self.endpoint_base + 'BBBBBBBB/users', json.dumps(data), \
            headers=self.headers)
        self.assertEqual(r.status_code, 201)

    def test_without_permission(self):
        '''
        The user doesn't have the EDIT permission on the Account
        '''
        token = utils.request_token('Nopermission', 'dummy', token_endpoint)
        headers = {
                    'Accept' : 'application/json',
                    'Content-type' : 'application/json',
                    'Authorization': 'Bearer %s' % str(token['access_token'])
        }
        r = requests.post(self.endpoint_base + 'AAAAAAAA/users', json.dumps(self.data), \
            headers=headers)
        self.assertEqual(r.status_code, 403)

    def test_wrong_account(self):
        '''
        The connected user is not associated with the account
        '''
        r = requests.post(self.endpoint_base + 'CCCCCCCC/users', json.dumps(self.data), \
            headers=self.headers)
        self.assertEqual(r.status_code, 403)

    def test_username_exist(self):
        '''
        The username already exists
        '''
        data = dict(self.data)
        data["username"] = "dummy"
        r = requests.post(self.endpoint_base + 'AAAAAAAA/users', json.dumps(data), \
            headers=self.headers)
        self.assertEqual(r.status_code, 400)

class TestPutUser(unittest.TestCase):
    '''
    Test PUT /api/v1/accounts/{account_hash}/users/{user_id}
    '''
    @classmethod
    def setUpClass(cls):
        utils.drop_create_db(db_hostname, db_user, db_password, db_mysql)
        utils.initialize_db(db)
        utils.feed_get_account(db)
        utils.feed_get_user(db)
        cls.token =  utils.request_token('dummy', 'dummy', token_endpoint)
        cls.endpoint_base = api_base_url + '/api/v1/accounts/'
        cls.headers = {
                    'Accept' : 'application/json',
                    'Content-type' : 'application/json',
                    'Authorization': 'Bearer %s' % str(cls.token['access_token'])
                  }
        cls.session = db_reconnect(db)
        cls.data = {"id": 2,
                    "username": "dummy3",
                    "firstName": "firstname",
                    "lastName": "lastname",
                    "password" : "fgghhhhhsS1",
                    "lastLogin": None,
                    "email": "dummy@nowhere.com",
                    "phone": "+33111111111",
                    "fax": "+33111111111",
                    "authorizedSupportContact": True,
                    "roles": {
                                "userAccount": {"account": "EDIT", "configuration": "EDIT", "report": "EDIT", "content": "EDIT"},
                                "subAccounts": {"account": "EDIT", "configuration": "EDIT", "report": "EDIT", "content": "EDIT"}
                            }
                    }

    def test_wrong_account(self):
        '''
        The connected user is not associated with the account
        '''
        r = requests.put(self.endpoint_base + 'CCCCCCCC/users/2', json.dumps(self.data), \
            headers=self.headers)
        self.assertEqual(r.status_code, 403)

    def test_wrong_account_user_id(self):
        '''
        The user is not associated with that account provided
        '''
        data = dict(self.data)
        data["id"] = 4
        r = requests.put(self.endpoint_base + 'AAAAAAAA/users/4', json.dumps(data), \
            headers=self.headers)
        self.assertEqual(r.status_code, 400)

    def test_good_values(self):
        '''
        Everything is good the user should be updated
        '''
        r = requests.put(self.endpoint_base + 'AAAAAAAA/users/2', json.dumps(self.data), \
            headers=self.headers)
        self.assertEqual(r.status_code, 200)
        expected = {"id": 2, "accountHash": "AAAAAAAA", "username": "dummy3", \
            "firstName": "firstname", "lastName": "lastname", "lastLogin": None, \
            "email": "dummy@nowhere.com", "phone": "+33111111111", "fax": "+33111111111", \
            "authorizedSupportContact": True, "roles": {"userAccount": {"account": "EDIT", \
            "configuration": "EDIT", "report": "EDIT", "content": "EDIT"}, "subAccounts": \
            {"account": "EDIT", "configuration": "EDIT", "report": "EDIT", "content": "EDIT"}}}
        self.assertEqual(json.loads(r.text), expected)

    def test_inconsistent_id(self):
        '''
        id in the url path and id in daa are different
        '''
        r = requests.put(self.endpoint_base + 'AAAAAAAA/users/1', json.dumps(self.data), \
            headers=self.headers)
        self.assertEqual(r.status_code, 400)

    def test_username_exist(self):
        '''
        The username already exists (with a different id)
        '''
        data = dict(self.data)
        data["username"] = "dummy"
        r = requests.put(self.endpoint_base + 'AAAAAAAA/users/2', json.dumps(data), \
            headers=self.headers)
        self.assertEqual(r.status_code, 400)

class TestPutMe(unittest.TestCase):
    '''
    Test PUT /api/v1/users/me
    '''
    @classmethod
    def setUpClass(cls):
        utils.drop_create_db(db_hostname, db_user, db_password, db_mysql)
        utils.initialize_db(db)
        utils.feed_get_account(db)
        utils.feed_get_user(db)
        cls.token =  utils.request_token('dummy', 'dummy', token_endpoint)
        cls.endpoint_base = api_base_url + '/api/v1/users/me'
        cls.headers = {
                    'Accept' : 'application/json',
                    'Content-type' : 'application/json',
                    'Authorization': 'Bearer %s' % str(cls.token['access_token'])
                  }
        cls.session = db_reconnect(db)
        cls.data = {"id": 1,
                    "username": "dummy3",
                    "firstName": "firstname",
                    "lastName": "lastname",
                    "password" : "fgghhhhhsS1",
                    "lastLogin": None,
                    "email": "dummy@nowhere.com",
                    "phone": "+33111111111",
                    "fax": "+33111111111",
                    "authorizedSupportContact": True,
                    }

    def test_wrong_account(self):
        '''
        The user try to change his account
        '''
        data = dict(self.data)
        data["accountHash"] = 'BBBBBBBB'
        r = requests.put(self.endpoint_base, json.dumps(data), \
            headers=self.headers)
        self.assertEqual(r.status_code, 400)

    def test_with_roles(self):
        '''
        The user try to change his own roles
        '''
        data = dict(self.data)
        data["roles"] = {
            "userAccount": {"account": "EDIT", "configuration": "EDIT", "report": "EDIT", "content": "EDIT"},
            "subAccounts": {"account": "EDIT", "configuration": "EDIT", "report": "EDIT", "content": "EDIT"}
        }
        r = requests.put(self.endpoint_base, json.dumps(data), \
            headers=self.headers)
        self.assertEqual(r.status_code, 400)

    def test_wrong_id(self):
        '''
        The user try to change his id
        '''
        data = dict(self.data)
        data["id"] = 2
        r = requests.put(self.endpoint_base, json.dumps(data), \
            headers=self.headers)
        self.assertEqual(r.status_code, 400)

    def test_good_change_with_id(self):
        '''
        Id is in the request
        '''
        r = requests.put(self.endpoint_base, json.dumps(self.data), \
            headers=self.headers)
        self.assertEqual(r.status_code, 200)
        expected = {"id": 1, "accountHash": "AAAAAAAA", "username": "dummy3", \
            "firstName": "firstname", "lastName": "lastname", "lastLogin": None, \
            "email": "dummy@nowhere.com", "phone": "+33111111111", "fax": "+33111111111", \
            "authorizedSupportContact": True, "roles": {"userAccount": \
            {"account": "EDIT", "configuration": "EDIT", "report": "EDIT", "content": "EDIT"}, \
            "subAccounts": {"account": "EDIT", "configuration": "EDIT", "report": "EDIT", "content": "EDIT"}}}
        self.assertEqual(expected, json.loads(r.text))
        #Back too previous values for other tests
        data = dict(self.data)
        data["username"] = "dummy"
        data["password"] = "dummy"
        r = requests.put(self.endpoint_base, json.dumps(self.data), \
            headers=self.headers)

    def test_good_change_without_id(self):
        '''
        Without id in the request
        '''
        data = dict(self.data)
        del data["id"]
        r = requests.put(self.endpoint_base, json.dumps(data), \
            headers=self.headers)
        self.assertEqual(r.status_code, 200)
        expected = {"id": 1, "accountHash": "AAAAAAAA", "username": "dummy3", \
            "firstName": "firstname", "lastName": "lastname", "lastLogin": None, \
            "email": "dummy@nowhere.com", "phone": "+33111111111", "fax": "+33111111111", \
            "authorizedSupportContact": True, "roles": {"userAccount": \
            {"account": "EDIT", "configuration": "EDIT", "report": "EDIT", "content": "EDIT"}, \
            "subAccounts": {"account": "EDIT", "configuration": "EDIT", "report": "EDIT", "content": "EDIT"}}}
        self.assertEqual(expected, json.loads(r.text))
        #Back too previous values for other tests
        data["username"] = "dummy"
        data["password"] = "dummy"
        r = requests.put(self.endpoint_base, json.dumps(self.data), \
            headers=self.headers)

class TestDeleteUser(unittest.TestCase):
    '''
    Test DELETE /api/v1/accounts/{account_hash}/users/{user_id}
    '''
    @classmethod
    def setUpClass(cls):
        utils.drop_create_db(db_hostname, db_user, db_password, db_mysql)
        utils.initialize_db(db)
        utils.feed_get_account(db)
        utils.feed_get_user(db)
        utils.feed_delete_user(db)
        cls.token =  utils.request_token('dummy', 'dummy', token_endpoint)
        cls.endpoint_base = api_base_url + '/api/v1/accounts/'
        cls.headers = {
                    'Accept' : 'application/json',
                    'Content-type' : 'application/json',
                    'Authorization': 'Bearer %s' % str(cls.token['access_token'])
                  }
        cls.session = db_reconnect(db)

    def test_good_permission(self):
        '''
        Test with good permission
        '''
        r = requests.delete(self.endpoint_base + 'AAAAAAAA/users/2', headers=self.headers)
        self.assertEqual(r.status_code, 200)

    def test_good_permission_sub_account(self):
        '''
        Test with good permission
        '''
        r = requests.delete(self.endpoint_base + 'BBBBBBBB/users/4', headers=self.headers)
        self.assertEqual(r.status_code, 200)

    def test_delete_myself(self):
        '''
        The logged user try to delete himself
        '''
        r = requests.delete(self.endpoint_base + 'AAAAAAAA/users/1', headers=self.headers)
        self.assertEqual(r.status_code, 400)

    def test_without_permission(self):
        '''
        The user doesn't have the EDIT permission on the Account
        '''
        token = utils.request_token('Nopermission2', 'dummy', token_endpoint)
        headers = {
                    'Accept' : 'application/json',
                    'Content-type' : 'application/json',
                    'Authorization': 'Bearer %s' % str(token['access_token'])
        }
        r = requests.delete(self.endpoint_base + 'AAAAAAAA/users/1', headers=headers)
        self.assertEqual(r.status_code, 403)

    def test_wrong_account(self):
        '''
        The connected user is not associated with the account
        '''
        r = requests.delete(self.endpoint_base + 'CCCCCCCC/users/3', headers=self.headers)
        self.assertEqual(r.status_code, 403)

    def test_wrong_user_account(self):
        '''
        Wrong association between the user id and account in the URL path
        '''
        r = requests.delete(self.endpoint_base + 'BBBBBBBB/users/3', headers=self.headers)
        self.assertEqual(r.status_code, 400)

    def test_wrong_id(self):
        '''
        The user id provided is not in the DB
        '''
        r = requests.delete(self.endpoint_base + 'BBBBBBBB/users/378', headers=self.headers)
        self.assertEqual(r.status_code, 400)
