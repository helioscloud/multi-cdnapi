import unittest
import datetime
import calendar
import random
import json
import requests

from models.base import *
from models.hosts import Host
from models.analytics import ConsolidatedUsage, RealTimeUsage
from models.accounts import Contact, Account
from models.users import RoleList, RoleMatrix, User

from common import access
from unit import utils

from sqlalchemy import Table, Column, String, DateTime, ForeignKey, Enum, \
    Text, Integer, BigInteger, Boolean, func


'''
The database will be DROPPED in the process. MYSQL sleeping process
will be KILLED
'''
db_hostname = '127.0.0.1'
db_user = 'api'
db_password = 'password'
db_mysql = 'optimicdnapi'
db = 'mysql+mysqldb://api:password@127.0.0.1/optimicdnapi'
token_endpoint = 'http://127.0.0.1:8080/auth/token'
api_base_url = 'http://127.0.0.1:8080/'
memcache_servers = ['127.0.0.1:11211']


class TestObjects(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.session = db_reconnect(db)
        account = cls.session.query(Account).all()[0]
        hosts = cls.session.query(Host).filter(Host.accountHash == account.hashCode).all()
        analytics = cls.session.query(ConsolidatedUsage).filter(ConsolidatedUsage.accountHash==hosts[0].accountHash).delete()
        real_time = cls.session.query(RealTimeUsage).filter(RealTimeUsage.accountHash==hosts[0].accountHash).delete()
        date = datetime.datetime.utcnow()
        rounded_date = datetime.datetime(date.year, date.month, date.day, date.hour, date.minute-(date.minute %5), 0)
        for host in hosts[0:2]:
            hits = random.randint(300, 1500)
            downloaded_bytes = hits*random.randint(5000, 10000)
            seconddelta = 0
            while seconddelta != 3000:
                value_change = random.randint(5, 15)
                if random.randint(0,1):
                    hits = int(hits+(hits*value_change)/100)
                    downloaded_bytes = int(downloaded_bytes+(downloaded_bytes*value_change)/100)
                else:
                    hits = int(hits-(hits*value_change)/100)
                    downloaded_bytes = int(downloaded_bytes-(downloaded_bytes*value_change)/100)
                new_date = rounded_date - datetime.timedelta(seconds=seconddelta)
                new_date_timestamp = str(calendar.timegm(new_date.utctimetuple()))
                analytic = ConsolidatedUsage(accountHash=host.accountHash, hostcname=host.cname, timestamp = new_date_timestamp, hits=hits, downloaded_bytes=downloaded_bytes)
                cls.session.add(analytic)
                cls.session.commit()
                real_time = RealTimeUsage(accountHash=host.accountHash, hostcname=host.cname, timestamp = new_date_timestamp, hits=hits, bandwidth=downloaded_bytes)
                cls.session.add(real_time)
                cls.session.commit()
                seconddelta += 300

    def test_consolidated_get_analytics(self):
        def _check_data(data, res):
            self.assertEqual(len(res['series'][0]['datapoints']), len(data))
            nb_match = 0
            for db_entry in data:
                for res_entry in res['series'][0]['datapoints']:
                    if res_entry[0] == db_entry.timestamp and res_entry[1] == db_entry.hits and res_entry[2] == db_entry.downloaded_bytes:
                        nb_match += 1
                        continue
            self.assertEqual(nb_match, len(data))
        cu = ConsolidatedUsage()
        end_date = datetime.datetime.utcnow()
        start_date = datetime.datetime.utcnow()  - datetime.timedelta(seconds=3000)
        res = cu.get_analytics(start_date.strftime('%Y-%m-%dT%H:%M:00Z'), end_date.strftime('%Y-%m-%dT%H:%M:00Z'), self.session, 'AAAAAAAA', group_by='host')
        hosts = self.session.query(Host).filter(Host.accountHash == 'AAAAAAAA').all()
        self.assertEqual(len(res['series']), len(hosts))
        res = cu.get_analytics(start_date.strftime('%Y-%m-%dT%H:%M:00Z'), end_date.strftime('%Y-%m-%dT%H:%M:00Z'), self.session, 'AAAAAAAA', host=hosts[0].cname)
        self.assertEqual(len(res['series']), 1)
        data = self.session.query(ConsolidatedUsage).filter(ConsolidatedUsage.hostcname==hosts[0].cname).all()
        _check_data(data, res)
        res = cu.get_analytics(start_date.strftime('%Y-%m-%dT%H:%M:00Z'), end_date.strftime('%Y-%m-%dT%H:%M:00Z'), self.session, 'AAAAAAAA')
        start_timestamp = str(calendar.timegm(start_date.utctimetuple()))
        end_timestamp = str(calendar.timegm(end_date.utctimetuple()))
        data = self.session.query(ConsolidatedUsage.timestamp.label('timestamp'), \
            func.sum(ConsolidatedUsage.hits).label('hits'), \
            func.sum(ConsolidatedUsage.downloaded_bytes).label('downloaded_bytes')).\
            filter(ConsolidatedUsage.timestamp>start_timestamp).\
            filter(ConsolidatedUsage.timestamp<=end_timestamp).\
            filter(ConsolidatedUsage.accountHash=='AAAAAAAA').\
            group_by(ConsolidatedUsage.timestamp).all()
        self.assertEqual(len(res['series']), 1)
        _check_data(data, res)
        res = cu.get_analytics(start_date.strftime('%Y-%m-%dT%H:%M:00Z'), end_date.strftime('%Y-%m-%dT%H:%M:00Z'), self.session, 'AAAAAAAA', group_by='host')
        hosts = self.session.query(Host).filter(Host.accountHash=='AAAAAAAA').all()
        self.assertEqual(len(hosts), len(res['series']))

    def test_realtime_get_analytics(self):
        def _check_data(data, res):
            self.assertEqual(len(res['series'][0]['datapoints']), len(data))
            nb_match = 0
            for db_entry in data:
                for res_entry in res['series'][0]['datapoints']:
                    if res_entry[0] == db_entry.timestamp and res_entry[1] == db_entry.hits and res_entry[3] == db_entry.bandwidth:
                        nb_match += 1
                        continue
            self.assertEqual(nb_match, len(data))
        rt = RealTimeUsage()
        end_date = datetime.datetime.utcnow()
        start_date = datetime.datetime.utcnow()  - datetime.timedelta(seconds=3000)
        res = rt.get_analytics(start_date.strftime('%Y-%m-%dT%H:%M:00Z'), end_date.strftime('%Y-%m-%dT%H:%M:00Z'), self.session, 'AAAAAAAA', group_by='host')
        hosts = self.session.query(Host).filter(Host.accountHash == 'AAAAAAAA').all()
        self.assertEqual(len(res['series']), len(hosts))
        res = rt.get_analytics(start_date.strftime('%Y-%m-%dT%H:%M:00Z'), end_date.strftime('%Y-%m-%dT%H:%M:00Z'), self.session, 'AAAAAAAA', host=hosts[0].cname)
        self.assertEqual(len(res['series']), 1)
        data = self.session.query(RealTimeUsage).filter(RealTimeUsage.hostcname==hosts[0].cname).all()
        _check_data(data, res)
        res = rt.get_analytics(start_date.strftime('%Y-%m-%dT%H:%M:00Z'), end_date.strftime('%Y-%m-%dT%H:%M:00Z'), self.session, 'AAAAAAAA')
        start_timestamp = str(calendar.timegm(start_date.utctimetuple()))
        end_timestamp = str(calendar.timegm(end_date.utctimetuple()))
        data = self.session.query(RealTimeUsage.timestamp.label('timestamp'), \
            func.sum(RealTimeUsage.hits).label('hits'), \
            func.sum(RealTimeUsage.bandwidth).label('bandwidth')).\
            filter(RealTimeUsage.timestamp>start_timestamp).\
            filter(RealTimeUsage.timestamp<=end_timestamp).\
            filter(RealTimeUsage.accountHash=='AAAAAAAA').\
            group_by(RealTimeUsage.timestamp).all()
        self.assertEqual(len(res['series']), 1)
        _check_data(data, res)
        res = rt.get_analytics(start_date.strftime('%Y-%m-%dT%H:%M:00Z'), end_date.strftime('%Y-%m-%dT%H:%M:00Z'), self.session, 'AAAAAAAA', group_by='host')
        hosts = self.session.query(Host).filter(Host.accountHash=='AAAAAAAA').all()
        self.assertEqual(len(hosts), len(res['series']))


class TestGetAnalytics(unittest.TestCase):

        @classmethod
        def setUpClass(cls):
            cls.session = db_reconnect(db)
            cls.token =  utils.request_token('dummy', 'dummy', token_endpoint)
            cls.endpoint_base = '%sapi/v1/accounts/AAAAAAAA/analytics/transfer' % (api_base_url)
            cls.headers = {
                        'Accept' : 'application/json',
                        'Content-type' : 'application/json',
                        'Authorization': 'Bearer %s' % str(cls.token['access_token'])
                      }
            cls.consolidated_analytics_delay = 1500  # Should be the same value than in cdn_api.ini
            return

        def test_wrong_dates(self):
            end_date = datetime.datetime.utcnow() - datetime.timedelta(seconds=self.consolidated_analytics_delay*2)
            start_date ='121qsqsd'
            url = '%s?startDate=%s&endDate=%s' % (self.endpoint_base, start_date, end_date.strftime('%Y-%m-%dT%H:%M:00Z'))
            r = requests.get(url, headers=self.headers)
            self.assertEqual(r.status_code, 400)
            start_date = datetime.datetime.utcnow() - datetime.timedelta(seconds=self.consolidated_analytics_delay*10)
            end_date = 'tytyt'
            url = '%s?startDate=%s&endDate=%s' % (self.endpoint_base, start_date, end_date)
            r = requests.get(url, headers=self.headers)
            self.assertEqual(r.status_code, 400)

        def test_wrong_host(self):
            host = 'OOOOOOOOOOOOO'
            end_date = datetime.datetime.utcnow() - datetime.timedelta(seconds=self.consolidated_analytics_delay*2)
            start_date = datetime.datetime.utcnow() - datetime.timedelta(seconds=self.consolidated_analytics_delay*10)
            url = '%s?startDate=%s&endDate=%s&host=%s' % (self.endpoint_base, start_date.strftime('%Y-%m-%dT%H:%M:00Z'), end_date.strftime('%Y-%m-%dT%H:%M:00Z'), host)
            r = requests.get(url, headers=self.headers)
            self.assertEqual(r.status_code, 403)

        def test_wrong_group_by(self):
            group_by = 'rrr'
            end_date = datetime.datetime.utcnow() - datetime.timedelta(seconds=self.consolidated_analytics_delay*2)
            start_date = datetime.datetime.utcnow() - datetime.timedelta(seconds=self.consolidated_analytics_delay*10)
            url = '%s?startDate=%s&endDate=%s&group_by=%s' % (self.endpoint_base, start_date.strftime('%Y-%m-%dT%H:%M:00Z'), end_date.strftime('%Y-%m-%dT%H:%M:00Z'), group_by)
            r = requests.get(url, headers=self.headers)
            self.assertEqual(r.status_code, 400)

        def test_group_by_host(self):
            end_date = datetime.datetime.utcnow() - datetime.timedelta(seconds=self.consolidated_analytics_delay*2)
            start_date = datetime.datetime.utcnow() - datetime.timedelta(seconds=self.consolidated_analytics_delay*4)
            url = '%s?startDate=%s&endDate=%s&group_by=host' % (self.endpoint_base, start_date.strftime('%Y-%m-%dT%H:%M:00Z'), end_date.strftime('%Y-%m-%dT%H:%M:00Z'))
            r = requests.get(url, headers=self.headers)
            self.assertEqual(r.status_code, 200)

        def test_with_host(self):
            end_date = datetime.datetime.utcnow() - datetime.timedelta(seconds=self.consolidated_analytics_delay*6)
            start_date = datetime.datetime.utcnow() - datetime.timedelta(seconds=self.consolidated_analytics_delay*20)
            url = '%s?startDate=%s&endDate=%s&host=dummyhost' % (self.endpoint_base, start_date.strftime('%Y-%m-%dT%H:%M:00Z'), end_date.strftime('%Y-%m-%dT%H:%M:00Z'))
            r = requests.get(url, headers=self.headers)
            self.assertEqual(r.status_code, 200)

        def test_account(self):
            end_date = datetime.datetime.utcnow() - datetime.timedelta(seconds=self.consolidated_analytics_delay*6)
            start_date = datetime.datetime.utcnow() - datetime.timedelta(seconds=self.consolidated_analytics_delay*20)
            url = '%s?startDate=%s&endDate=%s' % (self.endpoint_base, start_date.strftime('%Y-%m-%dT%H:%M:00Z'), end_date.strftime('%Y-%m-%dT%H:%M:00Z'))
            r = requests.get(url, headers=self.headers)
            self.assertEqual(r.status_code, 200)

        def test_with_only_rt(self):
            end_date = datetime.datetime.utcnow()
            start_date = datetime.datetime.utcnow() - datetime.timedelta(seconds=self.consolidated_analytics_delay/2)
            url = '%s?startDate=%s&endDate=%s&host=dummyhost' % (self.endpoint_base, start_date.strftime('%Y-%m-%dT%H:%M:00Z'), end_date.strftime('%Y-%m-%dT%H:%M:00Z'))
            r = requests.get(url, headers=self.headers)
            self.assertEqual(r.status_code, 200)

        def test_with_cu_and_rt(self):
            end_date = datetime.datetime.utcnow()
            start_date = datetime.datetime.utcnow() - datetime.timedelta(seconds=self.consolidated_analytics_delay*2)
            url = '%s?startDate=%s&endDate=%s&host=dummyhost' % (self.endpoint_base, start_date.strftime('%Y-%m-%dT%H:%M:00Z'), end_date.strftime('%Y-%m-%dT%H:%M:00Z'))
            r = requests.get(url, headers=self.headers)
            self.assertEqual(r.status_code, 200)
