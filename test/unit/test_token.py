
import unittest

import requests
import memcache

from unit import utils
from models.base import *
from models.accounts import Contact, Account
from models.services import Service
from models.users import RoleList, RoleMatrix, User


'''
The database will be DROPPED in the process. MYSQL sleeping process
will be KILLED
'''
db_hostname = '127.0.0.1'
db_user = 'api'
db_password = 'password'
db_mysql = 'optimicdnapi'
db = 'mysql+mysqldb://api:password@127.0.0.1/optimicdnapi'

service_endpoint = 'http://127.0.0.1:8080/auth/token'
memcache_servers = ['127.0.0.1:11211']

class TestAuthToken(unittest.TestCase):
    '''
    Test the token endpoint
    '''
    @classmethod
    def setUpClass(cls):
        utils.drop_create_db(db_hostname, db_user, db_password, db_mysql)
        utils.initialize_db(db)
        cls.session = db_reconnect(db)

    def _update_account_status(self, hashCode, status):
        account = self.session.query(Account).\
            filter(Account.hashCode == hashCode).one()
        account.accountStatus = status
        self.session.add(account)
        self.session.commit()

    def test_wrong_credential(self):
        data = '{"grant_type": "password","username": "dummy","password": "wrong"}'
        self._update_account_status('FFFFFFFFFF', 'ACTIVATED')
        r = requests.post(service_endpoint, data, headers={
          "Accept": "application/json"
        })
        self.assertEqual(r.status_code, 403)

    def test_good_credential(self):
        utils.feed_token_data(db)
        data = '{"grant_type": "password","username": "dummy","password": "dummy"}'
        self._update_account_status('FFFFFFFFFF', 'ACTIVATED')
        r = requests.post(service_endpoint, data, headers={
          "Accept": "application/json"
        })
        self.assertEqual(r.status_code, 200)
        token = r.json()
        mc = memcache.Client(memcache_servers)
        memcache_token = mc.get(token['access_token'])
        self.assertEqual(memcache_token['user'].username, 'dummy')

    def test_good_credential_suspended_account(self):
        self._update_account_status('FFFFFFFFFF', 'SUSPENDED')
        data = '{"grant_type": "password","username": "dummy","password": "dummy"}'
        r = requests.post(service_endpoint, data, headers={
          "Accept": "application/json"
        })
        self.assertEqual(r.status_code, 403)

    def test_good_credential_deleted_account(self):
        self._update_account_status('FFFFFFFFFF', 'DELETED')
        data = '{"grant_type": "password","username": "dummy","password": "dummy"}'
        r = requests.post(service_endpoint, data, headers={
          "Accept": "application/json"
        })
        self.assertEqual(r.status_code, 403)
