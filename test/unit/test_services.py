import unittest

import requests
import json
from unit import utils

from unit import utils
from models.base import *
from models.accounts import Contact, Account
from models.services import Service
from models.users import RoleList, RoleMatrix, User


db = 'mysql+mysqldb://api:password@127.0.0.1/optimicdnapi'
db_hostname = '127.0.0.1'
db_user = 'api'
db_password = 'password'
db_mysql = 'optimicdnapi'
token_endpoint = 'http://127.0.0.1:8080/auth/token'
api_base_url = 'http://127.0.0.1:8080/'

class TestGetServices(unittest.TestCase):
    '''
    Test  GET /api/v1/accounts/{account_hash}/services
    '''
    @classmethod
    def setUpClass(cls):
        utils.drop_create_db(db_hostname, db_user, db_password, db_mysql)
        utils.initialize_db(db)
        utils.feed_get_account(db)
        utils.feed_get_services(db)
        cls.token =  utils.request_token('dummy', 'dummy', token_endpoint)
        cls.endpoint_base = api_base_url + 'api/v1/accounts/'
        cls.headers = {
                    'Accept' : 'application/json',
                    'Content-type' : 'application/json',
                    'Authorization': 'Bearer %s' % str(cls.token['access_token'])
                  }
        cls.session = db_reconnect(db)

    def test_wrong_account(self):
        r = requests.get(self.endpoint_base + 'CCCCCCCC/services', headers=self.headers)
        self.assertEqual(r.status_code, 403)

    def test_good_account(self):
        r = requests.get(self.endpoint_base + 'AAAAAAAA/services', headers=self.headers)
        self.assertEqual(r.status_code, 200)
        json_data = r.json()
        self.assertEqual(json_data, [{"id": 1, "name": "MonoCDN", "description": "MonoCDN Service", \
            "multiCdnService": False}, {"id": 3, "name": "CDN Premium", "description": "Premium CDN MAP", \
            "multiCdnService": False}])

    def test_sub_account(self):
        r = requests.get(self.endpoint_base + 'KKKKKKKK/services', headers=self.headers)
        self.assertEqual(r.status_code, 200)
        json_data = r.json()
        self.assertEqual(json_data, [{"id": 1, "name": "MonoCDN", "description": "MonoCDN Service", \
            "multiCdnService": False}])
