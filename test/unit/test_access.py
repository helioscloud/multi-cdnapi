import unittest

from models.base import *
from models.accounts import Contact, Account
from models.users import RoleList, RoleMatrix, User

from common import access
from unit import utils


'''
The database will be DROPPED in the process. MYSQL sleeping process
will be KILLED
'''
db_hostname = '127.0.0.1'
db_user = 'api'
db_password = 'password'
db_mysql = 'optimicdnapi'
db = 'mysql+mysqldb://api:password@127.0.0.1/optimicdnapi'
token_endpoint = 'http://127.0.0.1:8080/auth/token'
api_base_url = 'http://127.0.0.1:8080/'
memcache_servers = ['127.0.0.1:11211']


class TestAccess(unittest.TestCase):

        @classmethod
        def setUpClass(cls):
            utils.drop_create_db(db_hostname, db_user, db_password, db_mysql)
            utils.initialize_db(db)
            utils.feed_get_account(db)
            cls.categories = ['account', 'configuration', 'report', 'content']
            cls.role_list = RoleList()
            for cat in cls.categories:
                setattr(cls.role_list, cat, 'NONE')
            cls.user_permission = { 'account': cls.role_list,
                                    'sub_accounts': cls.role_list
                                }
            cls.session = db_reconnect(db)
            cls.user = cls.session.query(User).filter(User.id == 1).one()

        def test_check_user_permission(self):
            account_hashCode = 'CCCCCCCC'
            for cat in self.categories:
                for permission in ['EDIT', 'READ']:
                    res = access.check_user_permissions(self.user, self.user_permission, account_hashCode, cat, permission, self.session)
                    self.assertFalse(res)
            for account_level in ['account', 'sub_accounts']:
                if account_level == 'account':
                    account_hashCode = 'AAAAAAAA'
                else:
                    account_hashCode = 'BBBBBBBB'
                for cat in self.categories:
                    permission = 'EDIT'
                    setattr(self.user_permission[account_level], cat, 'EDIT')
                    res = access.check_user_permissions(self.user, self.user_permission, account_hashCode, cat, permission, self.session)
                    self.assertTrue(res)
                    setattr(self.user_permission[account_level], cat, 'NONE')
                    res = access.check_user_permissions(self.user, self.user_permission, account_hashCode, cat, permission, self.session)
                    self.assertFalse(res)
                    setattr(self.user_permission[account_level], cat, 'READ')
                    res = access.check_user_permissions(self.user, self.user_permission, account_hashCode, cat, permission, self.session)
                    self.assertFalse(res)
                    permission = 'READ'
                    setattr(self.user_permission[account_level], cat, 'EDIT')
                    res = access.check_user_permissions(self.user, self.user_permission, account_hashCode, cat, permission, self.session)
                    self.assertTrue(res)
                    setattr(self.user_permission[account_level], cat, 'READ')
                    res = access.check_user_permissions(self.user, self.user_permission, account_hashCode, cat, permission, self.session)
                    self.assertTrue(res)
                    setattr(self.user_permission[account_level], cat, 'NONE')
                    res = access.check_user_permissions(self.user, self.user_permission, account_hashCode, cat, permission, self.session)
                    self.assertFalse(res)
