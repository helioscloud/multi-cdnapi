import unittest

import requests
import memcache
import json

from unit import utils
from models.base import *
from models.accounts import Contact, Account
from models.services import Service
from models.users import RoleList, RoleMatrix, User

'''
The database will be DROPPED in the process. MYSQL sleeping process
will be KILLED
'''
db_hostname = '127.0.0.1'
db_user = 'api'
db_password = 'password'
db_mysql = 'optimicdnapi'
db = 'mysql+mysqldb://api:password@127.0.0.1/optimicdnapi'
token_endpoint = 'http://127.0.0.1:8080/auth/token'
api_base_url = 'http://127.0.0.1:8080/'
memcache_servers = ['127.0.0.1:11211']


class TestGetAccount(unittest.TestCase):
    '''
    Test GET /api/v1/accounts/{account_hash} endpoint
    '''
    @classmethod
    def setUpClass(cls):
        utils.drop_create_db(db_hostname, db_user, db_password, db_mysql)
        utils.initialize_db(db)
        utils.feed_get_account(db)
        cls.token =  utils.request_token('dummy', 'dummy', token_endpoint)
        cls.endpoint_base = api_base_url + 'api/v1/accounts/'
        cls.headers = {
                    'Accept' : 'application/json',
                    'Content-type' : 'application/json',
                    'Authorization': 'Bearer %s' % str(cls.token['access_token'])
                  }
        cls.session = db_reconnect(db)

    def test_get_without_token_and_account(self):
        '''
        test without token
        '''
        r = requests.get(self.endpoint_base + 'AAAAAAAA')
        self.assertEqual(r.status_code, 403)

    def test_get_with_good_credential_and_account(self):
        '''
        The authentified user is attached to the account and has permission to
        read it
        '''
        account = self.session.query(Account).\
            filter(Account.hashCode == 'AAAAAAAA').one()
        all_services = self.session.query(Service)
        for service in all_services:
            account.services.append(service)
        self.session.add(account)
        self.session.commit()
        r = requests.get(self.endpoint_base + 'AAAAAAAA', headers=self.headers)
        self.assertEqual(r.status_code, 200)
        contact = {u'fax': None, u'firstName': None, u'lastName': None, \
            u'email': None, u'phone': None, u'id': 1}
        services =  [{u'description': u'MonoCDN Service', u'multiCdnService': False, u'id': 1, u'name': u'MonoCDN'},\
            {u'description': u'Global CDN MAP', u'multiCdnService': False, u'id': 2, u'name': u'CDN Global'}, \
            {u'description': u'Premium CDN MAP', u'multiCdnService': False, u'id': 3, u'name': u'CDN Premium'}]
        json_data = r.json()
        self.assertEqual(json_data['technicalContact'], contact)
        self.assertEqual(json_data['primaryContact'], contact)
        self.assertEqual(json_data['billingContact'], contact)
        self.assertEqual(json_data['accountStatus'], 'ACTIVATED')
        self.assertEqual(json_data['accountName'], 'ParentAccount')
        self.assertEqual(json_data['maximumDirectSubAccounts'], 200)
        self.assertEqual(json_data['subAccountCreationEnabled'], True)
        self.assertEqual(json_data['services'], services)
        self.assertEqual(json_data['hashCode'], 'AAAAAAAA')
        self.assertEqual(json_data['id'], 1)

    def test_get_with_good_credential_and_wrong_account(self):
        '''
        The authentified user is not attached to the account and the account is
        not a subaccount of the user's account
        '''
        r = requests.get(self.endpoint_base + 'CCCCCCCC', headers=self.headers)
        r = self.assertEqual(r.status_code, 403)

    def test_get_with_good_credential_and_deleted_subaccount(self):
        '''
        The authentified user can READ the subaccount and the subaccout status
        is DELETED
        '''
        r = requests.get(self.endpoint_base + 'DDDDDDDD', headers=self.headers)
        r = self.assertEqual(r.status_code, 403)

    def test_get_with_good_credential_and_suspended_subaccount(self):
        '''
        The authentified user can READ the subaccount but the subaccout status
        is SUSPENDED
        '''
        r = requests.get(self.endpoint_base + 'EEEEEEEE', headers=self.headers)
        r = self.assertEqual(r.status_code, 200)

    def test_get_subaccount_with_permissions(self):
        '''
        The account requested is a subaccount of the user's account and the
        user has the READ permission on the subaccount
        '''
        r = requests.get(self.endpoint_base + 'BBBBBBBB', headers=self.headers)
        r = self.assertEqual(r.status_code, 200)

    def test_get_subaccount_without_permissions(self):
        '''
        The account requested is a subaccount of the user's account but the
        user has no READ permission on the subaccount
        '''
        user = self.session.query(User).filter(User.username == "dummy").one()
        user.roles = 81
        self.session.add(user)
        self.session.commit()
        token = utils.request_token('dummy', 'dummy', token_endpoint)
        headers = {
                            'Accept' : 'application/json',
                            'Content-type' : 'application/json',
                            'Authorization': 'Bearer %s' % str(token['access_token'])
                        }
        r = requests.get(self.endpoint_base + 'BBBBBBBB', headers=headers)

        user.roles = 1
        self.session.add(user)
        self.session.commit()
        r = self.assertEqual(r.status_code, 403)

    def test_get_display_subaccounts(self):
        '''
        The account has some subaccounts and the option
        displayed_subaccount is True
        '''
        r = requests.get(self.endpoint_base + 'AAAAAAAA?display_subaccounts=true'\
            , headers=self.headers)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.json()['sub_accounts'], ['BBBBBBBB', 'DDDDDDDD',\
            'EEEEEEEE'])

class TestDeleteAccount(unittest.TestCase):
    '''
    Test DELETE /api/v1/accounts/{account_hash} endpoint
    '''
    @classmethod
    def setUpClass(cls):
        utils.drop_create_db(db_hostname, db_user, db_password, db_mysql)
        utils.initialize_db(db)
        utils.feed_get_account(db)
        cls.token =  utils.request_token('dummy', 'dummy', token_endpoint)
        cls.endpoint_base = api_base_url + 'api/v1/accounts/'
        cls.headers = {
                    'Accept' : 'application/json',
                    'Content-type' : 'application/json',
                    'Authorization': 'Bearer %s' % str(cls.token['access_token'])
                  }
        cls.session = db_reconnect(db)


    def test_delete_account_with_good_permission(self):
        '''
        The user has the EDIT permission on his own account.
        '''
        r = requests.delete(self.endpoint_base + 'AAAAAAAA', headers=self.headers)
        self.assertEqual(r.status_code, 200)
        account = self.session.query(Account).\
            filter(Account.hashCode == 'AAAAAAAA').one()
        self.assertEqual(account.accountStatus, 'DELETED')
        account.accountStatus = 'ACTIVATED'
        self.session.add(account)
        self.session.commit()

    def test_delete_subaccount_with_good_permission(self):
        '''
        The account to delete is a sub-account of the user'account and
        the user has the EDIT permission on subaccounts.
        '''
        r = requests.delete(self.endpoint_base + 'BBBBBBBB', headers=self.headers)
        account = self.session.query(Account).\
            filter(Account.hashCode == 'BBBBBBBB').one()
        self.assertEqual(account.accountStatus, 'DELETED')
        account.accountStatus = 'ACTIVATED'
        self.session.add(account)
        self.session.commit()
        self.assertEqual(r.status_code, 200)
        pass

    def test_delete_subaccount_without_good_permission(self):
        '''
        The account to delete is a sub-account of the user'account but
        the user doesn't have the EDIT permission on subaccounts.
        '''
        user = self.session.query(User).filter(User.username == "dummy").one()
        user.roles = 81
        self.session.add(user)
        self.session.commit()
        token = utils.request_token('dummy', 'dummy', token_endpoint)
        headers = {
                            'Accept' : 'application/json',
                            'Content-type' : 'application/json',
                            'Authorization': 'Bearer %s' % str(token['access_token'])
                        }
        r = requests.delete(self.endpoint_base + 'BBBBBBBB', headers=headers)
        user.roles = 1
        self.session.add(user)
        self.session.commit()
        r = self.assertEqual(r.status_code, 403)

    def test_delete_account_without_permission(self):
        '''
        The user doesn't have the EDIT permission on his own account
        '''
        user = self.session.query(User).filter(User.username == "dummy").one()
        user.roles = 6561
        self.session.add(user)
        self.session.commit()
        token = utils.request_token('dummy', 'dummy', token_endpoint)
        headers = {
                            'Accept' : 'application/json',
                            'Content-type' : 'application/json',
                            'Authorization': 'Bearer %s' % str(token['access_token'])
                        }
        r = requests.delete(self.endpoint_base + 'AAAAAAAA', headers=headers)
        user.roles = 1
        self.session.add(user)
        self.session.commit()
        r = self.assertEqual(r.status_code, 403)

class TestPostAccount(unittest.TestCase):
    '''
    Test POST /api/v1/accounts/{account_hash} endpoint
    '''
    @classmethod
    def setUpClass(cls):
        utils.drop_create_db(db_hostname, db_user, db_password, db_mysql)
        utils.initialize_db(db)
        utils.feed_get_account(db)
        cls.token =  utils.request_token('dummy', 'dummy', token_endpoint)
        cls.endpoint_base = api_base_url + 'api/v1/accounts/'
        cls.headers = {
                    'Accept' : 'application/json',
                    'Content-type' : 'application/json',
                    'Authorization': 'Bearer %s' % str(cls.token['access_token'])
                  }
        cls.session = db_reconnect(db)
        cls.data = {
                    "accountName": "TestGoodPostAcccount",
                    "accountStatus": "ACTIVATED",
                    "maximumDirectSubAccounts": 10,
                    "subAccountCreationEnabled": False,
                    "primaryContact" :  {
                                          "firstName": "firstprimary",
                                          "lastName": "lastprimary",
                                          "email": "emailprimary",
                                          "phone": "phoneprimary",
                                          "fax": "faxprimary"
                                        },
                    "technicalContact" :  {
                                          "firstName": "firsttechnical",
                                          "lastName": "lasttechnical",
                                          "email": "emailtechnical",
                                          "phone": "phonetechnical",
                                          "fax": "faxtechnical"
                                        },
                    "billingContact" :  {
                                          "firstName": "firstbilling",
                                          "lastName": "lastbilling",
                                          "email": "emailbilling",
                                          "phone": "phonebilling",
                                          "fax": "faxbilling"
                                        },
                    "services" : [{"id" : 1}]
                    }

    def test_user_without_conf_permission(self):
        '''
        Test with a user without the CONF WRITE permission
        '''
        user = self.session.query(User).filter(User.username == "dummy").one()
        user.roles = 6561
        self.session.add(user)
        self.session.commit()
        token = utils.request_token('dummy', 'dummy', token_endpoint)
        headers = {
                            'Accept' : 'application/json',
                            'Content-type' : 'application/json',
                            'Authorization': 'Bearer %s' % str(token['access_token'])
                        }
        r = requests.post(self.endpoint_base + 'AAAAAAAA', json.dumps(self.data), \
            headers=headers)
        user.roles = 1
        self.session.commit()
        self.assertEqual(r.status_code, 403)

    def test_user_without_subaccount_conf_permission(self):
        '''
        Test to add on a subaccount with a user without
        the CONF WRITE permission on subaccount
        '''
        user = self.session.query(User).filter(User.username == "dummy").one()
        user.roles = 81
        self.session.add(user)
        self.session.commit()
        token = utils.request_token('dummy', 'dummy', token_endpoint)
        headers = {
                            'Accept' : 'application/json',
                            'Content-type' : 'application/json',
                            'Authorization': 'Bearer %s' % str(token['access_token'])
                        }
        data = "{}"
        r = requests.post(self.endpoint_base + 'BBBBBBBB', data, \
            headers=headers)
        user.roles = 1
        self.session.commit()
        self.assertEqual(r.status_code, 403)

    def test_wrong_parent_account(self):
        '''
        Test post account with good values but the user's account is not the
        parent account or a subaccount of the user's account
        '''
        data = "{}"
        r = requests.post(self.endpoint_base + 'CCCCCCCC', data, \
            headers=self.headers)
        self.assertEqual(r.status_code, 403)

    def test_good_request(self):
        '''
        Test post account with good full values.
        The user's account is the parent account.
        '''
        r = requests.post(self.endpoint_base + 'AAAAAAAA', json.dumps(self.data), \
            headers=self.headers)
        self.assertEqual(r.status_code, 201)
        data = json.loads(r.text)
        self.assertTrue(isinstance(data['id'], int))
        del(data['id'])
        self.assertTrue(isinstance(data['hashCode'], unicode))
        del(data['hashCode'])
        del(data['primaryContact']['id'])
        self.assertEqual(data['primaryContact'], {"firstName": "firstprimary", "lastName": "lastprimary", \
        "email": "emailprimary", "phone": "phoneprimary", "fax": "faxprimary"})
        del(data['primaryContact'])
        del(data['billingContact']['id'])
        self.assertEqual(data['billingContact'], {"firstName": "firstbilling", "lastName": \
        "lastbilling", "email": "emailbilling", "phone": "phonebilling", \
        "fax": "faxbilling"})
        del(data['billingContact'])
        del(data['technicalContact']['id'])
        self.assertEqual(data['technicalContact'], {"firstName": \
        "firsttechnical", "lastName": "lasttechnical", "email": "emailtechnical", \
        "phone": "phonetechnical", "fax": "faxtechnical"})
        del(data['technicalContact'])
        expected_data = \
        {"accountName": "TestGoodPostAcccount", "accountStatus": "ACTIVATED", \
        "maximumDirectSubAccounts": 10, "subAccountCreationEnabled": False, \
        "parentId": 1, "services": [{"id": 1, "name": "MonoCDN", "description": \
        "MonoCDN Service", "multiCdnService": False}]}
        self.assertEqual(data, expected_data)

    def test_good_parent_account_user_subaccount(self):
        '''
        Test post account with good permission and full values
        '''
        r = requests.post(self.endpoint_base + 'BBBBBBBB', json.dumps(self.data), \
            headers=self.headers)
        self.assertEqual(r.status_code, 201)

    def test_wrong_service_list(self):
        '''
        One of the service id is not in the service list of the parent account
        The service should be ignored and not added to the new account
        '''
        new_data = dict(self.data)
        new_data['services'].append({"id" : 2})
        r = requests.post(self.endpoint_base + 'AAAAAAAA', json.dumps(self.data), \
            headers=self.headers)
        res = json.loads(r.text)
        self.assertEqual(r.status_code, 201)
        self.assertEqual(res['services'], [{"id": 1, "name": "MonoCDN", \
            "description": "MonoCDN Service", "multiCdnService": False}])

    def test_without_service_list(self):
        '''
        No service provided , the parent's services list should be used
        '''
        new_data = dict(self.data)
        del(new_data['services'])
        r = requests.post(self.endpoint_base + 'AAAAAAAA', json.dumps(new_data), \
            headers=self.headers)
        res = json.loads(r.text)
        self.assertEqual(r.status_code, 201)
        self.assertEqual(res['services'], [
            {u'description': u'MonoCDN Service', u'multiCdnService': False, u'id': 1, u'name': u'MonoCDN'},
            {u'description': u'Premium CDN MAP', u'multiCdnService': False, u'id': 3, u'name': u'CDN Premium'}])

    def test_without_contact_list(self):
        '''
        No contacts list provided, the parent's contacts should be used
        '''
        new_data = dict(self.data)
        for contact in ['primaryContact', 'billingContact', 'technicalContact']:
            del(new_data[contact])
        r = requests.post(self.endpoint_base + 'AAAAAAAA', json.dumps(new_data), \
            headers=self.headers)
        res = json.loads(r.text)
        parent_contact = {"id": 1, "firstName": None, "lastName": None, "email": None, "phone": None, "fax": None}
        for contact in ['primaryContact', 'billingContact', 'technicalContact']:
            self.assertEqual(res[contact], parent_contact)
        self.assertEqual(r.status_code, 201)

    def test_subaccount_false(self):
        '''
        subAccountCreationEnabled of the parent account is false
        '''
        parent = self.session.query(Account).\
            filter(Account.hashCode == 'AAAAAAAA').one()
        parent.subAccountCreationEnabled = False
        self.session.add(parent)
        self.session.commit()
        r = requests.post(self.endpoint_base + 'AAAAAAAA', json.dumps(self.data), \
                headers=self.headers)
        self.assertEqual(r.status_code, 400)
        parent.subAccountCreationEnabled = True
        self.session.add(parent)
        self.session.commit()

    def test_maximum_subaccount(self):
        '''
        number of subaccount >=  maximumDirectSubAccounts
        '''
        parent = self.session.query(Account).\
            filter(Account.hashCode == 'AAAAAAAA').one()
        parent.maximumDirectSubAccounts = 1
        self.session.add(parent)
        self.session.commit()
        r = requests.post(self.endpoint_base + 'AAAAAAAA', json.dumps(self.data), \
                headers=self.headers)
        self.assertEqual(r.status_code, 400)
        parent.maximumDirectSubAccounts = 200
        self.session.add(parent)
        self.session.commit()

class TestPutAccount(unittest.TestCase):
    '''
    Test PUT /api/v1/accounts/{account_hash} endpoint
    Most of the tests are done for POST, we just add specfics tests for PUT
    '''
    @classmethod
    def setUpClass(cls):
        utils.drop_create_db(db_hostname, db_user, db_password, db_mysql)
        utils.initialize_db(db)
        utils.feed_get_account(db)
        utils.feed_put_account(db)
        cls.token =  utils.request_token('dummy', 'dummy', token_endpoint)
        cls.endpoint_base = api_base_url + 'api/v1/accounts/'
        cls.headers = {
                    'Accept' : 'application/json',
                    'Content-type' : 'application/json',
                    'Authorization': 'Bearer %s' % str(cls.token['access_token'])
                  }
        cls.session = db_reconnect(db)
        cls.data = {
                    "accountName": "UpdateAcccount",
                    "accountStatus": "ACTIVATED",
                    "maximumDirectSubAccounts": 10,
                    "subAccountCreationEnabled": False,
                    "primaryContact" :  {
                                          "firstName": "firstprimary",
                                          "lastName": "lastprimary",
                                          "email": "emailprimary",
                                          "phone": "phoneprimary",
                                          "fax": "faxprimary"
                                        },
                    "technicalContact" :  {
                                          "firstName": "firsttechnical",
                                          "lastName": "lasttechnical",
                                          "email": "emailtechnical",
                                          "phone": "phonetechnical",
                                          "fax": "faxtechnical"
                                        },
                    "billingContact" :  {
                                          "firstName": "firstbilling",
                                          "lastName": "lastbilling",
                                          "email": "emailbilling",
                                          "phone": "phonebilling",
                                          "fax": "faxbilling"
                                        },
                    "services" : [{"id" : 1}]
                    }

    def test_full_update(self):
        r = requests.put(self.endpoint_base + 'EEEEEEEE', json.dumps(self.data), \
            headers=self.headers)
        self.assertEqual(r.status_code, 200)
        data = json.loads(r.text)
        del(data['primaryContact']['id'])
        self.assertEqual(data['primaryContact'], {"firstName": "firstprimary", "lastName": "lastprimary", \
        "email": "emailprimary", "phone": "phoneprimary", "fax": "faxprimary"})
        del(data['primaryContact'])
        del(data['billingContact']['id'])
        self.assertEqual(data['billingContact'], {"firstName": "firstbilling", "lastName": \
        "lastbilling", "email": "emailbilling", "phone": "phonebilling", \
        "fax": "faxbilling"})
        del(data['billingContact'])
        del(data['technicalContact']['id'])
        self.assertEqual(data['technicalContact'], {"firstName": \
        "firsttechnical", "lastName": "lasttechnical", "email": "emailtechnical", \
        "phone": "phonetechnical", "fax": "faxtechnical"})
        del(data['technicalContact'])
        expected_data = \
        {"id": 5, "hashCode": 'EEEEEEEE', "accountName": "UpdateAcccount", "accountStatus": "ACTIVATED", \
        "maximumDirectSubAccounts": 10, "subAccountCreationEnabled": False, \
        "parentId": 1, "services": [{"id": 1, "name": "MonoCDN", "description": \
        "MonoCDN Service", "multiCdnService": False}]}
        self.assertEqual(data, expected_data)

    def test_wrong_hascode(self):
        '''
        Test a wrong account
        '''
        r = requests.put(self.endpoint_base + 'TTTTTTTTTTT', json.dumps(self.data), \
                headers=self.headers)
        self.assertEqual(r.status_code, 403)

    def test_update_contact_parent_unchanged(self):
        '''
        Update contacts. The parent has the sames, new contacts are created
        and the parent contact is left unchanged
        '''
        r = requests.put(self.endpoint_base + 'BBBBBBBB', json.dumps(self.data), \
                headers=self.headers)
        session = db_reconnect(db)
        account = session.query(Account).\
            filter(Account.hashCode == 'BBBBBBBB').one()
        self.assertEqual(r.status_code, 200)
        self.assertNotEqual(account.primaryContact, 1)
        self.assertNotEqual(account.billingContact, 1)
        self.assertNotEqual(account.technicalContact, 1)

    def test_update_contact_parent_contact_different(self):
        '''
        Update a contact. Parent contact is different.
        The contact is updated (same id in the DB)
        '''
        r = requests.put(self.endpoint_base + 'HHHHHHHH', json.dumps(self.data), \
                headers=self.headers)
        account = self.session.query(Account).\
            filter(Account.hashCode == 'HHHHHHHH').one()
        self.assertEqual(r.status_code, 200)
        self.assertEqual(account.primaryContact, 15)
        self.assertEqual(account.billingContact, 16)
        self.assertEqual(account.technicalContact, 17)
        data = json.loads(r.text)
        self.assertEqual(data['primaryContact'], {"firstName": "firstprimary", "lastName": "lastprimary", \
        "email": "emailprimary", "phone": "phoneprimary", "fax": "faxprimary", "id": 15})

    def test_update_deleted_account(self):
        '''
        Deleted account can't be updated
        '''
        r = requests.put(self.endpoint_base + 'DDDDDDDD', json.dumps(self.data), \
                headers=self.headers)
        self.assertEqual(r.status_code, 403)

    def test_wrong_service_list(self):
        '''
        One of the service id is not in the service list of the parent account
        The service should be ignored and not added to the new account
        '''
        new_data = dict(self.data)
        new_data['services'].append({"id" : 2})
        r = requests.put(self.endpoint_base + 'HHHHHHHH', json.dumps(self.data), \
            headers=self.headers)
        res = json.loads(r.text)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(res['services'], [{"id": 1, "name": "MonoCDN", \
            "description": "MonoCDN Service", "multiCdnService": False}])

    def test_without_service_list(self):
        '''
        No service provided , the parent's services list should be used
        '''
        new_data = dict(self.data)
        del(new_data['services'])
        r = requests.put(self.endpoint_base + 'JJJJJJJJ', json.dumps(new_data), \
            headers=self.headers)
        res = json.loads(r.text)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(res['services'], [
            {u'description': u'MonoCDN Service', u'multiCdnService': False, u'id': 1, u'name': u'MonoCDN'},
            {u'description': u'Premium CDN MAP', u'multiCdnService': False, u'id': 3, u'name': u'CDN Premium'}])

    def test_change_service_list(self):
        '''
        Change the service list, previous service should be removed
        '''
        r = requests.put(self.endpoint_base + 'IIIIIIII', json.dumps(self.data), \
            headers=self.headers)
        res = json.loads(r.text)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(res['services'], [
            {u'description': u'MonoCDN Service', u'multiCdnService': False, u'id': 1, u'name': u'MonoCDN'}])
