#!/usr/bin/env python

import memcache
import string
import random
import hashlib

# from unit.test_token import TestAuthToken
# from unit.test_account import TestGetAccount, TestPostAccount, TestPutAccount
# from unit.test_services import TestGetServices
# from unit.test_users import TestDeleteUser, TestPutMe, TestObjects, TestPutUser, TestPostUsers, TestGetUsers, TestGetUser, TestGetMe
# from unit.test_hosts import TestObjects,TestHostname, TestOrigin, TestHostApiEndpoints, TestScope
# from unit.test_access import TestAccess
from unit.test_analytics import TestGetAnalytics  # TestObjects

import unittest


if __name__ == '__main__':
    unittest.main()
