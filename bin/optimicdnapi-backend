#!/usr/bin/env python

import ConfigParser
import logging
import yaml
import argparse
from common import utils
import sys
import signal
import backend
from multiprocessing import Process, freeze_support, active_children

def load_provs_from_config(prov_file, logger):
    print prov_file
    try:
        f = open(prov_file, 'r')
    except IOError:
        logger.error('Unable to find providers %s', prov_file)
    raw_cfg = f.read()
    return yaml.load(raw_cfg)

def parse_args():
    '''
    Parse arguments from command line
    '''
    desc = 'Backend runner'
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('-c', '--config', dest='config', required=True,
                        help='Configuration file')
    parser.add_argument('--threads', dest='thread_count', default=3, type=int,
                        help='Number of backend purge threads')
    parser.add_argument('--debug', dest='debug')
    return parser.parse_args()

def parse_config(config_file, logger):
    '''
    Parse config from config file. Return global config.
    '''
    try:
        f = open(config_file, 'r')
    except IOError:
        logger.error('Unable to find %s', config_file)
    conf_dict = {}
    conf = ConfigParser.ConfigParser()
    conf.readfp(f)
    sections = ['DEFAULT', 'BACKEND']
    for section in sections:
        for k, v in conf.items(section):
            if k == 'providers_config':
                conf_dict['providers'] = load_provs_from_config(v, logger)
            conf_dict[k] = v
    return conf_dict

def __backend(config):
    '''Starts parser backend process'''
    backend_service = backend.BackendService(config)
    backend_service.start()
    backend_service.wait()

if __name__ == '__main__':
    __name__ = 'cdnbackend'
    LOG = utils.logger_init(name=__name__, logpath='.')
    LOG.info('Starting %s', __name__)
    def on_sigint(signum, frame):
        for child in active_children():
            LOG.info("SIGINT received. PID %s exiting ...", child.pid)
            child.terminate()
    args = parse_args()
    if args.debug:
        LOG.setLevel(logging.DEBUG)
    conf = parse_config(args.config, LOG)
    signal.signal(signal.SIGINT, on_sigint)
    for i in range(args.thread_count):
        p = Process(target=__backend, args=(conf,))
        p.start()
        LOG.info('Started queue processor PID %s', p.pid)
