#!/usr/bin/env python

from __future__ import absolute_import
import ConfigParser
import logging
import argparse
import yaml
from datetime import datetime, timedelta
import calendar
import pytz

from sqlalchemy import or_, and_
from models.base import *
from models.hosts import *
from models.analytics import *

from providers.l3.level3 import *
from providers.hw.highwinds import *

from common import utils

def parse_args():
    desc = 'Analytics gathering'
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('-c', '--config', dest='config', required=True,
                        help='Configuration file')
    parser.add_argument('-d', '--data_type', dest='data_type', required=True)
    parser.add_argument('--debug', dest='debug')
    return parser.parse_args()

def load_provs_from_config(prov_file, logger):
    try:
        f = open(prov_file, 'r')
    except IOError:
        logger.error('Unable to find providers %s', prov_file)
    raw_cfg = f.read()
    return yaml.load(raw_cfg)

def parse_config(config_file, logger):
    try:
        f = open(config_file, 'r')
    except IOError:
        logger.error('Unable to find %s', config_file)
    conf_dict = {}
    conf = ConfigParser.ConfigParser()
    conf.readfp(f)
    sections = ['DEFAULT', 'BACKEND']
    for section in sections:
        for k, v in conf.items(section):
            if k == 'providers_config':
                conf_dict['providers'] = load_provs_from_config(v, logger)
            conf_dict[k] = v
    return conf_dict

def generate_providers_cnames_list(hosts_list, conf, session):
    l3_cnames_list = []
    hw_host_hashs = []
    hostcname_map = {}
    for host in hosts_list:
        try:
            l3_cname = '%s.%s' % (host.cname, conf['application_domain'])
            l3_cnames_list.append(l3_cname)
            hw_host = session.query(HwHost).filter(HwHost.cname == host.cname).one()
            hw_host_hashs.append(hw_host.host_hash)
            hostcname_map[host.cname] = {
                'L3': l3_cname,
                'HW': hw_host.host_hash,
            }
        except:
            pass
    return l3_cnames_list, hw_host_hashs, hostcname_map

current_dt = datetime.utcnow()
delta_hour = timedelta(hours=-4)
previous_dt = current_dt + delta_hour
delta_hour = timedelta(hours=-0)
current_dt = current_dt + delta_hour

start_datetime = datetime(previous_dt.year, previous_dt.month, previous_dt.day, previous_dt.hour, 0, 0)
end_datetime = datetime(current_dt.year, current_dt.month, current_dt.day, current_dt.hour, 0, 0)

LOG = utils.logger_init(name=__name__, logpath='.')
args = parse_args()
conf = parse_config(args.config, LOG)
session = db_reconnect(conf['database_url'])
l3 = Level3(conf['providers']['l3']['username'], \
    conf['providers']['l3']['access_key'], \
    conf['providers']['l3']['api_url'], LOG)
hw = Highwinds(conf['providers']['hw'])
now = datetime.utcnow()
delta = timedelta(seconds=int(conf['consolidated_analytics_delay']))
hosts = session.query(Host).filter(or_(and_(Host.active == False, Host.status_updated > (now-delta)), Host.active)).all()
l3_cnames_list, hw_host_hashs, hostcname_map = generate_providers_cnames_list(hosts, conf, session)
scid = conf['providers']['l3']['SCID']

def consolidated():
    L3_stats = l3.get_all_P5TM_stats(l3_cnames_list, start_datetime, end_datetime, scid)
    HW_stats = hw.get_stats_host_hashs(hw_host_hashs, start_datetime, end_datetime)
    for host in hostcname_map.keys():
        for timestamp in HW_stats[hostcname_map[host]['HW']].keys():
            hw_hits = HW_stats[hostcname_map[host]['HW']][timestamp]['hits']
            hw_bytes = HW_stats[hostcname_map[host]['HW']][timestamp]['bytes']
            try:
                consolidated_usage = session.query(ConsolidatedUsage).\
                    filter(ConsolidatedUsage.hostcname == host).\
                    filter(ConsolidatedUsage.timestamp == timestamp).one()
            except:
                consolidated_usage = ConsolidatedUsage()
                consolidated_usage.timestamp = timestamp
                hh = session.query(Host).filter(Host.cname == host).one()
                consolidated_usage.accountHash = hh.accountHash
                consolidated_usage.hostcname = hh.cname
            consolidated_usage.hits = int(hw_hits)
            consolidated_usage.downloaded_bytes = int(hw_bytes)
            if L3_stats.get(hostcname_map[host]['L3']):
                if L3_stats[hostcname_map[host]['L3']].get(timestamp):
                    l3_hits = L3_stats[hostcname_map[host]['L3']][timestamp]['hits']
                    l3_bytes = L3_stats[hostcname_map[host]['L3']][timestamp]['bytes']
                    consolidated_usage.hits = int(hw_hits) + int(l3_hits)
                    consolidated_usage.downloaded_bytes = int(hw_bytes) + int(l3_bytes)
            session.add(consolidated_usage)
            session.commit()

def realtime():
    current_time = datetime.utcnow()
    rounded_date = datetime(current_time.year, current_time.month, \
    current_time.day, current_time.hour, current_time.minute-(current_time.minute %5), 0)
    timestamp = calendar.timegm(rounded_date.utctimetuple())
    l3_rt = l3. get_current_bandwidth(l3_cnames_list, scid)
    hw_rt = hw.get_current_bandwidth(hw_host_hashs)
    for host in hostcname_map.keys():
        try:
            rt = session.query(RealTimeUsage).\
                filter(RealTimeUsage.hostcname == host).\
                filter(RealTimeUsage.timestamp == timestamp).one()
        except:
            rt = RealTimeUsage()
            rt.timestamp = timestamp
            rt.hostcname = host
            hh = session.query(Host).filter(Host.cname == host).one()
            rt.accountHash = hh.accountHash
        rt.bandwidth = hw_rt[hostcname_map[host]['HW']]
        if l3_rt.get(hostcname_map[host]['L3']):
            rt.bandwidth += l3_rt[hostcname_map[host]['L3']]
        session.add(rt)
        session.commit()

if args.data_type == 'PT5M':
    consolidated()
if args.data_type == 'RT':
    realtime()
