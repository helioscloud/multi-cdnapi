'''
Auth middleware
'''
from webob import Response, Request
from webob import exc

import logging
import re

import memcache

from common import utils
from models.base import *
from models.accounts import *
from models.users import *

from api import json_body, generate_response

exc.WSGIHTTPException.json_body = json_body
exc.WSGIHTTPException.generate_response = generate_response


class AuthMiddleware(object):
    '''
    Auth middleware
    '''

    def __init__(self, app, conf, logger=None):
        if conf is None:
            conf = {}
        if logger is None:
            self.logger = logging.getLogger(__name__)
        else:
            self.logger = logger
        self.app = app
        self.conf = conf
        self.session = None
        self.encodings = {'application/json': 'json'}
        self.methods = ['POST', 'GET', 'PUT', 'DELETE', 'OPTIONS']

    def __call__(self, env, start_response):
        self.logger.info('Inside auth middleware')
        req = Request(env)
        if req.body:
            self.logger.debug("Received body: %s", str(req.body))
        if req.method not in self.methods:
            resp = exc.HTTPMethodNotAllowed()
            return resp(env, start_response)
        if req.method == 'POST' or req.method == 'PUT':
            if req.content_type.split(';')[0] not in self.encodings.keys():
                message = 'Content-type allowed : '
                for key in self.encodings:
                    message += ' ' + key
                resp = exc.HTTPUnsupportedMediaType(message)
                return resp(env, start_response)
        # For CORS swagger.
        if req.method == 'OPTIONS':
            resp = exc.HTTPOk()
            resp.headers['Access-Control-Allow-Origin'] = self.conf.get('allow_origin')
            resp.headers['Access-Control-Allow-Headers'] = 'Content-Type'
            resp.headers['Access-Control-Allow-Methods'] = 'GET, POST, DELETE, PUT, OPTIONS'
            return resp(env, start_response)
        # ByPASS AUTH
        '''
        self.logger.debug('Bypassing auth')
        user_id = 1
        self.session = db_reconnect(db_url=self.conf.get('database_url'),
                                    session=self.session)
        user, user_permissions = utils.load_user_permissions(self.session, user_id)
        env['user'] = user
        env['user_permissions'] = user_permissions
        resp = req.get_response(self.app)
        return resp(env, start_response)
        '''
        # End BY PASS AUTH
        # END swagger
        token_header = req.headers.get('Authorization')
        if token_header:
            token = re.match('^Bearer (\w+)$', token_header)
            if token:
                self.session = db_reconnect(db_url=self.conf.get('database_url'),
                                            session=self.session)
                user, user_permissions = self._load_user_permissions_from_token(token.group(1))
                if user:
                    env['user'] = user
                    env['user_permissions'] = user_permissions
                    self.logger.info('Request from user : %s', user.username)
                    resp = req.get_response(self.app)
                else:
                    self.logger.info('Wrong token provided : %s', token.group(1))
                    resp = req.get_response(exc.HTTPForbidden('Access forbidden'))
            else:
                self.logger.info('Wrong token format')
                resp = req.get_response(exc.HTTPForbidden('Wrong token format'))
        else:
            self.logger.info('No auth token provided')
            resp = req.get_response(exc.HTTPForbidden('No token provided'))
        return resp(env, start_response)

    def _load_user_permissions_from_token(self, token):
        '''
            Return the user and the user's permission if there is an entry in memcache
            with token as a key or None, None otherwise
        '''
        user, user_permissions = (None, None)
        memcached_servers = self.conf.get('memcached_servers').split(',')
        mc = memcache.Client(memcached_servers)
        token_info = mc.get(token)
        if token_info:
            user = token_info.get('user')
            user_permissions = token_info.get('user_permissions')
        return user, user_permissions

def auth_filter_factory(global_conf, **local_conf):
    '''
    Egg entry point
    '''
    conf = global_conf.copy()
    conf.update(local_conf)
    def auth_filter(app):
        return AuthMiddleware(app, conf)
    return auth_filter

# vim: ts=4 sw=4 sts=4 et
