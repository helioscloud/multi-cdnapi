'''
Backend code
'''
from __future__ import absolute_import
import logging
import datetime
import time
import json

from common import amqp
from common import utils
from providers.ec.edgecast import EdgecastRoute
from providers.l3.level3 import Level3
from providers.hw.highwinds import Highwinds
from models.hosts import CdxApp, Host, HwHost, Hostname, Scope, Origin, HwOrigin
from models.base import *

log = logging.getLogger('cdnbackend')

class BackendService(amqp.Subscriber):
    '''
    Base class for backend
    '''
    def __init__(self, config):
        self.db_url = config.get('database_url')
        self.session = db_reconnect(db_url=config.get('database_url'),
            session=None)
        self.ec = EdgecastRoute(config['providers']['ec']['account'],
            config['providers']['ec']['api_key'], './application_domain')
        self.l3 = Level3(config['providers']['l3']['username'], \
            config['providers']['l3']['access_key'], \
            config['providers']['l3']['api_url'], log)
        self.hw = Highwinds(config['providers']['hw'])
        self.config = config
        super(BackendService, self).__init__(config, logger=log)

    def process_event(self, body, message):
        '''
        Process message from received queue
        '''
        try:
            routing_key = message.delivery_info["routing_key"]
        except (AttributeError, KeyError):
            routing_key = "<unknown>"
        self.logger.debug("routing_key=%s", routing_key)
        self.session = db_reconnect(db_url=self.config.get('database_url'),
            session=None)
        #try:
        action = '_process_%s_%s' % (body.get('object_type'),
            body.get('action'))
        meth = getattr(self, action)
        resp = meth(body)
        #except:
            #self.logger.error('Wrong message %s' % (body))

    def _process_application_add(self, body):
        is_uniq = False
        while not is_uniq:
            cname = utils.generate_uniq_hashCode(type, 10, self.session)
            apps = self.session.query(CdxApp).filter(CdxApp.cname == cname).all()
            if len(apps) == 0:
                is_uniq = True
        self.ec.add_or_update_record(self.config['application_domain'], \
            'CNAME', cname, self.config['cdx_application_hw'], \
            self.config['cdx_ttl'])
        to_add = CdxApp()
        to_add.cname = cname
        to_add.status = 'UNUSED'
        self.session.add(to_add)
        self.session.commit()
        self.logger.info('New application %s added' % (cname))

    def _process_host_add(self, body):
        cname = body['cname']
        host = self.session.query(Host).filter(Host.cname == cname).one()
        origin = self.session.query(Origin).filter(Origin.hostcname == cname).one()
        hostnames = host.get_hostnames(self.session)
        hostnames.insert(0, cname + '.' + self.config['application_domain'])
        scopes = self.session.query(Scope).filter(Scope.hostcname == cname).all()
        #HW creation
        self._hw_origin_add(origin.id)
        hw_hosts = self.session.query(HwHost).filter(HwHost.status == 'UNUSED').all()
        if not len(hw_hosts) > 0:
            self._hw_host_add()
            hw_hosts = self.session.query(HwHost).filter(HwHost.status == 'UNUSED').all()
        hw_host = hw_hosts[0]
        hw_host.status = 'ACTIVATED'
        hw_host.cname = cname
        self.session.add(hw_host)
        self.session.commit()
        self._hw_scopes_add_or_update(scopes[0])
        # L3 creation
        conf = {"originserver" : {
                                    "host": origin.hostname,
                                    "hostip": "10.10.10.1",
                                    "port": origin.port,
                                    "protocol": origin.protocol
                                },
                "aliases": hostnames
        }
        status, res = self.l3._post('/serviceConfiguration/v1.0/246768/'+self.config['providers']['l3']['SCID']+'/', conf)
        if status != 201:
            self.logger.error('L3 : creation of %s failed, status: %s, res: %s' % (cname, status, res))
        else:
            self.logger.info('L3 : creation of %s ' % (cname))
        for scope in scopes:
            self._l3_scope_add(scope)
        self.ec.add_or_update_record(self.config['application_domain'], \
            'CNAME', cname, self.config['cdx_application_multi'], \
            self.config['cdx_ttl'])
        self.logger.info('DNS switched to the multi cdx app for %s' % (cname))
        self.logger.info('Creation of %s finished' % (cname))

    def _process_origin_update(self, body):
        try:
            origin = self.session.query(Origin).filter(Origin.id == body['id']).one()
        except:
            self.logger.error('Unable to find the origin. Body received :' % (body))
            return
        self._hw_origin_update(origin)
        self._l3_origin_update(origin)

    def _process_scope_update(self, body):
        try:
            scope = self.session.query(Scope).filter(Scope.id == body['id']).one()
        except:
            self.logger.error('Unable to find the scope. Body received :' % (body))
            return
        self._hw_scopes_add_or_update(scope)
        self._l3_scope_add_or_update(scope, 'update')

    def _process_scope_add(self, body):
        try:
            scope = self.session.query(Scope).filter(Scope.id == body['id']).one()
        except:
            self.logger.error('Unable to find the scope to add. Body : %s' % (body))
            return
        self._hw_scopes_add_or_update(scope)
        self._l3_scope_add_or_update(scope, 'add')
        self.logger.info('Scope %s for cname %s added.' % (scope.id, scope.hostcname))

    def _process_scope_delete(self, body):
        try:
            scope = self.session.query(Scope).filter(Scope.hostcname ==  body['cname'])
        except:
            self.logger.error('Unable to find any scope associated with the host. Body: %s' % (body))
            return
        self._l3_scope_delete(body['id'], body['cname'])
        self._hw_scopes_add_or_update(scope[0])

    def _process_hostname_add(self, body):
        try:
            hostname = self.session.query(Hostname).filter(Hostname.id == body['id']).one()
            scope = self.session.query(Scope).filter(Scope.hostcname == hostname.hostcname)
        except:
            self.logger.error('Unable to find any hostname. Body: %s' % (body))
            return
        self._l3_hostname_add(hostname)
        self._hw_scopes_add_or_update(scope[0])

    def _process_hostname_delete(self, body):
        try:
            scope = self.session.query(Scope).filter(Scope.hostcname == body['cname'])
        except:
            self.logger.error('Unable to find any hostname. Body: %s' % (body))
            return
        self._l3_hostname_delete(body['hostname'], body['cname'])
        self._hw_scopes_add_or_update(scope[0])

    def _l3_process_post_put(self, url, body, method):
        status = 404
        while status == 404 or status == 500:
            if method == 'add':
                status, res = self.l3._post(url, body)
            else:
                if method == 'update':
                    status, res = self.l3._put(url, body)
                else:
                    return False
            if status == 404 or status == 500:
                time.sleep(60)
        if method == 'add' and status != 201:
            self.logger.error('L3 : creation failed. Url: %s, Body: %s, \
                Status: %s, L3message: %s' % (url, body, status, res))
            return False
        if method == 'update' and status != 204:
            self.logger.error('L3 : Update failed. Url: %s, Body: %s, \
                Status: %s, L3message: %s' % (url, body, status, res))
            return False
        return True

    def _l3_origin_update(self, origin):
        conf = {"originserver" : {
                                    "host": origin.hostname,
                                    "hostip": "10.10.10.1",
                                    "port": origin.port,
                                    "protocol": origin.protocol
                                }
        }
        url = '/serviceConfiguration/v1.0/246768/%s/%s.%s/Origin' % (self.config['providers']['l3']['SCID'], origin.hostcname, self.config['application_domain'])
        status, res = self.l3._put(url, conf)
        if status == 204:
            self.logger.info('L3: Origin %s updated.' % (origin.hostcname))

    def _l3_scope_add_or_update(self, scope, method):
        body = {
            "rgdef": [scope.path],
            "rgtype": "path",
            "rgid": str(scope.id)
        }
        url = '/serviceConfiguration/v1.0/246768/%s/%s/ResourceGroups' % \
            (self.config['providers']['l3']['SCID'],\
                scope.hostcname + '.' + self.config['application_domain'])
        if method == 'update':
            url += '/%s' % (scope.id)
        resp = self._l3_process_post_put(url, body, method)
        if not resp:
            return False
        body = {
            "CacheControl": {
                "cchomode": {
                    "int": "%ds" % (scope.cacheControlModeInt),# required
                    "ext": "%ds" % (scope.cacheControlModeExt)# optional
                }
            }
        }
        if scope.cacheControlModeForce:
            body['CacheControl']['cchomode']['force'] = 'yes'
        url = '/serviceConfiguration/v1.0/246768/%s/%s/ResourceGroups/%d/ConfGroups/CacheControl' % \
            (self.config['providers']['l3']['SCID'], \
                scope.hostcname + '.' + self.config['application_domain'], \
                scope.id)
        resp = self._l3_process_post_put(url, body, method)
        if not resp:
            return False
        if scope.queryStringModeOn:
            action = 'honor'
        else:
            action = 'ignore'
        body = {
                "ContentManipulation": {
                    "qshmode": {
                        "action": action,
                        "type": "string"
                    }
                }
        }
        url = '/serviceConfiguration/v1.0/246768/%s/%s/ResourceGroups/%d/ConfGroups/ContentManipulation' % \
            (self.config['providers']['l3']['SCID'], \
                scope.hostcname + '.' + self.config['application_domain'], \
                scope.id)
        resp = self._l3_process_post_put(url, body, method)
        if not resp:
            return False
        else:
            self.logger.info('L3: Scope %s for cname %s added' % (scope.id, scope.hostcname))
        return True

    def _l3_scope_delete(self, rgid, cname):
        url = '/serviceConfiguration/v1.0/246768/%s/%s.%s/ResourceGroups/%s' % (self.config['providers']['l3']['SCID'], cname, self.config['application_domain'], rgid)
        status, res = self.l3._delete(url)
        if status == 200:
            self.logger.info('L3: Scope %s for cname %s deleted.' % (rgid, cname))
        else:
            self.logger.error('L3: Fail to delete scope %s for cname %s deleted. Status: %s body: %s' % (rgid, cname, status, res))

    def _l3_hostname_add(self, hostname):
        url = '/serviceConfiguration/v1.0/246768/%s/%s.%s/Aliases/%s' % (self.config['providers']['l3']['SCID'], hostname.hostcname, self.config['application_domain'], hostname.hostname)
        status, res = self.l3._post(url, None)
        if status == 201:
            self.logger.info('L3: Hostname %s for cname %s added.' % (hostname.hostname, hostname.hostcname))
        else:
            self.logger.error('L3: Fail to add hostname %s for cname %s. Status: %s body: %s' % (hostname.hostname, hostname.hostcname, status, res))

    def _l3_hostname_delete(self, hostname, hostcname):
        url = '/serviceConfiguration/v1.0/246768/%s/%s.%s/Aliases/%s' % (self.config['providers']['l3']['SCID'], hostcname, self.config['application_domain'], hostname)
        status, res = self.l3._delete(url)
        if status == 200:
            self.logger.info('L3: Hostname %s for cname %s deleted.' % (hostname, hostcname))
        else:
            self.logger.error('L3: Fail to delete hostname %s for cname %s. Status: %s body: %s' % (hostname, hostcname, status, res))

    def __hw_generate_scope_payload(self, scope):
        origin = self.session.query(Origin).filter(Origin.hostcname == scope.hostcname).one()
        hw_origin = self.session.query(HwOrigin).filter(HwOrigin.ocdn_id == origin.id).one()
        hostnames = self.session.query(Hostname).filter(Hostname.hostcname == scope.hostcname).all()
        hw_payload = {
            'originPullHost': {'primary': hw_origin.hw_id},
            'hostname': [{'domain': '%s.%s' % (origin.hostcname, self.config['application_domain'])}],
            'dynamicContent': [],
            'cacheControl': []
        }
        for hostname in hostnames:
            hw_payload['hostname'].append({'domain': hostname.hostname})
        scopes = self.session.query(Scope).filter(Scope.hostcname == scope.hostcname)
        for scope in scopes:
            if scope.path == '/':
                if scope.queryStringModeOn:
                    hw_payload['dynamicContent'].append({'queryParams': '*'})
                    hw_payload['cacheControl'].append({'maxAge': scope.cacheControlModeInt})
            else:
                if scope.queryStringModeOn:
                    hw_payload['dynamicContent'].append({'queryParams': '*', 'pathFilter': scope.path})
                    hw_payload['cacheControl'].append({'maxAge': scope.cacheControlModeInt, 'pathFilter': scope.path})
        return hw_payload

    def _hw_scopes_add_or_update(self, scope):
        hw_payload = self.__hw_generate_scope_payload(scope)
        hwhost = self.session.query(HwHost).filter(HwHost.cname == scope.hostcname).one()
        res, body = self.hw.put_scope(hw_payload, hwhost.host_hash, hwhost.scope_id)
        if res:
            self.logger.info('HW: Scope %s for cname %s updated.' % (scope.id, scope.hostcname))
        else:
            self.logger.error('HW: Failed to update scope %s. Payload: %s, Res: %s, body: %s' % (scope.id, hw_payload, res, body))

    def __hw_generate_origin(self, origin):
        hw_payload = {
            'name': 'origin_%s' % (origin.hostcname),
            'type': 'EXTERNAL'
        }
        for attribut in ['hostname', 'port', 'protocol', 'path']:
            hw_payload[attribut] = getattr(origin, attribut)
        return hw_payload

    def _hw_origin_add(self, origin_id):
        origin = self.session.query(Origin).filter(Origin.id == origin_id).one()
        hw_payload = self.__hw_generate_origin(origin)
        resp, hw_origin_id = self.hw.post_origin(hw_payload)
        if resp:
            hw_origin = HwOrigin()
            hw_origin.hw_id =  hw_origin_id
            hw_origin.ocdn_id = origin.id
            self.session.add(hw_origin)
            self.session.commit()
            self.logger.info('HW: origin creation for %s.' %(origin.hostcname))
            return True
        else:
            self.logger.error('HW: origin creation failed for %s. Error: %s' % \
                (origin.hostcname, hw_origin_id))
            return False

    def _hw_origin_update(self, origin):
        hworigin = self.session.query(HwOrigin).filter(HwOrigin.ocdn_id == origin.id).one()
        hw_payload = self.__hw_generate_origin(origin)
        resp, hw_origin_id = self.hw.put_origin(hw_payload, hworigin.hw_id)
        if resp:
            self.logger.info('HW: origin %s updated' % (origin.hostcname))
        else:
            self.logger.error('HW: Fail to update origin for %s. Payload: %s' % (origin.hostcname, hw_payload))

    def _hw_host_add(self):
        hw_payload = {
            'name': 'FreeHost',
            'services' : [40]
        }
        res, body = self.hw.post_host(hw_payload)
        if res:
            host = HwHost()
            host.host_hash = body['hashCode']
            host.scope_id = body['scopes'][0]['id']
            host.status = 'UNUSED'
            self.session.add(host)
            self.session.commit()
            self.logger.info('HW: free host added: %s' % (host.host_hash))
        else:
            self.logger.error('HW: free host creation failed: %s' % (body))
