#!/usr/bin/env python
import requests
import urlparse
import httplib
import json
from datetime import datetime, timedelta
import calendar
import hmac
import hashlib
import base64
from collections import OrderedDict


class Level3:

    def __init__(self, username, access_key, api_url, log, content_type='json'):
        '''
        Parameters:
        username:   key_id
        access_key: secret
        api_url:    url
        '''
        self.hostname = 'ws.level3.com'
        self.port = '443'
        self.username = username
        self.access_key = access_key
        self.api_url = api_url
        self.current_date = None
        if content_type == 'json':
            self.headers = {'Content-Type': 'application/json',
                'Accept': 'application/json'}
            self.content_type = 'application/json'
        if content_type == 'xml':
            self.headers = {'Content-Type': 'text/xml',
                'Accept': 'text/xml'}
            self.content_type =  'text/xml'
        self.agrp = '246768'

    @property
    def formatted_date(self):
        '''format current date'''
        return self.current_date.strftime("%a, %d %b %Y %H:%M:%S +0000")

    def _refresh_date(self):
        '''Refresh current date'''
        self.current_date = datetime.utcnow()

    def _gen_auth_header(self, method, urlpath, content_md5=None):
        '''Generate auth header for request'''
        urlpath = urlpath.split('?')[0]
        self._refresh_date()
        authstring = "%s\n%s\n%s\n%s\n" % (
            self.formatted_date,
            urlpath,
            self.content_type,
            method,
        )
        if content_md5:
            authstring = "%s%s" %(authstring, content_md5)
        authhash = hmac.new(self.access_key, authstring, hashlib.sha1).digest()
        return "MPA %s:%s" % (self.username, base64.b64encode(authhash))

    def __pregenerate_headers(self, method, urlpath, content_md5=None):
        '''Pregenerate auth headers'''

        self.headers['Authorization'] = self._gen_auth_header(method, urlpath,
                                                              content_md5=content_md5)
        self.headers['Date'] = self.formatted_date
        if content_md5:
            self.headers['Content-MD5'] = content_md5
        else:
            try:
                self.headers.pop('Content-MD5')
            except KeyError:
                pass

    def _get(self,urlpath):
        self.__pregenerate_headers('GET', urlpath)
        uri = self.api_url + urlpath
        http_conn = httplib.HTTPSConnection(self.hostname, self.port)
        http_conn.request('GET', uri, body=None, headers=self.headers)
        resp = http_conn.getresponse()
        res = json.loads(resp.read())
        return resp.status,res

    def _post(self, urlpath, data):
        self.__pregenerate_headers('POST', urlpath)
        uri = self.api_url + urlpath
        http_conn = httplib.HTTPSConnection(self.hostname, self.port)
        http_conn.request('POST', uri, body=json.dumps(data),headers=self.headers)
        resp = http_conn.getresponse()
        res = json.loads(resp.read())
        return resp.status,res

    def _put(self, urlpath, data):
        self.__pregenerate_headers('PUT', urlpath)
        uri = self.api_url + urlpath
        http_conn = httplib.HTTPSConnection(self.hostname, self.port)
        http_conn.request('PUT', uri, body=json.dumps(data),headers=self.headers)
        resp = http_conn.getresponse()
        try:
             res = json.loads(resp.read())
        except:
            res = resp.read()
        return resp.status, res

    def _delete(self, urlpath):
        self.__pregenerate_headers('DELETE', urlpath)
        uri = self.api_url + urlpath
        http_conn = httplib.HTTPSConnection(self.hostname, self.port)
        http_conn.request('DELETE', uri, body=None, headers=self.headers)
        resp = http_conn.getresponse()
        try:
            res = json.loads(resp.read())
        except:
            res = resp.read()
        return resp.status,res

    def get_PT5M_stats(self, cname, start_datetime, end_datetime, scid):
        '''
        Collect Bytes, hits and bandwidth for a cname with 5min datapoints
        '''
        def _datapoint(data, res):
            for datapoint in data['data']['point']:
                datepoint = datetime.strptime(datapoint['id'], '%m/%d/%Y %H:%M:%S')
                res[calendar.timegm(datepoint.utctimetuple())] = {
                    'bytes': int(float(datapoint['item']['volume']) * 1000000000),
                    'hits' : datapoint['item']['requests']
                }
            return res
        url = "/usage/cdn/v1.0/246768/%s/%s?serviceType=CACHING&dateFrom=%s&dateTo=%s&dataInterval=5min" % \
            (scid, cname, start_datetime.strftime('%Y%m%d%H%M'), end_datetime.strftime('%Y%m%d%H%M'))
        delta = timedelta(days = 1)
        local_start_date = start_datetime
        local_end_date = local_start_date + delta
        resp = OrderedDict({})
        while local_end_date <= end_datetime:
            url = "/usage/cdn/v1.0/246768/%s/%s?serviceType=CACHING&dateFrom=%s&dateTo=%s&dataInterval=5min" % \
                (scid, cname, local_start_date.strftime('%Y%m%d%H%M'), local_end_date.strftime('%Y%m%d%H%M'))
            status, res = self._get(url)
            if status == 200:
                resp = _datapoint(res, resp)
            local_start_date = local_end_date + timedelta(seconds = 300)
            local_end_date = local_end_date + delta
        if not local_start_date >= end_datetime:
            url = "/usage/cdn/v1.0/246768/%s/%s?serviceType=CACHING&dateFrom=%s&dateTo=%s&dataInterval=5min" % \
                (scid, cname, local_start_date.strftime('%Y%m%d%H%M'), end_datetime.strftime('%Y%m%d%H%M'))
            status, res = self._get(url)
            if status == 200:
                resp = _datapoint(res, resp)
            return resp

    def get_all_P5TM_stats(self, cnames, start_datetime, end_datetime, scid):
        '''
        Collect Bytes, hits and bandwidth for a list of cnames with 5min datapoints
        '''
        resp = {}
        for cname in cnames:
            res = self.get_PT5M_stats(cname, start_datetime, end_datetime, scid)
            if len(res.keys()) > 0:
                resp[cname] = res
        return resp

    def get_current_bandwidth(self, cnames, scid):
        '''Collect the current bandwidth usage for the cnames provided as argument'''
        bw_cnames = {}
        urlpath = ('/rtm/cdn/v1.0/%s/%s?serviceType=CACHING&geo=none&property=true') % (self.agrp, scid)
        status, result = self._get(urlpath)
        if status == 200:
            for ni in result['accessGroup']['services'][0]['networkIdentifiers']:
                if ni['id'] in cnames:
                    bw_cnames[ni['id']] = int(float(ni['mbps']) * 1000000)
        return bw_cnames
