import requests
import json
import copy
import datetime

class EdgecastRoute:

    def __init__(self, username, access_key, save_path):
        self.username = username
        self.access_key = access_key
        self.host = 'api.edgecast.com'
        self.save_path = save_path

    def _get_headers(self):
        headers_auth =  {    'Authorization' : "TOK:%s" % self.access_key,
                            'Accept' : 'application/json',
                            'Content-type' : 'application/json'
                        }
        return headers_auth

    def _save_zone(self, zone_name, data):
        timestamp = datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S:00Z')
        save_file = '%s/%s_%s' % (self.save_path, zone_name, timestamp)
        f = open(save_file, 'w')
        f.write(data)
        f.close

    def get_all_zones(self):
        req_url = "https://%s/v2/mcc/customers/%s/dns/routezones" % (self.host, self.username)
        resp = requests.get(req_url, headers = self._get_headers())
        return resp.json()

    def get_zone(self, zone_name):
        req_url = "https://%s/v2/mcc/customers/%s/dns/routezone?name=%s" % (self.host, self.username, zone_name)
        resp = requests.get(req_url, headers = self._get_headers())
        return resp.json()

    def add_or_update_record(self, zone_name, record_type, record_name, record_data, record_ttl):
        req_url = "https://%s/v2/mcc/customers/%s/dns/routezone" % (self.host, self.username)
        current_conf = self.get_zone(zone_name)
        body = self.get_zone(zone_name)
        self._save_zone(zone_name, json.dumps(body))
        if body['Records'] == None:
            body['Records'] = {record_type:  [{'TTL': record_ttl, 'Name': record_name, 'Rdata': record_data}] }
        else:
            if body['Records'].get(record_type):
                to_add = True
                for record in  body['Records'][record_type]:
                    if record['Name'] == record_name:
                        record['Rdata'] = record_data
                        record['TTL'] = record_ttl
                        to_add = False
                if to_add:
                    body['Records'][record_type].append({'TTL': record_ttl, 'Name': record_name, 'Rdata': record_data})
            else:
                body['Records'][record_type] = [{'TTL': record_ttl, 'Name': record_name, 'Rdata': record_data}]
        body = json.dumps(body)
        resp = requests.put(req_url, body, headers = self._get_headers())
        return resp.json()

    def del_record(self, zone_name, record_type, record_name):
        req_url = "https://%s/v2/mcc/customers/%s/dns/routezone" % (self.host, self.username)
        current_conf = self.get_zone(zone_name)
        body = self.get_zone(zone_name)
        self._save_zone(zone_name, json.dumps(body))
        if body['Records'] == None:
            return 'No such record'
        if not body['Records'].get(record_type):
            return 'No such record'
        body = self.get_zone(zone_name)
        newbody = copy.deepcopy(body)
        newbody['Records'][record_type] = []
        for record in body['Records'][record_type]:
            if record['Name'] != record_name:
                newbody['Records'][record_type].append(record)
        newbody = json.dumps(newbody)
        return resp.json()
