from datetime import datetime, timedelta
from urllib import urlencode
import requests
import json
from collections import OrderedDict

class Highwinds:

    def __init__(self, conf):
        self.api_endpoint = conf['api_endpoint']
        self.account = conf['account']
        self.username = conf['username']
        self.password = conf['password']
        self.hash_list = {}
        self.token = conf['token']

    def _get_headers(self):
        headers = {
                    'Accept' : 'application/json',
                    'Content-type' : 'application/json',
                    'Authorization': 'Bearer %s' % str(self._valid_token())
                  }
        return headers

    def _get_token(self):
        ''' Get a token '''
        auth = requests.post(
        self.api_endpoint + "/auth/token",
        data={
          "grant_type": "password",
          "username": self.username,
          "password": self.password,
        }, headers={
          "Accept": "application/json"
        })
        self.token = auth.json()
        self.token['timestamp'] = datetime.utcnow()

    def _valid_token(self):
        ''' Return the current token or a new one if the current token has expired '''
        date_expired =  datetime.utcnow()
        if self.token:
            if self.token.get('access_token') and self.token['timestamp'] + timedelta(seconds=self.token['expires_in']) < datetime.utcnow():
                return self.token.get('access_token');
        else:
            self._get_token()
        return self.token.get('access_token');

    def post_origin(self, origin):
        '''
        Create a new origin
        Success : return True and the id of the created origin
        Failed : return False and the error message
        '''
        url = "%s/api/v1/accounts/%s/origins" % (self.api_endpoint, self.account)
        r = requests.post(url, json.dumps(origin), headers=self._get_headers())
        if r.status_code == 201:
            return True, r.json()['id']
        else:
            return False, r.json()['error']

    def put_origin(self, origin, origin_id):
        '''
        Update an origin
        Success : return True and the origin
        Failed : return False and the error message
        '''
        url = "%s/api/v1/accounts/%s/origins/%s" % (self.api_endpoint, self.account, origin_id)
        r = requests.put(url, json.dumps(origin), headers=self._get_headers())
        if r.status_code == 200:
            return True, r.json()
        else:
            return False, r.json()['error']

    def del_origin(self, origin_id):
        '''
        Delete an origin
        '''
        url = "%s/api/v1/accounts/%s/origins/%s" % (self.api_endpoint, self.account, origin_id)
        r = requests.delete(url, headers=self._get_headers())
        if r.status_code == 200:
            return True, ''
        else:
            return False, r.json()['error']

    def post_host(self, host):
        '''
        Create a new host
        Success : return True and the json description of the host
        Failed : return False and the error message
        '''
        url = "%s/api/v1/accounts/%s/hosts" % (self.api_endpoint, self.account)
        r = requests.post(url, json.dumps(host), headers=self._get_headers())
        if r.status_code == 201:
            return True, r.json()
        else:
            return False, r.json()['error']

    def put_host(self, host, hashCode):
        '''
        Update an host
        Success : return True and the the json description of the host
        Failed : return False and the error message
        '''
        url = "%s/api/v1/accounts/%s/hosts/%s" % (self.api_endpoint, self.account, hashCode)
        r = requests.put(url, json.dumps(host), headers=self._get_headers())
        if r.status_code == 200:
            return True, r.json()
        else:
            return False, r.json()['error']

    def del_host(self, hashCode):
        '''
        Delete an host
        '''
        url = "%s/api/v1/accounts/%s/hosts/%s" % (self.api_endpoint, self.account, hashCode)
        r = requests.delete(url, headers=self._get_headers())
        if r.status_code == 200:
            return True, ''
        else:
            return False, r.json()['error']

    def post_scope(self, scope, hashCode):
        '''
        Create a new scope associated with the host hashCode
        Success : return True and the json description of the scope
        Failed : return False and the error message
        '''
        url = "%s/api/v1/accounts/%s/hosts/%s/configuration/scopes" % \
            (self.api_endpoint, self.account, hashCode)
        r = requests.post(url, json.dumps(scope), headers=self._get_headers())
        if r.status_code == 200:
            return True, r.json()
        else:
            return False, r.json()['error']

    def put_scope(self, scope, hashCode, scope_id):
        '''
        Update an host
        Success : return True and the id of request, the scope and the new policy pushed
        Failed : return False and the error message
        '''
        url = "%s/api/v1/accounts/%s/hosts/%s/configuration/%s" % \
            (self.api_endpoint, self.account, hashCode, scope_id)
        r = requests.put(url, json.dumps(scope), headers=self._get_headers())
        if r.status_code == 200:
            return True, r.json()
        else:
            return False, r.json()['error']

    def del_scope(self, hashCode, scope_id):
        '''
        Delete an scope
        '''
        url = "%s/api/v1/accounts/%s/hosts/%s/configuration/%s" % \
            (self.api_endpoint, self.account, hashCode, scope_id)
        r = requests.delete(url, headers=self._get_headers())
        if r.status_code == 200:
            return True, ''
        else:
            return False, r.json()['error']

    def get_scope(self, hashCode, scope_id):
        '''
        Get a scope
        '''
        url = "%s/api/v1/accounts/%s/hosts/%s/configuration/%s" % \
            (self.api_endpoint, self.account, hashCode, scope_id)
        r = requests.get(url, headers=self._get_headers())
        if r.status_code == 200:
            return True, r.json()
        else:
            return False, r.json()['error']

    def get_stats_host_hashs (self, host_hashs, start_datetime, end_datetime):
        '''Collecte bytes and hits count for the selected host_hashs list and between start_datetime and end_datetime
        Return a dict, key are cnames and values are dict with bytes and hits per timestamp'''
        stats_host_hashs = {}
        stats = requests.get(
        self.api_endpoint + ':443/api/v1/accounts/%s/analytics/transfer?startDate=%s&endDate=%s&granularity=PT5M&platforms=3&groupBy=HOST' % (self.account, start_datetime.strftime('%Y-%m-%dT%H:00:00Z'), end_datetime.strftime('%Y-%m-%dT%H:00:00Z')),
        headers=self._get_headers()
        )
        for series in stats.json()['series']:
            host_hash = series['key']
            if host_hash in host_hashs:
                stats_host_hashs[host_hash] = OrderedDict({})
                timestamp_key = series['metrics'].index('usageTime')
                xfer_key = series['metrics'].index('xferUsedTotalMB')
                requests_key = series['metrics'].index('requestsCountTotal')
                for row in series['data']:
                    timestamp = int(row[timestamp_key]/1000)
                    stats_host_hashs[host_hash][timestamp] = {
                        'bytes': int(row[xfer_key])*1000000,
                        'hits': row[requests_key]
                    }
        return stats_host_hashs

    def get_current_bandwidth(self, host_hashs):
        '''Collect the current bandwidth usage for the host_hashs provided as argument'''
        #There is no realtime stats at HW so we collect the last available point
        bw_host_hashs = {}
        end_datetime = datetime.utcnow()
        delta_second = timedelta(seconds=-600)
        start_datetime = end_datetime + delta_second
        url = '%s:443/api/v1/accounts/%s/analytics/transfer?startDate=%s&endDate=%s&granularity=PT5M&platforms=3&groupBy=HOST' \
            % (self.api_endpoint, self.account, start_datetime.strftime('%Y-%m-%dT%H:%M:00Z'), end_datetime.strftime('%Y-%m-%dT%H:%M:00Z'))
        stats = requests.get(url, headers=self._get_headers())
        for series in stats.json()['series']:
            host_hash = series['key']
            if host_hash in host_hashs:
                bw_key = series['metrics'].index('xferRateMeanMbps')
                try:
                    mbps = float(series['data'][0][bw_key])
                except:
                    mbps = 0
                bdw = int(mbps*1000000)
                bw_host_hashs[host_hash] = bdw
        return bw_host_hashs
