'''
Part of the API dedicated to Oauth2
'''

from webob import Response, Request
import webob.dec
from webob import exc
from paste import httpserver
from paste.deploy import loadapp

import logging
import re
import json
import memcache
import string
import random
import hashlib

from models.base import *
from models.accounts import *
from models.users import *
from sqlalchemy.orm import aliased

'''
Monkey-patch webob.exc to return json
'''

from webob.compat import (
    class_types,
    text_type,
    urlparse,
    )
from string import Template
from webob.exc import no_escape


json_template_obj = Template('''\
{
  "status": "${status}",
  "message": "${body}"
}''')


def json_body(self, environ):
    body = self._make_body(environ, no_escape)
    body = TAG_RE.sub('', body.strip())
    body = body.replace('\n','')
    return json_template_obj.substitute(status=self.status,
                                        body=body)
def generate_response(self, environ, start_response):
    if self.content_length is not None:
        del self.content_length
    headerlist = list(self.headerlist)
    accept = environ.get('HTTP_ACCEPT', '')
    if accept and 'html' in accept or '*/*' in accept:
        content_type = 'text/html'
        body = self.html_body(environ)
    elif accept and 'json' in accept:
        content_type = 'application/json'
        body = self.json_body(environ)
    else:
        content_type = 'text/plain'
        body = self.plain_body(environ)
    extra_kw = {}
    if isinstance(body, text_type):
        extra_kw.update(charset='utf-8')
    resp = Response(body,
                    status=self.status,
                    headerlist=headerlist,
                    content_type=content_type,
                    **extra_kw
                   )
    resp.content_type = content_type
    return resp(environ, start_response)

exc.WSGIHTTPException.json_body = json_body
exc.WSGIHTTPException.generate_response = generate_response

class OauthApp(object):
    '''
    OauthApp application
    '''
    def __init__(self, conf, logger=None):
        if conf is None:
            conf = {}
        if logger is None:
            self.logger = logging.getLogger(__name__)
        else:
            self.logger = logger
        self.conf = conf
        self.session = None

    def __call__(self, env, start_response):
        req = Request(env)
        self.logger.debug('Starting processing call')
        # For CORS swagger.
        if req.method == 'OPTIONS':
            resp = exc.HTTPOk()
            resp.headers['Access-Control-Allow-Origin'] = \
                self.conf.get('allow_origin')
            resp.headers['Access-Control-Allow-Headers'] = 'Content-Type'
            resp.headers['Access-Control-Allow-Methods'] = \
                'GET, POST, DELETE, PUT, OPTIONS'
            return resp(env, start_response)
        if req.method == 'POST' and req.path_info == '/token':
            try :
                resp = self.do_post_token(req)
            except exc.HTTPException, e:
                resp = e
        else:
            resp = exc.HTTPNotImplemented()
        self._add_headers(resp)
        return resp(env, start_response)

    def do_post_token(self, req):
        data = self._load_json(req.body)
        if data.get('grant_type') == 'password':
            user, user_permissions = self._check_credentials(data.get('username'), \
                data.get('password'))
            if user != None:
                token_info = self._create_uniq_token(user, user_permissions)
                resp = exc.HTTPOk()
                resp.body = json.dumps(token_info)
                return resp
            else:
                return exc.HTTPForbidden('Wrong credentials.')
        else:
            if data.get('grant_type') == 'refresh_token':
                return exc.HTTPNotImplemented('Not yet implemented.')
            else:
                raise exc.HTTPBadRequest('Wrong grant_type.')
        return exc.HTTPBadRequest('Wrong grant_type.')

    def _check_credentials(self, username, password):
        '''
            Check against the DB if the credentials are good, Return a tuple
            with the user object and the user permission
            or None in case of wrong credentials
        '''
        self.session = db_reconnect(db_url=self.conf.get('database_url'),
                                                session=self.session)
        try:
            user = self.session.query(User).filter(User.username == username, \
                User.password == hashlib.sha256(password).hexdigest()).one()
            account = self.session.query(Account).\
                filter(Account.hashCode == user.accountHash).\
                filter(Account.accountStatus == 'ACTIVATED').one()
            role_account_alias = aliased(RoleList)
            role_subaccount_alias = aliased(RoleList)
            role_matrix, role_account, role_subaccount = self.session.query(RoleMatrix, \
                role_account_alias, role_subaccount_alias)\
                .join(role_account_alias, RoleMatrix.userAccount == role_account_alias.id)\
                .join(role_subaccount_alias, RoleMatrix.subAccounts == role_subaccount_alias.id)\
                .filter(RoleMatrix.id == user.roles).one()
            user_permissions = {
                            'account' : role_account,
                            'sub_accounts' : role_subaccount
            }
        except:
            user = None
            user_permissions = None
        return user, user_permissions

    def _refresh_token(self, refresh_token):
        ''' Not yet implemented '''
        return True

    def _create_uniq_token(self, user, user_permissions):
        '''
        Create a uniq token_id. The token_id is used as a key in memcache.
        Data stored are the user, the user's permissions and a refresh_token
        Return a dict with all token info
        '''
        token = self._generate_token()
        memcached_servers = self.conf.get('memcached_servers').split(',')
        token_default_duration = self.conf.get('token_default_duration')
        mc = memcache.Client(memcached_servers)
        while mc.get(token):
            token = self._generate_token()
        refresh_token = self._generate_token()
        to_store = {    'user' : user,
                        'refresh_token' : refresh_token,
                        'user_permissions' : user_permissions
                    }
        mc.set(token, to_store, time=int(token_default_duration))
        token_info = {
                'access_token' : token,
                'token_type' : 'bearer',
                'expires_in' : token_default_duration,
                'refresh_token' : refresh_token
                }
        return token_info

    def _generate_token(self):
        '''
        Return a 128 long token with random hexa digits
        '''
        chars = string.hexdigits.lower()
        token = ''.join(random.choice(chars) for _ in range(64))
        return token

    def _add_headers(self, resp):
        '''
        Add headers : json and CORS for swagger.
        * will be replaced by the domain where swagger will be installed
        '''
        if self.conf.get('allow_origin'):
            resp.headers['Access-Control-Allow-Origin'] = "%s" % \
                self.conf.get('allow_origin')
        else:
            resp.headers['Access-Control-Allow-Origin'] = '*'
        resp.headers['Access-Control-Allow-Headers'] = '*'
        resp.headers['Access-Control-Allow-Methods'] = \
            'GET, POST, DELETE, PUT, OPTIONS'
        resp.content_type = 'application/json'

    def _load_json(self, data):
        try:
            jdata = json.loads(data)
        except:
            raise exc.HTTPBadRequest('Wrong format')
        return jdata

def oauth_factory(global_conf, **local_conf):
    '''
    Oauth wsgi app entry point
    '''
    conf = global_conf.copy()
    conf.update(local_conf)
    app = OauthApp(conf)
    return app
