'''
User's permissions
'''

from models.base import *
from models.accounts import Account

def check_user_permissions(user, user_permissions, account_hashCode, Category, permission, session):
    '''
    Check if the user has the permission to do an action on an account
    '''
    def check(a, b):
        if a == 'READ' and b == 'EDIT':
            return True
        if a == b:
            return True
        return False
    if user.accountHash == account_hashCode and check(permission, getattr(user_permissions['account'], Category)):
        return True
    if check(permission, getattr(user_permissions['sub_accounts'], Category)):
        try:
            account = session.query(Account).filter(Account.hashCode == account_hashCode).\
                filter((Account.accountStatus == 'ACTIVATED') | (Account.accountStatus == 'SUSPENDED')).one()
        except:
            return False
        else:
            while account.parentId != None:
                try:
                    parent = session.query(Account).filter(Account.id == account.parentId).\
                         filter((Account.accountStatus == 'ACTIVATED')| (Account.accountStatus == 'SUSPENDED')).one()
                except:
                    return False
                if parent.hashCode == user.accountHash:
                    return True
                account = parent
    return False
