'''
AMQP library based on kombu
'''

import kombu
import kombu.entity
import kombu.messaging
import kombu.connection
import kombu.pools
import time
import logging
import json
import socket
import eventlet
from common import utils

class AMQPProto(object):

    def __init__(self, config, conn_type='producer', logger=None):
        self.params = dict(hostname=config['rabbit_host'],
                          port=config['rabbit_port'],
                          userid=config['rabbit_userid'],
                          password=config['rabbit_password'],
                          virtual_host=config['rabbit_virtual_host'])
        self.exchange = None
        self.exchange_name = config.get('exchange')
        self.routing_key = config.get('routing_key')
        self.connection = None
        self.durable = config.get('durable')
        self.conn_type = conn_type
        if logger is None:
            self.logger = logging.getLogger(__name__)
        else:
            self.logger = logger
        self.queues = None

    def reconnect(self):
        '''Reconnect to AMQP'''
        if self.connection:
            try:
                self.connection.close()
            except self.connection.connection_errors:
                pass
            time.sleep(1)

        self.connection = kombu.connection.BrokerConnection(**self.params)
        self.logger.debug("initialized kombu connection: %s", self.params)

        options = {
            "durable": utils.getboolean(self.durable),
            "exclusive": False
        }

        exchange = kombu.entity.Exchange(
                name=self.exchange_name,
                type='topic',
                durable=self.durable,
                auto_delete=False)
        self.exchange = exchange

        if self.conn_type == 'consumer':
            queue_name = 'queue-%s' % self.routing_key
            self._queue_append(queue_name, self.routing_key, False, options)

    def _queue_append(self, queue_name, routing_key, auto_delete, options):
        '''append queue to class queues'''
        self.channel = self.connection.channel()
        self.queues = []
        self.queues.append(kombu.entity.Queue(
            name=queue_name,
            exchange=self.exchange,
            routing_key=routing_key,
            channel=self.channel,
            auto_delete=auto_delete,
            **options))

class Publisher(AMQPProto):

    def __init__(self, config, logger=None):
        super(Publisher, self).__init__(config, conn_type='producer',
                                        logger=logger)

    def produce(self, data, routing_key=None, timeout=None):
        '''
        Put a message on a queue.
        :data should be json serializable
        :routing_key will be taken from instantiated class unless specified
        :timeout to wait while publishing a message. Produce is blocking
        '''
        def __macro_publish(rk):
            with kombu.messaging.Producer(self.connection,
                                          exchange=self.exchange,
                                          routing_key=rk,
                                          serializer='json') \
                      as producer:
                self.logger.debug("Publishing data: %s",
                          json.dumps(data, indent=2, sort_keys=True))
                if timeout:
                    producer.publish(data, headers={'ttl': (timeout * 1000)})
                else:
                    producer.publish(data)

        try:
            if not routing_key:
                routing_key = self.routing_key
            self.reconnect()
            __macro_publish(rk=routing_key)

        except socket.error:
            pass
        except Exception, exc:
            self.logger.exception('Failed to push message to queues: %s',
                                   str(exc))



class Subscriber(AMQPProto):

    def __init__(self, config, logger=None):
        self.eventlet = None
        super(Subscriber, self).__init__(config, conn_type='consumer',
                                         logger=logger)

    def process_message(self, body, message):
        '''
        Pick up a message from queue and do something
        '''
        try:
            self.process_event(body, message)
        except KeyError:
            self.logger.exception("cannot handle message")
        message.ack()

    def process_event(self, body, message):
        """
        This function receive ``body`` and pass it to processing.
        Should be defined in inheritance.
        """
        raise NotImplementedError

    def consume(self):
        """
        Get messages in an infinite loop. This is the main function for
        service green thread.
        """
        while True:
            self.consume_one()

    def consume_one(self):
        '''
        Get a message
        '''
        try:
            self.reconnect()
            with kombu.messaging.Consumer(
                channel=self.channel,
                queues=self.queues,
                callbacks=[self.process_message]):
                while True:
                    self.connection.drain_events()
        except socket.error:
            pass
        except Exception, exc:
            self.logger.exception('Failed to consume message from queue: %s',
                                  str(exc))

    def start(self):
        '''start consume in a thread'''
        self.eventlet = eventlet.spawn(self.consume)

    def stop(self):
        '''kill all threads'''
        self.eventlet.kill()

    def wait(self):
        '''wait for consume thread to return - won't happen :)'''
        self.eventlet.wait()
