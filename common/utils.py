'''
General set of utilities
'''
from __future__ import absolute_import
import re
import os
import stat
import json
import logging
import os.path
import urlparse
import string
import random


from collections import OrderedDict

URLRE = re.compile(
        r'^(?:http|ftp)s?://' # http:// or https://
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|' #domain...
        r'localhost|' #localhost...
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' # ...or ip
        r'(?::\d+)?' # optional port
        r'(?:/?|[/?]\S+)$', re.IGNORECASE)


def verify_url(url):
    '''
    Check if url is properly formed
    '''
    if URLRE.match(url):
        return True
    else:
        return False

def check_permissions(filepath):
    '''
    Check if file has strict enough permissions and is a file. We are looking
    for file to be accessible by current user and current user only.
    '''
    try:
        st = os.stat(filepath)
    except OSError:
        return False
    if stat.S_ISREG(st.st_mode) and \
            stat.S_IMODE(st.st_mode) in (384, 448) and \
            st.st_uid == os.geteuid():
        return True
    else:
        return False

_BOOLEAN_STATES = {'1': True, 'yes': True, 'true': True, 'on': True,
                   '0': False, 'no': False, 'false': False, 'off': False}

def getboolean(value):
    '''
    Convert config value to boolean
    '''
    if value.lower() not in _BOOLEAN_STATES:
        raise ValueError('Not a boolean: %s' % value)
    return _BOOLEAN_STATES[value.lower()]

def getlist(value):
    '''
    Convert config value of comma separated items to list
    '''
    if not isinstance(value, basestring):
        return None
    else:
        return value.split(',')

def logger_init(debug=False, name='__main__', logpath=None):
    '''Init logger'''
    logger = logging.getLogger(name)
    logformat = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    formatter = logging.Formatter(logformat)
    logger.setLevel(logging.INFO)
    c_handler = logging.StreamHandler()
    if not logpath:
        logpath = os.path.abspath('.')
    else:
        logpath = os.path.abspath(logpath)
    logfile = "%s/%s.log" % (logpath, name)
    f_handler = logging.FileHandler(logfile)
    f_handler.setLevel(logging.INFO)
    c_handler.setFormatter(formatter)
    f_handler.setFormatter(formatter)
    logger.addHandler(c_handler)
    if debug:
        logger.setLevel(logging.DEBUG)
        c_handler.setLevel(logging.DEBUG)
    logger.addHandler(f_handler)
    return logger

def action_from_path(method, path_info):
    '''
    Return the method to call from the path_info
    '''
    if path_info == '/v1/users/me':
        return 'do_' + method.lower() +'_me'
    path_split = path_info.split('/')
    path_split.pop(0)
    if len((path_split)) <= 1:
        return ''
    function_to_call = "do_%s" % method.lower()
    if len(path_split) % 2:
        function_to_call = function_to_call + '_' + str(path_split[-2]) + '_detail'
    else:
        function_to_call = function_to_call + '_' + str(path_split[-1]) + '_main'
    return function_to_call

def dict_attributes(obj, display_attributes):
    '''
    Extract from an object each attribute provided in display_attributes
    Return a OrderedDict (required for a predicatable json.dumps order)
    '''
    result = OrderedDict({})
    for attribute in display_attributes:
        try:
            result[attribute] = getattr(obj, attribute)
        except:
            result[attribute] = None
    return result

def check_fields(data, ignored_fields, required_fields, optional_fields=None):
    '''
    Check if the dict data contain all the required_field,
    if optional_fields have the good syntax
    and no other fields than ignored_fields, required_field or  optional_fields
    Return a boolean and error message.
    '''
    message = ''
    for key in data.keys():
        if key in ignored_fields:
            continue
        if key in required_fields:
            if not re.match(required_fields[key]['regexp'], str(data[key])):
                message = 'Wrong format : %s' % key
                return False, message
            else:
                required_fields[key]['filled'] = True
                continue
        if key in optional_fields:
            if not re.match(optional_fields[key]['regexp'], str(data[key])):
                message = 'Wrong format : %s' % key
                return False, message
            else:
                continue
        message = 'Unknowned field : %s ' % key
        return False, message
    for key in required_fields.keys():
        if not required_fields[key]['filled']:
            message = '%s is missing' %  key
            return False, message
    return True, message

def generate_uniq_hashCode(type, size, session):
    ''' Generate a uniq hash and check the DB '''
    chars = string.ascii_lowercase + string.digits
    return ''.join(random.choice(chars) for _ in range(size))
