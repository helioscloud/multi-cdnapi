'''
API part of application
'''

from webob import Response, Request
import webob.dec
from webob import exc
from paste import httpserver
from paste.deploy import loadapp

import logging
import re
import json
import yaml
import datetime

from common import utils
from common import access
from models.base import *
from models.accounts import *
from models.services import *
from models.users import *
from models.hosts import *
from models.analytics import *
from common import amqp

TAG_RE = re.compile(r'<[^>]+>')
'''
Monkey-patch webob.exc to return json
'''

from webob.compat import (
    class_types,
    text_type,
    urlparse,
    )
from string import Template
from webob.exc import no_escape


json_template_obj = Template('''\
{
  "status": "${status}",
  "message": "${body}"
}''')


def json_body(self, environ):
    body = self._make_body(environ, no_escape)
    body = TAG_RE.sub('', body.strip())
    body = body.replace('\n','')
    return json_template_obj.substitute(status=self.status,
                                        body=body)
def generate_response(self, environ, start_response):
    if self.content_length is not None:
        del self.content_length
    headerlist = list(self.headerlist)
    accept = environ.get('HTTP_ACCEPT', '')
    if accept and 'html' in accept or '*/*' in accept:
        content_type = 'text/html'
        body = self.html_body(environ)
    elif accept and 'json' in accept:
        content_type = 'application/json'
        body = self.json_body(environ)
    else:
        content_type = 'text/plain'
        body = self.plain_body(environ)
    extra_kw = {}
    if isinstance(body, text_type):
        extra_kw.update(charset='utf-8')
    resp = Response(body,
                    status=self.status,
                    headerlist=headerlist,
                    content_type=content_type,
                    **extra_kw
                   )
    resp.content_type = content_type
    return resp(environ, start_response)

exc.WSGIHTTPException.json_body = json_body
exc.WSGIHTTPException.generate_response = generate_response


class ApiApp(object):
    '''
    Api application
    '''
    def __init__(self, conf, logger=None):
        #logging.basicConfig()
        #logging.getLogger('sqlalchemy.engine').setLevel(logging.WARN)
        if conf is None:
            conf = {}
        if logger is None:
            self.logger = logging.getLogger('optimicdnapi')
        else:
            self.logger = logger

        self.conf = conf
        self.session = None


    def __call__(self, env, start_response):
        self.logger.debug('Starting processing call')
        if not env.get('user'):
            self.logger.error("Can't find the user_permission. Make sure "
                              "to configure auth middleware prior to main app")
            resp = exc.HTTPForbidden()
            return resp(env, start_response)
        req = Request(env)
        if req.body:
            self.logger.debug("Received body: %s", str(req.body))
        action = utils.action_from_path(req.method, req.path_info)
        try:
            meth = getattr(self, action)
        except AttributeError:
            self.logger.error("Method or Endpoint not implemented : %s %s", req.method, action)
            resp = exc.HTTPNotImplemented()
        else:
            self.session = db_reconnect(db_url=self.conf.get('database_url'),
                                        session=self.session)
            try:
                resp = meth(env['user'], env['user_permissions'], req)
            except exc.HTTPException, e:
                resp = e
        self._add_headers(resp)
        return resp(env, start_response)

    def _push_to_amqp(self, datafeed):
        '''
        Submit a message to amqp for processing
        '''
        publisher = amqp.Publisher(self.conf)
        publisher.produce(datafeed)

    def do_get_accounts_detail(self, user, user_permissions, req):
        '''
        Display an account details
        '''
        # TBD : check account entry (regexp ?)
        account_hash_code = req.path_info.split('/')[-1]
        if not access.check_user_permissions(user, user_permissions, account_hash_code, 'account', 'READ', self.session):
            self.logger.info('Denied access for user %s on the account %s' % \
                (user.username, account_hash_code))
            raise exc.HTTPForbidden()
        display_subaccounts = False
        if 'display_subaccounts' in req.GET.keys():
            display_subaccounts = req.GET['display_subaccounts'].capitalize()
        self.logger.info('User %s request info on account %s' % (user.username, account_hash_code))
        resp = exc.HTTPOk()
        account = Account(hashCode=account_hash_code)
        resp.body = json.dumps(account.to_dict(self.session, display_subaccounts), indent=4)
        return resp

    def do_delete_accounts_detail(self, user, user_permissions, req):
        '''
        Delete an account
        '''
        account_hash_code = req.path_info.split('/')[-1]
        self.logger.info('DELETE')
        if not access.check_user_permissions(user, user_permissions, account_hash_code, 'account', 'EDIT', self.session):
            self.logger.info('User %s tried to delete the account %s without required permissions.' % \
                (user.username, account_hash_code))
            raise exc.HTTPForbidden()
        try:
            account = Account(hashCode=account_hash_code)
            if account.delete(self.session):
                self.logger.info('User %s delete the account %s' % (user.username, account_hash_code))
                resp = exc.HTTPOk('Account deleted')
            else:
                self.logger.error('User %s failed to delete the account %s' % (user.username, account_hash_code))
                raise HTTPBadRequest('Account %s can\'t be deleted.' % (account_hash_code))
        except:
            raise exc.HTTPForbidden()
        return resp

    def do_post_accounts_detail(self, user, user_permissions, req):
        '''
        Add a new account
        '''
        account_hash_code = req.path_info.split('/')[-1]
        self.logger.debug('do_post_accounts_details user: %s req.body: %s' ) % (user.username, req.body)
        if access.check_user_permissions(user, user_permissions, account_hash_code, 'account', 'EDIT', self.session) != True:
            self.logger.error('Access denied for user %s to create a new account' % (user.username))
            raise exc.HTTPForbidden('Access denied')
        account = Account()
        is_created, message = account.create_from_dict(self._load_json(req.body), self.session, account_hash_code)
        if not is_created:
            self.logger.error('User %s fails to create a new account : %s' % (user.username, message))
            raise exc.HTTPBadRequest(message)
        else:
            resp = exc.HTTPCreated()
            resp.body = json.dumps(message)
        return resp

    def do_put_accounts_detail(self, user, user_permissions, req):
        '''
        Update an account
        '''
        account_hash_code = req.path_info.split('/')[-1]
        if access.check_user_permissions(user, user_permissions, account_hash_code, 'account', 'EDIT', self.session) != True:
            raise exc.HTTPForbidden()
        account = Account()
        is_updated, message = account.update_from_dict(self._load_json(req.body), self.session, account_hash_code)
        if not is_updated:
            raise exc.HTTPBadRequest(message)
        else:
            resp = exc.HTTPOk()
            resp.body = json.dumps(message)
        return resp

    def do_get_services_main(self, user, user_permissions, req):
        '''
        Display the services associated with an account
        '''
        # TBD : check account entry (regexp ?)
        account_hash_code = req.path_info.split('/')[-2]
        if access.check_user_permissions(user, user_permissions, account_hash_code, 'account', 'READ', self.session) != True:
            raise exc.HTTPForbidden()
        self.logger.info('User %s request the services list for account %s' % (user.username, account_hash_code))
        account = self.session.query(Account).filter(Account.hashCode == account_hash_code).one()
        services_list = []
        for service in account.services:
             services_list.append(service.to_dict())
        resp = exc.HTTPOk()
        resp.body = json.dumps(services_list)
        return resp

    def do_get_me(self, user, user_permissions, req):
        '''
        Display info about the logged user
        '''
        self.logger.info('User %s request info about himself' % (user.username))
        resp = exc.HTTPOk()
        resp.body = json.dumps(user.to_dict(self.session))
        return resp

    def do_put_me(self, user, user_permissions, req):
        '''
        Update the logged user. No Roles could be changed there
        '''
        self.logger.info('User %s update info bout himself' % (user.username))
        is_updated, message = user.update_me(self._load_json(req.body), \
            self.session)
        if not is_updated:
            raise exc.HTTPBadRequest(message)
        else:
            resp = exc.HTTPOk()
            resp.body = json.dumps(message)
        return resp

    def do_get_users_detail(self, user, user_permissions, req):
        '''
        Display info about a user
        '''
        splitted_path = req.path_info.split('/')
        account_hash_code = splitted_path[-3]
        user_id = splitted_path[-1]
        self.logger.info('User %s request info about the user %s for the account %s' \
            % (user.username, user_id, account_hash_code))
        if access.check_user_permissions(user, user_permissions, account_hash_code, 'account', 'READ', self.session) != True:
            raise exc.HTTPForbidden()
        try:
            user_to_display = self.session.query(User).filter(User.id == user_id).one()
            if user_to_display.accountHash != account_hash_code:
                raise exc.HTTPBadRequest()
            else:
                resp = exc.HTTPOk()
                resp.body = json.dumps(user_to_display.to_dict(self.session))
                return resp
        except:
            raise exc.HTTPBadRequest()

    def do_get_users_main(self, user, user_permissions, req):
        '''
        Display all users attached to an account
        '''
        account_hash_code = req.path_info.split('/')[-2]
        if access.check_user_permissions(user, user_permissions, account_hash_code, 'account', 'READ', self.session) != True:
            raise exc.HTTPForbidden()
        try:
            to_display = []
            users = self.session.query(User).filter(User.accountHash == account_hash_code)
            for user_to_display in users:
                to_display.append(user_to_display.to_dict(self.session))
            resp = exc.HTTPOk()
            resp.body = json.dumps(to_display)
            return resp
        except:
            raise exc.HTTPBadRequest()

    def do_post_users_main(self, user, user_permissions, req):
        '''
        Create a new user attached to an account
        '''
        account_hash_code = req.path_info.split('/')[-2]
        if access.check_user_permissions(user, user_permissions, account_hash_code, 'account', 'EDIT', self.session) != True:
            raise exc.HTTPForbidden()
        user = User()
        is_created, message = user.create_from_dict(self._load_json(req.body), self.session, account_hash_code)
        if not is_created:
            raise exc.HTTPBadRequest(message)
        else:
            resp = exc.HTTPCreated()
            resp.body = json.dumps(message)
        return resp

    def do_put_users_detail(self, user, user_permissions, req):
        '''
        Update a user attached to an account
        '''
        splitted_path = req.path_info.split('/')
        account_hash_code = splitted_path[-3]
        user_id = splitted_path[-1]
        if access.check_user_permissions(user, user_permissions, account_hash_code, 'account', 'EDIT', self.session) != True:
            raise exc.HTTPForbidden()
        data = self._load_json(req.body)
        self.logger.info('User %s updates info about the user %s for the account %s' \
            % (user.username, user_id, account_hash_code))
        if data.get('id'):
            if int(data.get('id')) != int(user_id):
                raise exc.HTTPBadRequest('Wrong user id')
        else:
            data["id"] = user_id
        user = User()
        is_updated, message = user.update_from_dict(data, self.session, account_hash_code)
        if not is_updated:
            raise exc.HTTPBadRequest(message)
        else:
            resp = exc.HTTPOk()
            resp.body = json.dumps(message)
        return resp

    def do_delete_users_detail(self, user, user_permissions, req):
        '''
        Delete a user attached to an account
        '''
        splitted_path = req.path_info.split('/')
        account_hash_code = splitted_path[-3]
        user_id = int(splitted_path[-1])
        if access.check_user_permissions(user, user_permissions, account_hash_code, 'account', 'EDIT', self.session) != True:
            raise exc.HTTPForbidden()
        self.logger.info('User %s delete the user %s for the account %s' \
            % (user.username, user_id, account_hash_code))
        if user.id == user_id:
            raise exc.HTTPBadRequest('You can\'t delete yourself.')
        try:
            user_to_delete = self.session.query(User).filter(User.id == user_id).\
                filter(User.accountHash == account_hash_code).one()
            self.session.delete(user_to_delete)
            self.session.commit()
        except:
            raise exc.HTTPBadRequest('Wrong user id')
        return exc.HTTPOk('User deleted')

    def do_get_origin_main(self, user, user_permissions, req):
        '''
        Display the origins associated with an account
        '''
        # TBD : check account entry (regexp ?)
        splitted_path = req.path_info.split('/')
        account_hash_code = splitted_path[-4]
        cname = splitted_path[-2]
        if access.check_user_permissions(user, user_permissions, account_hash_code, 'configuration', 'READ', self.session) != True:
            raise exc.HTTPForbidden()
        self.logger.info('User %s request the origin details for the host %s on account %s' % (user.username, cname, account_hash_code))
        try :
            host = self.session.query(Host).filter(Host.cname == cname).\
                filter(Host.accountHash == account_hash_code).one()
            origin = self.session.query(Origin).filter(Origin.hostcname == cname).one()
        except:
            raise exc.HTTPBadRequest('Unknown Account/Host.')
        resp = exc.HTTPOk()
        resp.body = json.dumps(origin.to_dict(self.session))
        return resp

    def do_put_origin_main(self, user, user_permissions, req):
        '''
        Update an origin
        '''
        splitted_path = req.path_info.split('/')
        account_hash_code = splitted_path[-4]
        cname = splitted_path[-2]
        if access.check_user_permissions(user, user_permissions, account_hash_code, 'configuration', 'READ', self.session) != True:
            raise exc.HTTPForbidden()
        self.logger.info('User %s update the origin for the host %s on account %s' % (user.username, cname, account_hash_code))
        try :
            host = self.session.query(Host).filter(Host.cname == cname).\
                filter(Host.accountHash == account_hash_code).one()
            origin = self.session.query(Origin).filter(Origin.hostcname == cname).one()
        except:
            raise exc.HTTPBadRequest('Unknown Account/Host.')
        is_updated, message = origin.update_from_dict(cname, self._load_json(req.body), self.session)
        if is_updated:
            resp = exc.HTTPOk()
            resp.body = json.dumps(origin.to_dict(self.session))
            self._push_to_amqp({'object_type': 'origin', 'action': 'update', 'id': origin.id})
        else:
            resp = exc.HTTPBadRequest(message)
        return resp

    def do_get_hosts_main(self, user, user_permissions, req):
        '''
        Display the Hosts associated with an account
        '''
        # TBD : check account entry (regexp ?)
        account_hash_code = req.path_info.split('/')[-2]
        if access.check_user_permissions(user, user_permissions, account_hash_code, 'configuration', 'READ', self.session) != True:
            raise exc.HTTPForbidden()
        self.logger.info('User %s request the hosts list for account %s' % (user.username, account_hash_code))
        hosts = self.session.query(Host).filter(Host.accountHash == account_hash_code).\
            filter(Host.active == True).all()
        hosts_list = []
        for host in hosts:
            hosts_list.append(host.to_dict(self.session))
        resp = exc.HTTPOk()
        resp.body = json.dumps(hosts_list)
        return resp

    def do_get_hosts_detail(self, user, user_permissions, req):
        '''
        Display an host details
        '''
        # TBD : check account entry (regexp ?)
        splitted_path = req.path_info.split('/')
        account_hash_code = splitted_path[-3]
        cname = splitted_path[-1]
        self.logger.info('User %s request info on host %s under account %s' % (user.username, cname, account_hash_code))
        if not access.check_user_permissions(user, user_permissions, account_hash_code, 'configuration', 'READ', self.session):
            self.logger.info('Denied access for user %s on the account %s' % \
                (user.username, account_hash_code))
            raise exc.HTTPForbidden()
        try:
            host = self.session.query(Host).filter(Host.cname == cname, Host.accountHash == account_hash_code,\
                Host.active == True).one()
        except:
            raise exc.HTTPForbidden()
        resp = exc.HTTPOk()
        resp.body = json.dumps(host.to_dict(self.session), indent=4)
        return resp

    def do_get_hostnames_main(self, user, user_permissions, req):
        '''
        Display the hostnames associated with an account
        '''
        # TBD : check account entry (regexp ?)
        splitted_path = req.path_info.split('/')
        account_hash_code = splitted_path[-4]
        cname =  splitted_path[-2]
        if access.check_user_permissions(user, user_permissions, account_hash_code, 'configuration', 'READ', self.session) != True:
            raise exc.HTTPForbidden()
        self.logger.info('User %s request the hostnames list for the host %s on the account %s' % (user.username, cname, account_hash_code))
        try:
            host = self.session.query(Host).filter(Host.cname == cname).\
                filter(Host.accountHash == account_hash_code).one()
        except:
            raise exc.HTTPBadRequest('Unknown Account/Host.')
        hostnames_list = host.get_hostnames(self.session)
        resp = exc.HTTPOk()
        resp.body = json.dumps(hostnames_list)
        return resp

    def do_post_hostnames_main(self, user, user_permissions, req):
        '''
        Add a hostname to a hosts
        '''
        splitted_path = req.path_info.split('/')
        account_hash_code = splitted_path[-4]
        cname = splitted_path[-2]
        if access.check_user_permissions(user, user_permissions, account_hash_code, 'configuration', 'EDIT', self.session) != True:
            raise exc.HTTPForbidden()
        data = self._load_json(req.body)
        self.logger.info('User %s add the hostname %s to the host %s on the account %s' % (user.username, data, cname, account_hash_code))
        try:
            host = self.session.query(Host).filter(Host.cname == cname).\
                filter(Host.accountHash == account_hash_code).one()
        except:
            raise exc.HTTPBadRequest('Unknown Account/Host.')
        hh = Hostname()
        is_created, message = hh.create_from_dict(cname, data, self.session)
        if is_created:
            resp = exc.HTTPOk()
            resp.body = json.dumps(message)
            nh = self.session.query(Hostname).filter(Hostname.hostname == data).one()
            self._push_to_amqp({'object_type': 'hostname', 'action': 'add', 'id': nh.id})
            return resp
        else:
            raise exc.HTTPBadRequest(message)

    def do_delete_hostnames_detail(self, user, user_permissions, req):
        '''
        Delete an hostname from a host
        '''
        splitted_path = req.path_info.split('/')
        hostname = splitted_path[-1]
        cname = splitted_path[-3]
        account_hash_code = splitted_path[-5]
        if access.check_user_permissions(user, user_permissions, account_hash_code, 'configuration', 'EDIT', self.session) != True:
            raise exc.HTTPForbidden()
        self.logger.info('User %s delete the hostname %s from the host %s on the account %s' % (user.username, hostname, cname, account_hash_code))
        try:
            host = self.session.query(Host).filter(Host.cname == cname).\
                filter(Host.accountHash == account_hash_code).one()
        except:
            raise exc.HTTPBadRequest('Unknown Account/Host.')
        hh = Hostname()
        is_deleted, message = hh.delete(cname, hostname, self.session)
        if is_deleted:
            resp = exc.HTTPOk()
            resp.body = json.dumps(message)
            self._push_to_amqp({'object_type': 'hostname', 'action': 'delete', 'cname': cname, 'hostname': hostname})
            return resp
        else:
            raise exc.HTTPBadRequest(message)

    def do_get_scopes_main(self, user, user_permissions, req):
        '''
        Get the scopes associated with an host
        '''
        splitted_path = req.path_info.split('/')
        cname = splitted_path[-2]
        account_hash_code = splitted_path[-4]
        if access.check_user_permissions(user, user_permissions, account_hash_code, 'configuration', 'READ', self.session) != True:
            raise exc.HTTPForbidden()
        self.logger.info('User %s retrieve the scopes list associated with the host %s on the account %s' % (user.username, cname, account_hash_code))
        try:
            host = self.session.query(Host).filter(Host.cname == cname).\
                filter(Host.accountHash == account_hash_code).one()
        except:
            raise exc.HTTPBadRequest('Unknown Account/Host.')
        scopes_list = host.get_scopes(self.session)
        resp = exc.HTTPOk()
        resp.body = json.dumps(scopes_list)
        return resp

    def do_post_scopes_main(self, user, user_permissions, req):
        '''
        Post a new scope
        '''
        splitted_path = req.path_info.split('/')
        cname = splitted_path[-2]
        account_hash_code = splitted_path[-4]
        if access.check_user_permissions(user, user_permissions, account_hash_code, 'configuration', 'EDIT', self.session) != True:
            raise exc.HTTPForbidden()
        self.logger.info('User %s post a new scope for the host %s on the account %s' % (user.username, cname, account_hash_code))
        data = self._load_json(req.body)
        try:
            host = self.session.query(Host).filter(Host.cname == cname).\
                filter(Host.accountHash == account_hash_code).one()
        except:
            raise exc.HTTPBadRequest('Unknown Account/Host.')
        sh = Scope()
        res, message = sh.create_from_dict(cname, data, self.session)
        if res:
            resp = exc.HTTPOk()
            resp.body = json.dumps(message)
            self._push_to_amqp({'object_type': 'scope', 'action': 'add', 'id': message['id']})
        else:
            resp = exc.HTTPBadRequest(message)
        return resp

    def do_get_scopes_detail(self, user, user_permissions, req):
        '''
        Get the the description of an scope
        '''
        splitted_path = req.path_info.split('/')
        scope_id = splitted_path[-1]
        cname = splitted_path[-3]
        account_hash_code = splitted_path[-5]
        if access.check_user_permissions(user, user_permissions, account_hash_code, 'configuration', 'READ', self.session) != True:
            raise exc.HTTPForbidden()
        self.logger.info('User %s retrieve the scope %s associated with the host %s on the account %s' % (user.username, scope_id, cname, account_hash_code))
        try:
            host = self.session.query(Host).filter(Host.cname == cname).\
                filter(Host.accountHash == account_hash_code).one()
            scope = self.session.query(Scope).filter(Scope.hostcname == cname).\
                filter(Scope.id == scope_id).one()
        except:
            raise exc.HTTPBadRequest('Unknown Account/Host/Scope id.')
        resp = exc.HTTPOk()
        resp.body = json.dumps(scope.to_dict(self.session))
        return resp

    def do_put_scopes_detail(self, user, user_permissions, req):
        '''
        Get update a scope
        '''
        splitted_path = req.path_info.split('/')
        scope_id = splitted_path[-1]
        cname = splitted_path[-3]
        account_hash_code = splitted_path[-5]
        if access.check_user_permissions(user, user_permissions, account_hash_code, 'configuration', 'READ', self.session) != True:
            raise exc.HTTPForbidden()
        self.logger.info('User %s update the scope %s associated with the host %s on the account %s' % (user.username, scope_id, cname, account_hash_code))
        try:
            host = self.session.query(Host).filter(Host.cname == cname).\
                filter(Host.accountHash == account_hash_code).one()
            scope = self.session.query(Scope).filter(Scope.hostcname == cname).\
                filter(Scope.id == scope_id).one()
        except:
            raise exc.HTTPBadRequest('Unknown Account/Host/Scope id.')
        data = self._load_json(req.body)
        res, message = scope.update_from_dict(scope_id, cname, data, self.session)
        if res:
            resp = exc.HTTPOk()
            self._push_to_amqp({'object_type': 'scope', 'action': 'update', 'id': scope_id})
        else:
            resp = exc.HTTPBadRequest()
        resp.body = json.dumps(message)
        return resp

    def do_delete_scopes_detail(self, user, user_permissions, req):
        '''
        Delete a scope
        '''
        splitted_path = req.path_info.split('/')
        scope_id = splitted_path[-1]
        cname = splitted_path[-3]
        account_hash_code = splitted_path[-5]
        if access.check_user_permissions(user, user_permissions, account_hash_code, 'configuration', 'EDIT', self.session) != True:
            raise exc.HTTPForbidden()
        self.logger.info('User %s deletes the scope %s associated with the host %s on the account %s' % (user.username, scope_id, cname, account_hash_code))
        try:
            host = self.session.query(Host).filter(Host.cname == cname).\
                filter(Host.accountHash == account_hash_code).one()
            scope = self.session.query(Scope).filter(Scope.hostcname == cname).\
                filter(Scope.id == scope_id).one()
        except:
            raise exc.HTTPBadRequest('Unknown Account/Host/Scope id.')
        is_deleted, message = scope.delete(scope.id, cname, self.session)
        if is_deleted:
            resp = exc.HTTPOk()
            self._push_to_amqp({'object_type': 'scope', 'action': 'delete', 'id': scope_id, 'cname': cname})
        else:
            resp = exc.HTTPBadRequest()
        resp.body = json.dumps(message)
        return resp

    def do_post_hosts_main(self, user, user_permissions, req):
        '''
        Create a new host
        '''
        account_hash_code = req.path_info.split('/')[-2]
        if access.check_user_permissions(user, user_permissions, account_hash_code, 'configuration', 'EDIT', self.session) != True:
            raise exc.HTTPForbidden()
        self.logger.info('User %s creates an new host for account %s' % (user.username, account_hash_code))
        data = self._load_json(req.body)
        try:
            host = Host()
            res, message = host.create_from_dict(account_hash_code, data, self.session)
            if res:
                resp = exc.HTTPOk()
                resp.body = json.dumps(message)
                self._push_to_amqp({'object_type': 'host', 'action': 'add', 'cname': message['cname']})
                self._push_to_amqp({'object_type': 'application', 'action': 'add'})
                self._push_to_amqp({'object_type': 'hwhost', 'action': 'add'})
            else:
                resp = exc.HTTPBadRequest(message)
            return resp
        except:
            raise exc.HTTPForbidden()

    def do_put_hosts_detail(self, user, user_permissions, req):
        '''
        Update an host
        '''
        split_path = req.path_info.split('/')
        account_hash_code =  split_path[-3]
        cname = split_path[-1]
        if access.check_user_permissions(user, user_permissions, account_hash_code, 'configuration', 'EDIT', self.session) != True:
            raise exc.HTTPForbidden()
        self.logger.info('User %s update the host %s under the account %s' % (user.username, cname, account_hash_code))
        data = self._load_json(req.body)
        try:
            host = self.session.query(Host).filter(Host.cname == cname).filter(Host.active == True).filter(Host.accountHash == account_hash_code).one()
            res, message = host.update_from_dict(account_hash_code, cname, data, self.session)
            if res:
                resp = exc.HTTPOk()
                resp.body = json.dumps(message)
            else:
                resp = exc.HTTPBadRequest(message)
            return resp
        except:
            raise exc.HTTPForbidden()

    def do_delete_hosts_detail(self, user, user_permissions, req):
        '''
        Delete an scope
        '''
        split_path = req.path_info.split('/')
        account_hash_code = split_path[-3]
        cname =  split_path[-1]
        if access.check_user_permissions(user, user_permissions, account_hash_code, 'configuration', 'EDIT', self.session) != True:
            raise exc.HTTPForbidden()
        self.logger.info('User %s delete the host %s under account %s' % (user.username, cname, account_hash_code))
        try:
            host = self.session.query(Host).filter(Host.cname == cname).filter(Host.active == True).filter(Host.accountHash == account_hash_code).one()
            res, message = host.delete(self.session)
            if res:
                resp = exc.HTTPOk()
                resp.body = json.dumps(message)
            else:
                resp = exc.HTTPBadRequest(message)
            return resp
        except:
            raise exc.HTTPForbidden()

    def do_get_analytics_detail(self, user, user_permissions, req):
        '''
        Display analytics
        '''
        # TBD : check account entry (regexp ?)
        account_hash = req.path_info.split('/')[-3]
        if not access.check_user_permissions(user, user_permissions, account_hash, 'report', 'READ', self.session):
            self.logger.info('Denied access for user %s for analytics on the account %s' % \
                (user.username, account_hash_code))
            raise exc.HTTPForbidden()
        if not req.GET.get('startDate') or not req.GET.get('endDate'):
            self.logger.error('' % ())
            return exc.HTTPBadRequest('startDate and endDate are required')
        try:
            startDate = datetime.datetime.strptime(req.GET.get('startDate'), '%Y-%m-%dT%H:%M:00Z')
            endDate = datetime.datetime.strptime(req.GET.get('endDate'), '%Y-%m-%dT%H:%M:00Z')
            if startDate >= endDate:
                return exc.HTTPBadRequest('endDate should be greater than startDate')
        except ValueError as e:
            return exc.HTTPBadRequest('Date format should be \%Y-\%m-\%dT\%H:\%M:00Z')
        if req.GET.get('group_by'):
            if req.GET.get('group_by') != 'account' and  req.GET.get('group_by') != 'host':
                return exc.HTTPBadRequest('group_by should be acount or host')
            group_by = req.GET.get('group_by')
        else:
            group_by = 'account'
        if req.GET.get('host'):
            try:
                hh = self.session.query(Host).\
                    filter(Host.accountHash == account_hash).\
                    filter(Host.cname == req.GET.get('host')).one()
            except:
                raise exc.HTTPForbidden('')
        now = datetime.datetime.utcnow()
        delta = datetime.timedelta(seconds=int(self.conf['consolidated_analytics_delay']))
        if startDate > now - delta:
            rt = RealTimeUsage()
            data = rt.get_analytics(req.GET.get('startDate'), \
                req.GET.get('endDate'), self.session, account_hash, \
                group_by, req.GET.get('host'))
        if endDate < now - delta:
            cu = ConsolidatedUsage()
            data = cu.get_analytics(req.GET.get('startDate'), \
                req.GET.get('endDate'), self.session, account_hash, \
                group_by, req.GET.get('host'))
        if startDate < now - delta and endDate > now - delta:
            limit_date = now - delta
            rounded_date = datetime.datetime(limit_date.year, limit_date.month, \
                limit_date.day, limit_date.hour, limit_date.minute-(limit_date.minute %5), 0)
            cu = ConsolidatedUsage()
            data = cu.get_analytics(req.GET.get('startDate'), \
                rounded_date.strftime('%Y-%m-%dT%H:%M:00Z'),\
                self.session, account_hash, \
                group_by, req.GET.get('host'))
            rt = RealTimeUsage()
            data_rt = rt.get_analytics(rounded_date.strftime('%Y-%m-%dT%H:%M:00Z'), \
                req.GET.get('endDate'), self.session, account_hash, \
                group_by, req.GET.get('host'))
            for rt_serie in data_rt['series']:
                for cu_serie in data['series']:
                    print 'cu', cu_serie , 'rt', rt_serie
                    if cu_serie['serie_type'] == rt_serie['serie_type'] and cu_serie['key'] == rt_serie['key']:
                        for datapoint in rt_serie['datapoints']:
                            cu_serie['datapoints'].append(datapoint)
                        break
        resp = exc.HTTPOk()
        resp.body = json.dumps(data)
        return resp

    def _add_headers(self, resp):
        '''
        Add headers : json and CORS for swagger. * will be replaced by the domain where swagger will be installed
        '''
        if self.conf.get('allow_origin'):
            resp.headers['Access-Control-Allow-Origin'] = "%s" % self.conf.get('allow_origin')
        else:
            resp.headers['Access-Control-Allow-Origin'] = '*'
        resp.headers['Access-Control-Allow-Headers'] = '*'
        resp.headers['Access-Control-Allow-Methods'] = 'GET, POST, DELETE, PUT, OPTIONS'
        resp.content_type = 'application/json'

    def _load_json(self, data):
        try:
            jdata = json.loads(data)
        except:
            raise exc.HTTPBadRequest('Wrong format')
        return jdata

def optimicdnapi_factory(global_conf, **local_conf):
    '''
    Main wsgi app entry point
    '''
    conf = global_conf.copy()
    conf.update(local_conf)
    app = ApiApp(conf)
    return app
